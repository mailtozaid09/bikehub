import React, {useState, useEffect, useRef} from 'react'
import { Text, View, SafeAreaView, StyleSheet, Button, TouchableOpacity, Image, ScrollView, Dimensions, useColorScheme, TextInput, Alert, ToastAndroid, } from 'react-native'

import { media } from '../../global/media'
import { colors } from '../../global/colors'
import { Poppins } from '../../global/fontFamily'
import { screenWidth } from '../../global/constants'

import PrimaryButton from '../../components/button/PrimaryButton'

import LottieView from 'lottie-react-native'
import OTPTextView from 'react-native-otp-textinput';
import auth from '@react-native-firebase/auth'
import AlertModal from '../../components/modal/AlertModal'
import { useDispatch } from 'react-redux'
import { currentUserDetails, userLoggedIn } from '../../store/modules/auth/actions'
import { LocalNotification } from '../../utils/LocalPushController'
import { sendEmailOtp, sendPhoneOtp, verifyEmailOtp, verifyPhoneOtp } from '../../global/api'
import Icon from '../../utils/icons'

const picture_url = "https://images.unsplash.com/photo-1563990308267-cd6d3cc09318?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxzZWFyY2h8OHx8YmlrZXN8ZW58MHx8MHx8fDA%3D&auto=format&fit=crop&w=800&q=60"

const OtpVerify = (props) => {

    const dispatch = useDispatch()
    const input = useRef(null);

    const [errors, setErrors] = useState({});
    const [otpValue, setOtpValue] = useState('');
    const [loginTypeEmail, setLoginTypeEmail] = useState(false);
    const [loginTypeValue, setLoginTypeValue] = useState('');

    const [userDetails, setUserDetails] = useState({});

    const [number, setNumber] = useState('');
    const [orgNumber, setOrgNumber] = useState('');

    const [confirm, setConfirm] = useState(null);

    const [alertType, setAlertType] = useState('');
    const [alertTile, setAlertTile] = useState('');
    const [alertDescription, setAlertDescription] = useState('');
    const [showAlertModal, setShowAlertModal] = useState(false);

    const [buttonLoader, setButtonLoader] = useState(false);

    useEffect(() => {
        var number = props?.route?.params?.params?.value
        var loginType = props?.route?.params?.params
        var num = '+91 ' + loginType?.value
        
        console.log('====================================');
        console.log("loginType  // // ",loginType);
        console.log('====================================');
        setUserDetails(loginType)
        setLoginTypeValue(loginType?.value)
        setLoginTypeEmail(loginType?.loginTypeEmail)
        setNumber(num)
        setOrgNumber(number)

        checkForLogin(loginType)
        //signIn(number)
    }, [])

    const checkForLogin = (loginType) => {
        console.log('====================================');
        console.log("checkForLogin =>> ",loginType);
        console.log('====================================');

        if(loginType?.loginTypeEmail){
            signInWithEmailFunc(loginType?.value)
        }else{
            signInWithPhoneFunc(loginType?.value)
        }
    }

    const signInWithEmailFunc = async (val) => {
        console.log("signInWithEmailFunc ", val);

        var body = {
            "email": val
        }

        sendEmailOtp(body)
         .then(resp => {
            if(resp?.success){
                const message = resp?.message;
                ToastAndroid.show(message, ToastAndroid.SHORT);
            }
            console.log("resp => ", resp);
        })
        .catch(err => {
            console.log("err => ", err); 
        })

    }

    const signInWithPhoneFunc = async (val) => {
        console.log("signInWithPhoneFunc >>",val);
        var num = '+91' + val
        console.log('====================================');
        console.log(" var num =  ",num );
        console.log('====================================');
        try {
            const confirmation = await auth().signInWithPhoneNumber(num);
            console.log('====================================');
            console.log("confirmation-> ",confirmation);
            console.log('====================================');
            setConfirm(confirmation);
        } catch (error) {
            if (error.code === "auth/too-many-requests") {
                setAlertType('Error')
                setAlertTile("Error!!!");
                setAlertDescription("We have blocked all requests from this device due to unusual activity. Try again later!");
                setShowAlertModal(true)
                console.log('Invalid Verification Code! => ', error);
            }else{
                setAlertType('Error')
                setAlertTile("Error!!!");
                setAlertDescription(error.code);
                setShowAlertModal(true)
                console.log("signIn error =>> ", error);
            }
            
            
        }
    }

    const checkDoesUserExist = (newUser) => {
        if(userDetails?.userExist){
            console.log('====================================');
            console.log("userExist =  userDetails= > > ", userDetails);
            console.log('====================================');
            dispatch(currentUserDetails({
                id: userDetails?.user?.id,
                name: userDetails?.user?.name,
                email: userDetails?.user?.email,
                profile_url: userDetails?.user?.profile_url,
                phone: userDetails?.user?.phone,
                isAdmin: userDetails?.user?.isAdmin,
            }))

            dispatch(userLoggedIn(true))
            
            // const message = resp?.message;
            // ToastAndroid.show(message, ToastAndroid.SHORT);

            LocalNotification({
                picture: picture_url,
                title: "You've successfully logged in!",
                message: `Enjoy your experience with BikeHub.`,
            })

            setConfirm(null);
            setAlertType('Success')
            setAlertTile("Success!!!");
            setAlertDescription('User Logged In Successfully!');
            setShowAlertModal(true)
        }else{
            console.log('====================================');
            console.log("userExist nooooo---");
            console.log('====================================');
            props.navigation.navigate('Registration', {params: userDetails})
        }
    }

    const phoneVerificationCode = async (code) => {
        try {
            await confirm.confirm(code);
            
            setButtonLoader(false)

            checkDoesUserExist()
        } catch (error) {
            setButtonLoader(false)
            if (error.code === "auth/invalid-verification-code") {
                setAlertType('Error')
                setAlertTile("Error!!!");
                setAlertDescription("Invalid Verification Code!");
                setShowAlertModal(true)
                console.log('Invalid Verification Code! => ', error);
            }else if (error.code === "auth/invalid-phone-number") {
                setAlertType('Error')
                setAlertTile("Error!!!");
                setAlertDescription("Invalid Phone Number!");
                setShowAlertModal(true)
                console.log('Invalid Verification Code! => ', error);
            }else if (error.code === "auth/too-many-requests") {
                setAlertType('Error')
                setAlertTile("Error!!!");
                setAlertDescription("We have blocked all requests from this device due to unusual activity. Try again later!");
                setShowAlertModal(true)
                console.log('Invalid Verification Code! => ', error);
            }else if (error.code === "auth/session-expired") {
                setAlertType('Error')
                setAlertTile("Error!!!");
                setAlertDescription("The sms code has expired. Please re-send the verification code to try again!");
                setShowAlertModal(true)
                console.log('The sms code has expired. Please re-send the verification code to try again! => ', error);
            }else {
                setAlertType('Error')
                setAlertTile("Error!!!");
                setAlertDescription(error.code);
                setShowAlertModal(true)
                console.log("confirm error =>> ", error);
            }
        }
    }

    const emailVerificationCode = async (otp) => {
        console.log("emailVerificationCode ", otp, " ---  ", loginTypeValue);

        var body = {
            "otp": otp,
            "email": loginTypeValue
        }

        verifyEmailOtp(body)
         .then(resp => {
            if(resp?.success){
                const newUser = resp?.user?.newUser
                const message = resp?.message;
                ToastAndroid.show(message, ToastAndroid.SHORT);
            
                checkDoesUserExist(newUser)
            }
            console.log("resp => ", resp);
            
            setButtonLoader(false)

        })
        .catch(err => {
            console.log("err => ", err); 
            setButtonLoader(false)
        })
    }

    const signIn = async (phoneNumber) => {
        var body = {
            "phone": phoneNumber
        }

        console.log("body=> ", body);

        // sendPhoneOtp(body)
        //  .then(resp => {
        //     if(resp?.success){
        //         const message = resp?.message;
        //         ToastAndroid.show(message, ToastAndroid.SHORT);
        //     }
        //     console.log("resp => ", resp);
        // })
        // .catch(err => {
        //     console.log("err => ", err); 
        // })
    }

    // const phoneVerificationCode = async (code) => {

    //     var body = {
    //         "otp": code,
    //         "phone": orgNumber
    //     }

    //     console.log("body=> ", body);

    //     verifyPhoneOtp(body)
    //      .then(resp => {
    //         console.log("verify resp => ", resp);
    //         if(resp?.success){
                
    //             dispatch(currentUserDetails({
    //                 id: resp?.user?.id,
    //                 name: resp?.user?.name,
    //                 email: resp?.user?.email,
    //                 profile_url: resp?.user?.profile_url,
    //                 phone: resp?.user?.phone,
    //                 isAdmin: resp?.user?.isAdmin,
    //             }))

    //             dispatch(userLoggedIn(true))
                
    //             const message = resp?.message;
    //             ToastAndroid.show(message, ToastAndroid.SHORT);

    //             LocalNotification({
    //                 picture: picture_url,
    //                 title: "You've successfully logged in!",
    //                 message: `Enjoy your experience with BikeHub.`,
    //             })
    
    //             setAlertType('Success')
    //             setAlertTile("Success!!!");
    //             setAlertDescription('User Logged In Successfully!');
    //             setShowAlertModal(true)
    
                
    //             setButtonLoader(false)
    //         }else{
    //             setButtonLoader(false)
    //             setAlertType('Error')
    //             setAlertTile("Error!!!");
    //             setAlertDescription(resp?.message);
    //             setShowAlertModal(true);
    //         }
    //     })
    //     .catch(err => {
    //         console.log("err => ", err); 
    //         setAlertType('Error')
    //         setAlertTile("Error!!!");
    //         setAlertDescription(err?.message);
    //         setShowAlertModal(true);
    //         setButtonLoader(false);
    //     })
    //   }


    const verifyFunction = () => {
        setButtonLoader(true)
        var isValid = true
                
        if(otpValue.length != 6){
            console.log("Please enter a valid OTP");
            isValid = false
            setErrors((prev) => {
                return {...prev, OTP: 'Please enter a valid OTP'}
            })
        }

        if(isValid){
            !loginTypeEmail ? phoneVerificationCode(otpValue) : emailVerificationCode(otpValue)
        }else{
            setButtonLoader(false)
        }


    }
    
    return (
        <SafeAreaView style={styles.container} >           
           <ScrollView keyboardShouldPersistTaps="always" contentContainerStyle={{flex: 1}} >
                <View style={styles.mainContainer} >
                    <View style={{}} >
                        <View style={styles.iconContainer}>
                            <Icon type="MaterialCommunityIcons" name={loginTypeEmail ? 'email' : 'cellphone-text'} size={34} color={colors.dark_gray} />
                        </View>

                        <View style={{marginTop: 20, marginBottom: 10}} >
                            <Text style={styles.title} >Continue with {loginTypeEmail ? 'Email' : 'Phone'}</Text>  
                            <Text style={styles.subTitle} >Sign in or sign up with your {loginTypeEmail ? 'email' : 'phone number'}.</Text>   
                            <Text style={styles.subTitle} >OTP has been sent on the regsterd {loginTypeEmail ? 'email' : 'phone number'} - <Text>{loginTypeValue}</Text>.</Text>            
                        </View>

                        <View style={styles.otpContainer} >
                            <OTPTextView
                                ref={input}
                                containerStyle={styles.textInputContainer}
                                handleTextChange={(otp) => {setOtpValue(otp); setErrors({})}}
                                inputCount={6}
                                tintColor={colors.primary}
                                keyboardType="numeric"
                                textInputStyle={styles.textInputStyle}
                            />
                        
                            {errors.OTP && <Text style={{fontSize: 14, fontFamily: Poppins.Medium, color: colors.reddish }} >{errors?.OTP}</Text>}
                        </View>
                    </View>
                
                   

                    <View>
                        {/* <View style={{alignItems: 'center', width: '100%'}} >
                            {apiError && <Text style={{fontSize: 14, fontFamily: Poppins.Medium, color: colors.reddish }} >{apiError}</Text>}
                        </View>
                         */}
                        <PrimaryButton
                            title="Verify OTP" 
                            buttonLoader={buttonLoader}
                            onPress={() => {
                                verifyFunction()
                            }}
                        />
                    </View>

                </View>
            </ScrollView>
            {showAlertModal && 
                <AlertModal
                    alertType={alertType}
                    title={alertTile}
                    description={alertDescription}
                    onAdd={() => {setShowAlertModal(false); props.navigation.navigate('Tabbar')}}
                    closeModal={() => {setShowAlertModal(false); }} 
                />
            }
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colors.bg_color,
    },
    mainContainer: {
        padding: 20,
        flex: 1,
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    alreadyContainer: {
        paddingHorizontal: 20,
        alignItems: 'center',
        justifyContent: 'center',
    },
    alreadyText: {
        fontSize: 14,
        fontFamily: Poppins.Medium,
        color: colors.gray,
        marginTop: 8,
        textAlign: 'center',
    },
    textInputStyle: {
        height: 44,
        width: 44,
        borderRadius: 6,
        alignItems: 'center',
        justifyContent: 'center',
        borderWidth: 1,
        fontSize: 14,
        fontFamily: Poppins.Medium,
        color: colors.black,
    },
    iconContainer: {
        height: 60,
        width: 60,
        borderRadius: 30,
        backgroundColor: colors.gray,
        alignItems: 'center',
        justifyContent: 'center'
    },
    title: {
        fontSize: 18,
        fontFamily: Poppins.SemiBold,
        color: colors.black,
    },
    subTitle: {
        fontSize: 14,
        fontFamily: Poppins.SemiBold,
        color: colors.dark_gray,
        marginBottom: 10,
    },
})

export default OtpVerify