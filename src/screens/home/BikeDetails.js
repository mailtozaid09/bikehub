import React, {useState, useEffect, useRef} from 'react'
import { Text, View, SafeAreaView, StyleSheet, TouchableOpacity, Image, ScrollView, Dimensions, useColorScheme, BackHandler, ImageBackground, } from 'react-native'

import { media } from '../../global/media'
import { colors } from '../../global/colors'
import { Poppins } from '../../global/fontFamily'
import { bikes_array } from '../../global/sampleData';


import { screenHeight, screenWidth } from '../../global/constants'

import Header from '../../components/header'

import * as Animatable from 'react-native-animatable';
import { addITemToCart, deleteItemFromCart } from '../../store/modules/home/actions'
import { useDispatch, useSelector } from 'react-redux'



const BikeDetailsScreen = (props) => {
    const handleViewRef = useRef(null);
    const refRBSheet = useRef();
    const dispatch = useDispatch()

    const cart_list = useSelector(state => state.home.cart_list);
    console.log("cart_list=> ",cart_list);

    const [showAnimation, setShowAnimation] = useState(true);
    const [details, setDetails] = useState({});

    const [currentTab, setCurrentTab] = useState('Description');

    useEffect(() => {
        setDetails(props?.route?.params?.params)

        leftToRightAnimation()
    }, []);

    const addItemToCartFunc = (bike) => {
        if(checkIsIncludedInCart(bike)){
            dispatch(deleteItemFromCart(bike.id))
        }else{
            dispatch(addITemToCart({
                id: bike.id,
                bikeName: bike.name,
                bikeType: bike.type,
                bikePrice: bike.price,
                bikeIcon: bike.image,
                bikeDescription: bike.description,
                bikeSpecificaton: bike.specs,
            }))
        }
    }

    const checkIsIncludedInCart = (bike) => {
        var cartList = cart_list.some(i => i.id.includes(bike.id));

        return cartList
    }


    
    const leftToRightAnimation = () => {
        handleViewRef.current.fadeInLeft(3000).then(endState => {
            console.log(endState.finished ? 'bounce finished' : 'bounce cancelled')
            setShowAnimation(false)
        });
    }
  
    const Description = () => {
        return(
            <View>
                <Text style={styles.bikeName} >{details?.name}</Text>
                <Text numberOfLines={7} style={styles.bikeDescription} >{details?.description} The LR01 uses the same design as the most iconic bikes from PEUGEOT Cycles' 130-year history and combines it with agile, dynamic performance that's perfectly suited to navigating today's cities. As well as a lugged steel frame and iconic PEUGEOT black-and-white chequer design, this city bike also features a 16-speed Shimano Claris drivetrain.</Text>
            </View>
        )
    }

    const Specification = () => {
        return(
            <View>
                {details?.specifications && (
                    <View>
                        {details?.specifications?.map((item) => (
                            <Text style={styles.specification} >{item}</Text>
                        ))}
                    </View>
                )}
                {/* {details?.specs?.frame && <Text style={styles.specification} >Frame: <Text style={{fontFamily: Poppins.Regular, color: '#ffffff90'}} >{details?.specs?.frame}</Text></Text>}
                {details?.specs?.wheelsize && <Text style={styles.specification} >Wheel Size: <Text style={{fontFamily: Poppins.Regular, color: '#ffffff90'}} >{details?.specs?.wheelsize}</Text></Text>}
                {details?.specs?.motor && <Text style={styles.specification} >Motor: <Text style={{fontFamily: Poppins.Regular, color: '#ffffff90'}} >{details?.specs?.motor}</Text></Text>}
                {details?.specs?.battery && <Text style={styles.specification} >Battery: <Text style={{fontFamily: Poppins.Regular, color: '#ffffff90'}} >{details?.specs?.battery}</Text></Text>}
                {details?.specs?.brakes && <Text style={styles.specification} >Brakes: <Text style={{fontFamily: Poppins.Regular, color: '#ffffff90'}} >{details?.specs?.brakes}</Text></Text>}
                {details?.specs?.gearset && <Text style={styles.specification} >Gearset: <Text style={{fontFamily: Poppins.Regular, color: '#ffffff90'}} >{details?.specs?.gearset}</Text></Text>}
                {details?.specs?.suspension && <Text style={styles.specification} >Suspension: <Text style={{fontFamily: Poppins.Regular, color: '#ffffff90'}} >{details?.specs?.suspension}</Text></Text>} */}
            </View>
        )
    }

    const BikeDetailsSheet = () => {
        return(
            <View style={{flex: 1, width: '100%', alignItems: 'center'}} >
                <Image 
                    source={
                        details.image_icon 
                        ? details.image_icon 
                        : {uri: details.image}
                    } 
                    style={{height: screenWidth-100, width: screenWidth-100, resizeMode: 'contain'}} 
                    />
                <View style={styles.bottomSheetContainer} >
                
                    <View style={{ padding: 20,}} >
                        <View style={styles.sheetButtonMainContainer} >
                            <TouchableOpacity
                                activeOpacity={0.5} 
                                onPress={() => {setCurrentTab('Description')}}
                                style={[styles.sheetButtonContainer, currentTab == 'Description' ? styles.sheetButtonshadowStyle : {borderWidth: 0}]}
                            >
                                <Text style={[styles.sheetButtonText, {color: currentTab == 'Description' ? colors.primary : colors.gray} ]} >Description</Text>
                            </TouchableOpacity>
                            <TouchableOpacity
                                activeOpacity={0.5} 
                                onPress={() => {setCurrentTab('Specification')}}
                                style={[styles.sheetButtonContainer, currentTab == 'Specification' ? styles.sheetButtonshadowStyle : {borderWidth: 0}]}
                            >
                                <Text style={[styles.sheetButtonText, {color: currentTab == 'Specification' ? colors.primary : colors.gray} ]} >Specification</Text>
                            </TouchableOpacity>
                        </View>

                        {currentTab == 'Description'
                                ?
                                <Description />
                                :
                                <Specification />
                                }
                    </View>

                    <View style={styles.addToCartContainer} >
                        <View>
                            <Text style={styles.price} >$ {details?.price}</Text>
                        </View>
                        {!checkIsIncludedInCart(details) 
                        ?
                        <TouchableOpacity 
                            activeOpacity={0.5}
                            style={[styles.addToCart, styles.shadowStyle]} 
                            onPress={() => {addItemToCartFunc(details)}}
                        >
                            <Text style={styles.addToCartText} >Add To Cart</Text>
                        </TouchableOpacity>
                        :
                        <TouchableOpacity 
                            activeOpacity={0.5}
                            style={[styles.removeFromCart, styles.shadowStyle]} 
                            onPress={() => {addItemToCartFunc(details)}}
                        >
                            <Text style={styles.removeFromCartText} >Remove from Cart</Text>
                        </TouchableOpacity>
                        }
                    </View>
                </View>
            </View>
        )
    }

    return (
        <SafeAreaView style={styles.container} >
            <ImageBackground source={media.bg_image} style={{flex: 1}} >
                <Header title="Bike Details" />
               
                    <View style={styles.mainContainer} >
                        
                        {showAnimation
                        ?
                        <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center', width: '100%', height: '100%'}}  >
                           <Animatable.View ref={handleViewRef} >
                                <Image 
                                source={
                                    details.image_icon 
                                    ? details.image_icon 
                                    : {uri: details.image}
                                } 
                                style={{height: screenWidth-40, width: screenWidth-40, resizeMode: 'contain'}} />
                            </Animatable.View>
                        </View>
                        :
                        <BikeDetailsSheet />
                        }
                    </View>
                
            </ImageBackground>
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colors.bg_color,
    },
    mainContainer: {
        flex: 1,
        // padding: 20,
        alignItems: 'center',
    },
    bottomSheetContainer: {
        flex: 1,
        width: '100%',
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
        borderWidth: 1,
        alignItems: 'center',
        justifyContent: 'space-between',
        borderColor: colors.dark_gray,
        backgroundColor: colors.dark_blue,
    },
    sheetButtonMainContainer: {
        marginBottom: 20,
        width: '100%',
        flexDirection: 'row', 
        alignItems: 'center', 
        justifyContent: 'space-evenly',
    },
    sheetButtonText: {
        fontSize: 16,
        fontFamily: Poppins.Medium,
    },
    sheetButtonContainer: {
        width: 130,
        height: 44,
        borderRadius: 10,
        alignItems: 'center', 
        justifyContent: 'center',
        borderWidth: 1,
    },
    addToCartContainer: {
        width: '100%',
        height: 80,
        paddingHorizontal: 20,
        borderRadius: 40,
        alignItems: 'center', 
        flexDirection: 'row',
        justifyContent: 'space-between',
        borderWidth: 1,
        borderColor: colors.dark_gray,
    },
    addToCart: {
        width: 150,
        height: 40,
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 10,
        backgroundColor: colors.primary,
    },
    addToCartText: {
        fontSize: 14,
        color: colors.white,
        fontFamily: Poppins.Medium,
    },
    removeFromCart: {
        width: 150,
        height: 40,
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 10,
        borderWidth: 1,
        borderColor: colors.primary,
    },
    removeFromCartText: {
        fontSize: 14,
        color: colors.primary,
        fontFamily: Poppins.Medium,
    },
    price: {
        fontSize: 20,
        color: colors.white,
        fontFamily: Poppins.Medium,
    },
    specification: {
        fontSize: 16,
        color: colors.white,
        fontFamily: Poppins.SemiBold,
        marginBottom: 2,
    },
    bikeName: {
        fontSize: 16,
        color: colors.white,
        fontFamily: Poppins.Medium,
        marginBottom: 10,
    },
    bikeDescription: {
        fontSize: 18,
        color: colors.white,
        fontFamily: Poppins.Light,
    },

    sheetButtonshadowStyle: {
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.34,
        shadowRadius: 1.27,
        elevation: 8,
        borderColor: colors.primary,
        backgroundColor: colors.dark_gray
    },
    shadowStyle: {
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.34,
        shadowRadius: 1.27,
        elevation: 8,
    },
})


export default BikeDetailsScreen