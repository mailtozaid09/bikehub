import React, {useState, useEffect} from 'react'
import { Text, View, SafeAreaView, StyleSheet, TouchableOpacity, Image, ScrollView, Dimensions, useColorScheme, BackHandler, ImageBackground, } from 'react-native'

import { media } from '../../global/media'
import { colors } from '../../global/colors'
import { Poppins } from '../../global/fontFamily'
import { bikes_array } from '../../global/sampleData';

import Banner from '../../components/custom/home/Banner'
import BikeCards from '../../components/custom/home/BikeCards'
import FilterOptions from '../../components/custom/home/FilterOptions'
import SearchBar from '../../components/custom/home/SearchBar'

import { addFavItemDetails, deleteFavItemDetails } from '../../store/modules/home/actions'
import { useDispatch, useSelector } from 'react-redux'
import { getAllProducts } from '../../global/api'
import moment from 'moment'

const filterOptions = [
    {
        title: 'All',
        icon: null,
    },
    {
        title: 'Electric',
        icon: media.battery,
        iconActive: media.battery_active,
    }, 
    {
        title: 'Road',
        icon: media.road,
        iconActive: media.road_active,
    }, 
    {
        title: 'Mountain',
        icon: media.mountain,
        iconActive: media.mountain_active,
    }, 
    {
        title: 'Helmet',
        icon: media.helmet,
        iconActive: media.helmet_active,
    },
]


const HomeScreen = ({navigation}) => {
    const dispatch = useDispatch()

    const [showSearchInput, setShowSearchInput] = useState(false);
    const [searchText, setSearchText] = useState('');

    const [bikesArray, setBikesArray] = useState([]);
    const [orgBikesArray, setOrgBikesArray] = useState([]);
    const [currentOption, setCurrentOption] = useState('All');

    const favorite_list = useSelector(state => state.home.favorite_list);

    const current_user_details = useSelector(state => state.auth.current_user_details);
    
    console.log('====================================');
    console.log("current_user_details ", current_user_details);
    console.log('====================================');

    useEffect(() => {
        const unsubscribe = navigation.addListener('focus', () => {
            // setBikesArray(bikes_array)
            // setOrgBikesArray(bikes_array)
            getAllProductsFunction()
        });
  
        return unsubscribe;
    }, [navigation]);


    const getAllProductsFunction = () => {
        getAllProducts()
        .then((resp) => {
            console.log('====================================');
            console.log("getAllProducts resp =>> ", resp);
            console.log('====================================');

            // const sortedAsc = resp.sort(
            //     (objA, objB) => moment(objA.dateCreated).format('MM DD YYYY hh:mm:ss ') - moment(objB.dateCreated).format('MM DD YYYY hh:mm:ss '),
            //   );

            //   console.log('====================================');
            //   console.log('sortedAsc= > > ',sortedAsc);
            //   console.log('====================================');

            setBikesArray(resp)
            setOrgBikesArray(resp)
        })
        .catch((err) => {
            console.log('====================================');
            console.log("err =>> ", err);
            console.log('====================================');
        })
    }


    const onFilterOption = (val) => {
        setCurrentOption(val)
        if(val == 'All'){
            setBikesArray(orgBikesArray)
        }else{
            var newArray = orgBikesArray?.filter((bike) => {
                return bike.type == val
            });

            setBikesArray(newArray)
        }
    }
    

    const searchFilterFunction = (text) => {
        var bikes = bikesArray
        if (text) {
          const newData = bikes.filter(
            function (item) {
                const nameData = item?.name ? item?.name.toUpperCase() : ''.toUpperCase();
                const typeData = item?.type ? item?.type?.toUpperCase() : ''.toUpperCase();
                const textData = text.toUpperCase();
                return nameData.indexOf(textData) > -1 || typeData.indexOf(textData) > -1;
          });
          
          setBikesArray(newData);
          setSearchText(text);
        } else {

          setBikesArray(orgBikesArray);
          setSearchText(text);
        }
    };


    const addItemToFav = (bike) => {
        if(checkISFav(bike)){
            dispatch(deleteFavItemDetails(bike.name))
        }else{
            dispatch(addFavItemDetails({
                bikeName: bike.name,
                bikeType: bike.type,
                bikePrice: bike.price,
                bikeDescription: bike.description,
                bikeSpecificaton: bike.specs,
            }))
        }
    }

    const checkISFav = (bike) => {
        var favList = favorite_list.some(i => i.bikeName.includes(bike.name));

        return favList
    }

    
    const BikeListItems = () => {
        return (
            <View>
                {!searchText
                    ?
                    <>
                        <Banner />

                        <FilterOptions 
                            currentOption={currentOption}
                            filterOptions={filterOptions}
                            onFilterOption={(val) => onFilterOption(val)}
                        />


                        <BikeCards dataArr={bikesArray} addItemToFav={(bike) => addItemToFav(bike)} checkISFav={(fav) => checkISFav(fav)}  />
                    </>
                    :
                    <View>
                        <BikeCards dataArr={bikesArray} addItemToFav={(bike) => addItemToFav(bike)} checkISFav={(fav) => checkISFav(fav)} isVertical />
                    </View>
                }
            </View>
        )
    }



    return (
        <SafeAreaView style={styles.container} >
            <ImageBackground source={media.bg_image}  style={{flex: 1}} >
                <ScrollView>
                    <View style={styles.mainContainer} >

                        <SearchBar
                            placeholder="Search"
                            value={searchText}
                            showSearchInput={showSearchInput}
                            onClearSearchText={() => {setSearchText(''); setShowSearchInput(false); setBikesArray(orgBikesArray);}}
                            onChangeText={(text) => {searchFilterFunction(text)}}
                            onShowSearchBar={() => {setShowSearchInput(!showSearchInput)}}
                        />

                        <BikeListItems />

                    </View>
                </ScrollView>
            </ImageBackground>
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colors.bg_color,
    },
    mainContainer: {
        flex: 1,
        padding: 20,
        marginBottom: 20,
        alignItems: 'center',
    },
})


export default HomeScreen