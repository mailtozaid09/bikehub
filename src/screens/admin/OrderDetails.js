import React, {useState, useEffect} from 'react'
import { Text, View, SafeAreaView, StyleSheet, TouchableOpacity, Image, ScrollView, Dimensions, useColorScheme, BackHandler, ImageBackground, FlatList, } from 'react-native'

import { media } from '../../global/media'
import { colors } from '../../global/colors'
import { Poppins } from '../../global/fontFamily'
import { screenHeight, screenWidth } from '../../global/constants'
import LottieView from 'lottie-react-native'
import EmptyLottie from '../../components/lottie/EmptyLottie'
import { getAllOrders, getOrdersByUserId } from '../../global/api'
import { useSelector } from 'react-redux'
import moment from 'moment'


const OrderDetails = (props) => {

    const [orderDetails, setOrderDetails] = useState([]);
    const [loader, setLoader] = useState(true);

    useEffect(() => {
        var details = props?.route?.params?.params
        setOrderDetails(details)
        setLoader(false)
        console.log('====================================');
        console.log("View order details => ",details);
        console.log('====================================');
  
    }, []);


    const OrderDetails = () => {
        const product = orderDetails?.orderItems[0]?.product

        return(
            <View  style={[styles.cardContainer]} >
                <View style={{width: '100%'}} >
                    <Text style={styles.cardTitle}  >ORDER ID: #123456</Text>
                    <View style={styles.rowSpace} >
                        <Text style={styles.heading}>Order Placed:</Text>
                        <Text style={styles.subheading}>{moment(orderDetails?.dateOrdered).format('DD/MM/YYYY, hh:mm A')}</Text>
                    </View>
                    <View style={styles.rowSpace} >
                        <Text style={styles.heading}>Order Items: {orderDetails?.orderItems?.length} Items</Text>
                    </View>
                    <View>
                        {orderDetails?.orderItems?.map((item, idx) => (
                            <View key={idx} style={{flexDirection: 'row', marginBottom: orderDetails?.orderItems?.length - 1 == idx ? 0 : 15}} >
                                <View style={styles.imageContainer} >
                                    <Image source={{uri: item?.product?.image}} style={{height: 50, width: 50, resizeMode: 'contain'}} />
                                </View>
                                <View style={{flex: 1, justifyContent: 'center'}} >
                                    <Text style={styles.heading}>{item?.product?.name}</Text>
                                    <Text  numberOfLines={3} style={styles.subheading}>{item?.product?.description}</Text>
                                </View>
                            </View>
                        ))}
                    </View>
                </View>
            </View>
        )
    } 



    const OrderInfo = () => {
        return(
            <View style={[styles.cardContainer]} >
                <View style={{width: '100%'}} >
                    <Text style={styles.cardTitle}  >Order Information</Text>
                    <View style={styles.rowSpace} >
                        <Text style={styles.heading}>Subtotal:</Text>
                        <Text style={styles.subheading}>$ dsad</Text>
                    </View>
                    <View style={styles.rowSpace}>
                        <Text style={styles.heading}>Delivery Fee:</Text>
                        <Text style={[styles.subheading,  {textDecorationLine: true ? 'line-through' : 'none'}]} >$ dsds</Text>
                    </View>
                    <View style={styles.rowSpace} >
                        <Text style={styles.heading}>Discount:</Text>
                        <Text style={styles.subheading}  >$ dsds</Text>
                    </View>
                    <View style={[styles.rowSpace, {paddingTop: 10, marginTop: 10, borderTopWidth: 1, borderColor: colors.dark_gray}]} >
                        <Text style={styles.heading}>Total:</Text>
                        <Text style={styles.subheading}>$ dsad</Text>
                    </View>
                </View>
            </View>
        )
    } 

    const ShippingDetails = () => {
        return(
            <View style={[styles.cardContainer]} >
                <View style={{width: '100%'}} >
                    <Text style={styles.cardTitle}  >Shipping Details</Text>
                    <View style={styles.rowSpace} >
                        <Text style={styles.heading}  >Name:</Text>
                        <Text style={styles.subheading}  >{orderDetails?.address?.street}</Text>
                    </View>
                    <View style={styles.rowSpace} >
                        <Text style={styles.heading}  >Email:</Text>
                        <Text style={styles.subheading}  >{orderDetails?.address?.street}</Text>
                    </View>
                    <View style={styles.rowSpace} >
                        <Text style={styles.heading}  >Mobile:</Text>
                        <Text style={[styles.subheading, ]}  >{orderDetails?.address?.street}</Text>
                    </View>
                    <View style={[styles.rowSpace, {flex: 1,}]} >
                        <Text style={styles.heading}  >Address:</Text>
                       <View style={{flex: 1, alignItems: 'flex-end', marginLeft: 35}} >
                         <Text style={styles.subheading}  >{orderDetails?.address?.street}, {orderDetails?.address?.apartment}, {orderDetails?.address?.city}, {orderDetails?.address?.zip}</Text>
                       </View>
                    </View>
                </View>
            </View>
        )
    } 

    const PaymentMode = () => {
        return(
            <View style={[styles.cardContainer]} >
                <Text style={styles.heading}  >Payment Mode</Text>
                <Text style={styles.subheading}  >Cash on Delivery</Text>
            </View>
        )
    } 
    
    
    return (
        <SafeAreaView style={styles.container} >
            <ImageBackground source={media.bg_image} style={{flex: 1}} >
                {!loader ? (
                    <ScrollView>
                        <View style={{padding: 20}} >
                            <OrderDetails />
                            <ShippingDetails />
                            <PaymentMode />
                            <OrderInfo />
                            
                        </View>
                    </ScrollView>
                ):(
                    <Text>Dsads</Text>
                )}
            
            </ImageBackground>
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colors.bg_color,
    },
    mainContainer: {
        flex: 1,
        padding: 20,
        alignItems: 'center',
        justifyContent: 'center'
    },
    imageContainer: {
        height: 70,
        width: 70,
        borderRadius: 10,
        marginRight: 12,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#373f53'
    },
    cardContainer: {
        width: '100%',
        padding: 15,
        borderRadius: 14,
        marginBottom: 15,
        borderWidth: 1,
        borderColor: colors.dark_gray,
        backgroundColor: colors.dark_blue,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.34,
        shadowRadius: 1.27,
        elevation: 8,
    },
    rowSpace: {
        flexDirection: 'row',
        alignItems: 'center',
        marginBottom: 4,
        justifyContent: 'space-between',
    },
    cardTitle: {
        fontSize: 18,
        fontFamily: Poppins.Medium,
        color: '#FFFFFF',
    },
    heading: {
        fontSize: 16,
        fontFamily: Poppins.Medium,
        color: '#FFFFFF',
    },
    subheading: {
        fontSize: 16,
        fontFamily: Poppins.Medium,
        color: '#FFFFFF90',
    },
    viewDetails: {
        borderWidth: 1,
        borderRadius: 20,
        height: 40,
        paddingHorizontal: 15,
        alignItems: 'center', 
        justifyContent: 'center',
        borderColor: colors.primary,
    },
    viewDetailsText: {
        fontSize: 14,
        fontFamily: Poppins.Regular,
        color: colors.primary,
    }
})


export default OrderDetails
