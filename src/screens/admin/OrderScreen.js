import React, {useState, useEffect} from 'react'
import { Text, View, SafeAreaView, StyleSheet, TouchableOpacity, Image, ScrollView, Dimensions, useColorScheme, BackHandler, ImageBackground, FlatList, } from 'react-native'

import { media } from '../../global/media'
import { colors } from '../../global/colors'
import { Poppins } from '../../global/fontFamily'
import { screenHeight, screenWidth } from '../../global/constants'
import LottieView from 'lottie-react-native'
import EmptyLottie from '../../components/lottie/EmptyLottie'
import { getAllOrders } from '../../global/api'
import { useSelector } from 'react-redux'
import moment from 'moment'


const OrderScreen = (props) => {

    const [orderDetails, setOrderDetails] = useState([]);

    const current_user_details = useSelector(state => state.auth.current_user_details);
    
    useEffect(() => {
        console.log('====================================');
        console.log("getAllOrdersFunction");
        console.log('====================================');
        getAllOrdersFunction()
    }, []);

    const getAllOrdersFunction = () => {
        
        getAllOrders()
        .then((resp) => {
            setOrderDetails(resp)
            console.log('====================================');
            console.log("resp => ", resp);
            console.log('====================================');
        })
        .catch((err) => {
            console.log('====================================');
            console.log("err => ", err);
            console.log('====================================');
        })
    }

    const OrderDetails = ({item, index}) => {
        const product = item?.orderItems[0]?.product

        console.log('====================================');
        console.log("product > ",product);
        console.log('====================================');
        return(
            <View key={index} style={[styles.cardContainer]} >
                <View style={{width: '100%'}} >
                    <Text style={styles.cardTitle}  >ORDER ID: #123456</Text>
                    <View style={styles.rowSpace} >
                        <Text style={styles.heading}>Order Placed:</Text>
                        <Text style={styles.subheading}>{moment(item?.dateOrdered).format('DD/MM/YYYY, hh:mm A')}</Text>
                    </View>
                    <View style={{flexDirection: 'row', marginBottom: 10}} >
                        <View style={styles.imageContainer} >
                            <Image source={{uri: product?.image}} style={{height: 50, width: 50, resizeMode: 'contain'}} />
                        </View>
                        <View style={{flex: 1, justifyContent: 'center'}} >
                            <Text style={styles.heading}>{product?.name}</Text>
                            {item?.orderItems?.length > 1 && <Text style={[styles.subheading, {fontSize: 14}]}>+ {item?.orderItems?.length - 1} more items in the order</Text>}
                        </View>
                    </View>
                    <View style={styles.rowSpace} >
                        <Text style={styles.heading}>Total Price: {item?.totalPrice}</Text>
                    </View>
                    <View style={styles.rowSpace} >
                        <Text style={[styles.heading, {color: colors.reddish}]}>{item?.status}</Text>
                        <TouchableOpacity 
                            activeOpacity={0.5}
                            onPress={() => {props.navigation.navigate('OrderDetails', {params: item})}}
                            style={styles.viewDetails} >
                            <Text style={styles.viewDetailsText}  >View Details</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
        )
    } 
    
    return (
        <SafeAreaView style={styles.container} >
            <ImageBackground source={media.bg_image} style={{flex: 1}} >
                <View style={{padding: 20}} >
                    <FlatList
                        data={orderDetails}
                        ListEmptyComponent={<EmptyLottie />}
                        renderItem={({item, index}) => (
                            <OrderDetails item={item} index={item} />
                        )}
                    />
                </View>
                    

            </ImageBackground>
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colors.bg_color,
    },
    mainContainer: {
        flex: 1,
        padding: 20,
        alignItems: 'center',
        justifyContent: 'center'
    },
    imageContainer: {
        height: 70,
        width: 70,
        borderRadius: 10,
        marginRight: 12,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#373f53'
    },
    cardContainer: {
        width: '100%',
        padding: 15,
        borderRadius: 14,
        marginBottom: 15,
        borderWidth: 1,
        borderColor: colors.dark_gray,
        backgroundColor: colors.dark_blue,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.34,
        shadowRadius: 1.27,
        elevation: 8,
    },
    rowSpace: {
        flexDirection: 'row',
        alignItems: 'center',
        marginBottom: 4,
        justifyContent: 'space-between',
    },
    cardTitle: {
        fontSize: 18,
        fontFamily: Poppins.Medium,
        color: '#FFFFFF',
    },
    heading: {
        fontSize: 16,
        fontFamily: Poppins.Medium,
        color: '#FFFFFF',
    },
    subheading: {
        fontSize: 16,
        fontFamily: Poppins.Medium,
        color: '#FFFFFF90',
    },
    viewDetails: {
        borderWidth: 1,
        borderRadius: 20,
        height: 40,
        paddingHorizontal: 15,
        alignItems: 'center', 
        justifyContent: 'center',
        borderColor: colors.primary,
    },
    viewDetailsText: {
        fontSize: 14,
        fontFamily: Poppins.Regular,
        color: colors.primary,
    }
})


export default OrderScreen
