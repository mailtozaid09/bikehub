import React, {useState, useEffect} from 'react'
import { Text, View, SafeAreaView, StyleSheet, TouchableOpacity, Image, ScrollView, Dimensions, useColorScheme, BackHandler, ImageBackground, TextInput, } from 'react-native'

import { media } from '../../global/media'
import { colors } from '../../global/colors'
import { Poppins } from '../../global/fontFamily'
import { screenHeight, screenWidth } from '../../global/constants'
import LottieView from 'lottie-react-native'
import EmptyLottie from '../../components/lottie/EmptyLottie'
import Icon from '../../utils/icons'
import { bikes_array } from '../../global/sampleData'
import ProductCards from '../../components/custom/admin/ProductCards'
import { getAllProducts } from '../../global/api'


const operations = [
    {
        id: 1,
        title: 'Orders',
        iconName: 'bag-handle',
        iconType: 'Ionicons',
        goToScreen: 'OrderScreen',
    },
    {
        id: 2,
        title: 'Products',
        iconName: 'plus',
        iconType: 'Feather',
        goToScreen: 'AddProductScreen',
    },
    {
        id: 1,
        title: 'Categories',
        iconName: 'plus',
        iconType: 'Feather',
        goToScreen: 'AddCategoryScreen',
    },
]

const AdminScreen = ({navigation}) => {

    const [searchText, setSearchText] = useState('');
    const [bikesArray, setBikesArray] = useState([]);
    const [orgBikesArray, setOrgBikesArray] = useState([]);

    useEffect(() => {
        const unsubscribe = navigation.addListener('focus', () => {
            // setBikesArray(bikes_array)
            // setOrgBikesArray(bikes_array)
            getAllProductsFunction()
        });
  
        return unsubscribe;
    }, [navigation]);



    const getAllProductsFunction = () => {
        getAllProducts()
        .then((resp) => {
            console.log('====================================');
            console.log("resp =>> ", resp);
            console.log('====================================');
            setBikesArray(resp)
            setOrgBikesArray(resp)
        })
        .catch((err) => {
            console.log('====================================');
            console.log("err =>> ", err);
            console.log('====================================');
        })
    }


    const searchFilterFunction = (text) => {
        var bikes = bikesArray
        if (text) {
          const newData = bikes.filter(
            function (item) {
                const nameData = item?.name ? item?.name.toUpperCase() : ''.toUpperCase();
                const typeData = item?.type ? item?.type?.toUpperCase() : ''.toUpperCase();
                const textData = text.toUpperCase();
                return nameData.indexOf(textData) > -1 || typeData.indexOf(textData) > -1;
          });
          
          setBikesArray(newData);
          setSearchText(text);
        } else {

          setBikesArray(orgBikesArray);
          setSearchText(text);
        }
    };

    const AdminOperations = () => {
        return(
            <View style={styles.operationMainContainer} >
                {operations?.map((item, index) => (
                    <TouchableOpacity 
                        activeOpacity={0.5}
                        onPress={() => {navigation.navigate(item.goToScreen)}}
                        style={styles.operationContainer} key={index} >
                        <Icon type={item.iconType} name={item.iconName}  size={20} style={{marginRight: 4, marginBottom: 2}} color={colors.white} />
                        <Text style={styles.operationTitle} >{item.title}</Text>
                    </TouchableOpacity>
                ))}
            </View>
        )
    }
    
    return (
        <SafeAreaView style={styles.container} >
            <ImageBackground source={media.bg_image} style={{flex: 1, }} >
                    <View style={styles.mainContainer} >
                        <AdminOperations />

                    <View style={styles.searchContainer} >
                        <TextInput
                            value={searchText}
                            placeholder='Search'
                            style={styles.inputStyle} 
                            placeholderTextColor={colors.gray}
                            onChangeText={(text) => {searchFilterFunction(text)}}
                        />

                        {searchText && 
                        <TouchableOpacity
                            activeOpacity={0.5}
                            onPress={() => {setSearchText(''); setBikesArray(orgBikesArray);}}
                        >
                            <Icon type={'AntDesign'} name={'close'}  size={24} color={colors.white} />
                        </TouchableOpacity>
                        }
                    </View>

                        <ProductCards dataArr={bikesArray} />
                    </View>
            </ImageBackground>
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colors.bg_color,
    },
    mainContainer: {
        flex: 1,
        padding: 20,
        width: '100%',
        alignItems: 'center',
        //justifyContent: 'center',
    },
    operationMainContainer: {
        width: '100%',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    operationContainer: {
        backgroundColor: colors.primary,
        flexDirection: 'row',
        alignItems: 'center',
        borderRadius: 6,
        paddingVertical: 12,
        paddingHorizontal: 10,
        marginBottom: 20,
        alignItems: 'center',
        justifyContent: 'center',
    },
    operationTitle: {
        fontSize: 12,
        fontFamily: Poppins.SemiBold,
        color: colors.white,
    },
    searchContainer: {
        height: 50,
        paddingHorizontal: 15,
        borderWidth: 0.5,
        borderColor: colors.gray,
        borderRadius: 10,
        marginRight: 14,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    inputStyle: {
        fontSize: 16,
        alignItems: 'center',
        justifyContent: 'center',
        fontFamily: Poppins.Medium,
        color: colors.white,
        flex: 1,
    },
})


export default AdminScreen
