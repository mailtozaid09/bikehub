import React, {useState, useEffect} from 'react'
import { Text, View, SafeAreaView, StyleSheet, TouchableOpacity, Image, ScrollView, Dimensions, useColorScheme, BackHandler, ImageBackground, } from 'react-native'

import { media } from '../../global/media'
import { colors } from '../../global/colors'
import { Poppins } from '../../global/fontFamily'
import { screenHeight, screenWidth } from '../../global/constants'

import Icon from '../../utils/icons'
import Input from '../../components/input'
import PrimaryButton from '../../components/button/PrimaryButton'

import { launchImageLibrary } from 'react-native-image-picker'
import { addNewProductDetails, getAllCategories, getAllProducts } from '../../global/api'

import DropDownPicker from 'react-native-dropdown-picker';


const AddProductScreen = ({navigation}) => {


    const [form, setForm] = useState({})
    const [errors, setErrors] = useState({})

    const [filePath, setFilePath] = useState('');
    const [fileType, setFileType] = useState('');
    const [mainImage, setMainImage] = useState('');

    const [buttonLoader, setButtonLoader] = useState(false);

    const [open, setOpen] = useState(false);
    const [value, setValue] = useState(null);
    const [items, setItems] = useState(null);

    const [loading, setLoading] = useState(true);

    useEffect(() => {
        getAllCategoriesFunction()
    }, []);

    const getAllCategoriesFunction = () => {
        getAllCategories()
        .then((resp) => {

            let partialArrayItems = resp?.map((item) => {
                return { 
                    id: item.id, 
                    label: item.name,
                    value: item.id, 
                };
            });
            setItems(partialArrayItems)
        })
        .catch((err) => {
            console.log('====================================');
            console.log("err => ", err);
            console.log('====================================');
        })
    }

    const onChange = ({name, value}) => {
        setForm({...form, [name]: value})
    }


    const addProduct = () => {
        console.log("form-> > ", form);

        setButtonLoader(true)
      
        if(checkIsValid()){
            console.log("checkIsValid > ");
            let formData = new FormData();

            formData.append("image", {
                uri: filePath,
                type: fileType,
                name: fileType
            });


            formData.append("category", value);
            formData.append("name", form.name);
            formData.append("description", form?.description);
            formData.append('specifications', JSON.stringify(["test specifications"]));
            formData.append("price", form?.price);
            formData.append("type", form?.type);


            addNewProductDetails(formData)
                .then(resp => {
                    console.log("product upload SUCCESS ! ! ! ", resp)
                    setButtonLoader(false);
                    navigation.goBack()
            })
            .catch(err => {
                console.log("product upload err => ", err);
                setButtonLoader(false)
            })

        }else{
            setButtonLoader(false)
        }

    }


    const checkIsValid = () => {
        var isValid = true

        if(!form.name){
            console.log("Please enter a valid product name!");
            isValid = false
            
            setErrors((prev) => {
                return {...prev, name: 'Please enter a valid product name!'}
            })
        }

        if(!form.description){
            console.log("Please enter a valid product description!");
            isValid = false
            
            setErrors((prev) => {
                return {...prev, description: 'Please enter a valid product description!'}
            })
        }


        if(!form.specifications){
            console.log("Please enter a valid product specifications!");
            isValid = false
            
            setErrors((prev) => {
                return {...prev, specifications: 'Please enter a valid product specifications!'}
            })
        }

        if(!form.price){
            console.log("Please enter a valid product price!");
            isValid = false
            
            setErrors((prev) => {
                return {...prev, price: 'Please enter a valid product price!'}
            })
        }
        
        if(!form.type){
            console.log("Please enter a valid product type!");
            isValid = false
            
            setErrors((prev) => {
                return {...prev, type: 'Please enter a valid product type!'}
            })
        }

        if(!value){
            console.log("Please enter a valid product category!");
            isValid = false
            
            setErrors((prev) => {
                return {...prev, category: 'Please enter a valid product category!'}
            })
        }

        return isValid

    }

    const chooseFile = () => {
        let options = {
            includeBase64: true,
            mediaType: 'photo',
            maxWidth: 300,
            maxHeight: 550,
            quality: 1,
        };
        launchImageLibrary(options, (response) => {

        if (response.didCancel) {
            alert('User cancelled camera picker');
            return;
        } else if (response.errorCode == 'camera_unavailable') {
            alert('Camera not available on device');
            return;
        } else if (response.errorCode == 'permission') {
            alert('Permission not satisfied');
            return;
        } else if (response.errorCode == 'others') {
            alert(response.errorMessage);
            return;
        }
        // console.log('====================================');
        // console.log("fileName - > ", response?.assets[0].fileName);
        // console.log("fileSize - > ", response?.assets[0].fileSize);
        // console.log("originalPath - > ", response?.assets[0].originalPath);
        // console.log("type - > ", response?.assets[0].type);
        // console.log("uri - > ", response?.assets[0].uri);
        // console.log('====================================');
        setFilePath(response?.assets[0]?.uri);
        setMainImage(response?.assets[0]?.uri)
        setFileType(response?.assets[0]?.type)
        });
    };


    return (
        <SafeAreaView style={styles.container} >
            <ImageBackground source={media.bg_image} style={{flex: 1, }} >
                <View style={styles.mainContainer} >

                    <ScrollView style={{width: screenWidth, paddingHorizontal: 20, }} >
                        <View style={{alignItems: 'center'}} >
                            <View style={{height: 120, width: 120, borderRadius: 60, borderWidth: 3, marginVertical: 14, alignItems: 'center', justifyContent: 'center', borderColor: colors.primary}} >
                                <Image 
                                    source={
                                        !mainImage 
                                        ? media.admin_active
                                        : {uri: mainImage}
                                    }
                                    style={{height: 100, width: 100, borderRadius: 50}} 
                                />

                                <TouchableOpacity  
                                    onPress={() => {
                                        chooseFile();
                                    }}
                                    style={styles.cameraContainer} > 
                                    <Icon type={"MaterialCommunityIcons"} name={"camera-wireless-outline"}  size={20} color={colors.primary} />
                                </TouchableOpacity>
                            </View>                    

                            <Input
                                label="Name"
                                placeholder="Enter your product name"
                                keyboardType="default"
                                error={errors.name}
                                bgColor={colors.dark_blue}
                                onChangeText={(text) => {onChange({name: 'name', value: text,}); setErrors({}); }}
                            />

                            <Input
                                label="Description"
                                placeholder="Enter your product description"
                                keyboardType="default"
                                error={errors.description}
                                bgColor={colors.dark_blue}
                                onChangeText={(text) => {onChange({name: 'description', value: text,}); setErrors({}); }}
                            />

                            <Input
                                label="Specifications"
                                placeholder="Enter your product specifications"
                                keyboardType="default" 
                                error={errors.specifications}
                                bgColor={colors.dark_blue}
                                onChangeText={(text) => {onChange({name: 'specifications', value: text,}); setErrors({}); }}
                            />

                            <Input
                                label="Price"
                                placeholder="Enter your product price"
                                keyboardType="number-pad"
                                error={errors.price}
                                bgColor={colors.dark_blue}
                                onChangeText={(text) => {onChange({name: 'price', value: text,}); setErrors({}); }}
                            />
                            
                            <Input
                                label="Type"
                                placeholder="Enter your product type"
                                keyboardType="default"
                                error={errors.type}
                                bgColor={colors.dark_blue}
                                onChangeText={(text) => {onChange({name: 'type', value: text,}); setErrors({}); }}
                            />

                            {items && <View>
                                <Text style={styles.label} >Category</Text>
                                <DropDownPicker
                                    open={open}
                                    value={value}
                                    items={items}
                                    setOpen={setOpen}
                                    setValue={setValue}
                                    setItems={setItems}
                                    arrowIconStyle={{tintColor: colors.white}}
                                    placeholder='Choose your product category'
                                    placeholderStyle={{ color: colors.white, textAlign: 'left' }}
                                    dropDownContainerStyle={{ backgroundColor: "#dfdfdf", }}
                                    selectedItemContainerStyle={{ backgroundColor: colors.primary, }}
                                    selectedItemLabelStyle={{ color: colors.white }}
                                    labelStyle={{ color: colors.white }}
                                    style={{backgroundColor: colors.dark_blue, borderWidth: 0.5, borderColor: colors.gray, }}
                                    textStyle={{fontSize: 16, fontFamily: Poppins.Medium, }}
                                />
                            </View>}

                        </View>

                    </ScrollView>

                    <PrimaryButton
                        title="Add Product"
                        buttonLoader={buttonLoader}
                        onPress={() => {addProduct()}}
                    />

                </View>
            </ImageBackground>
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colors.bg_color,
    },
    mainContainer: {
        flex: 1,
        padding: 20,
        paddingBottom: 0,
        width: '100%',
        alignItems: 'center',
    },
    title: {
        fontSize: 16,
        fontFamily: Poppins.SemiBold,
        color: colors.white,
    },
    cameraContainer: {
        position: 'absolute', 
        bottom: -20, 
        right: 5,
        height: 40, 
        width: 40, 
        alignItems: 'center', 
        justifyContent: 'center', 
        borderRadius: 20, 
        borderWidth: 1, 
        borderColor: colors.dark_gray,
        backgroundColor: colors.dark_blue
    },
    label: {
        fontSize: 14,
        fontFamily: Poppins.SemiBold,
        color: colors.white,
        marginBottom: 4,
    },
})


export default AddProductScreen
