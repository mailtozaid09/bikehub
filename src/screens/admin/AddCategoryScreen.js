import React, {useState, useEffect} from 'react'
import { Text, View, SafeAreaView, StyleSheet, TouchableOpacity, Image, ScrollView, Dimensions, useColorScheme, BackHandler, ImageBackground, } from 'react-native'

import { media } from '../../global/media'
import { colors } from '../../global/colors'
import { Poppins } from '../../global/fontFamily'
import { screenHeight, screenWidth } from '../../global/constants'
import LottieView from 'lottie-react-native'
import EmptyLottie from '../../components/lottie/EmptyLottie'
import Icon from '../../utils/icons'


const AddCategoryScreen = (props) => {


    useEffect(() => {

    }, []);

    
    return (
        <SafeAreaView style={styles.container} >
            <ImageBackground source={media.bg_image} style={{flex: 1, }} >
                <ScrollView>
                    <View style={styles.mainContainer} >
                
                        <Text>AddCategoryScreen</Text>
                    </View>
                </ScrollView>
            </ImageBackground>
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colors.bg_color,
    },
    mainContainer: {
        flex: 1,
        padding: 20,
        width: '100%',
        alignItems: 'center',
    },
    title: {
        fontSize: 16,
        fontFamily: Poppins.SemiBold,
        color: colors.white,
    }
})


export default AddCategoryScreen
