import React, {useState, useEffect} from 'react'
import { Text, View, SafeAreaView, StyleSheet, TouchableOpacity, Image, ScrollView, Dimensions, useColorScheme, BackHandler, ImageBackground, } from 'react-native'

import { media } from '../../global/media'
import { colors } from '../../global/colors'
import { Poppins } from '../../global/fontFamily'
import { screenHeight } from '../../global/constants'

import Header from '../../components/header'
import { useSelector } from 'react-redux'
import PrimaryButton from '../../components/button/PrimaryButton'
import { useNavigation } from '@react-navigation/native'

const CheckoutScreen = (props) => {

    const navigation = useNavigation();

    const cart_list = useSelector(state => state.home.cart_list);

    const [totalPrice, setTotalPrice] = useState(0);
    const [subTotalPrice, setSubTotalPrice] = useState(0);
    const [delivieryPrice, setDelivieryPrice] = useState(120);
    const [discountPrice, setDiscountPrice] = useState(300);
    const [isDeliveryFree, setIsDeliveryFree] = useState(false);

    const [shippingAdd, setShippingAdd] = useState('1703, Single Business Tower, \nBusiness Bay, \nDubai, UAE \n+971 4 457 7824 \nwww.klipit.co');

    useEffect(() => {
        var price = cart_list.reduce((n, {bikePrice}) => n + bikePrice, 0);
        
        var deliveryFee = price >= 1000 ? 0 : 120

        var total_price = price + deliveryFee
        
        setTotalPrice(total_price);
        setSubTotalPrice(price);
        setIsDeliveryFree(deliveryFee == 0 ? true : false);
    }, [cart_list || discountPrice]);


    const AddressDetails = () => {
        return(
            <View style={[{width: '100%', marginTop: 25,}, styles.checkoutContainer]} >
                <View style={{width: '100%'}} >
                    <View >
                        <Text style={styles.heading}  >Shipping Address</Text>
                        <Text style={styles.subheading}  >{shippingAdd}</Text>
                    </View>
                </View>
            </View>
        )
    }

    const CartDetails = () => {
        return(
            <View style={[{width: '100%', marginTop: 25,}, styles.checkoutContainer]} >
                <View style={{width: '100%'}} >
                    <View style={styles.rowSpace} >
                        <Text style={styles.heading}  >Subtotal:</Text>
                        <Text style={styles.subheading}  >$ {subTotalPrice}</Text>
                    </View>
                    <View style={styles.rowSpace} >
                        <Text style={styles.heading}  >Delivery Fee:</Text>
                        <Text style={[styles.subheading, {textDecorationLine: subTotalPrice >= 1000 ? 'line-through' : 'none'}]}  >$ {delivieryPrice}</Text>
                    </View>
                    <View style={styles.rowSpace} >
                        <Text style={styles.heading}  >Discount:</Text>
                        <Text style={styles.subheading}  >$ {discountPrice}</Text>
                    </View>
                    <View style={[styles.rowSpace, {marginTop: 15,}]} >
                        <Text style={styles.heading}  >Total:</Text>
                        <Text style={styles.subheading}  >$ {totalPrice-discountPrice}</Text>
                    </View>
                </View>
            </View>
        )
    }


    
    return (
        <SafeAreaView style={styles.container} >
            <ImageBackground source={media.bg_image} style={{flex: 1}} >
                    <Header title="Checkout" />

                    <View style={styles.mainContainer} >
                        <View style={{width: '100%'}} >
                            <CartDetails />

                            <AddressDetails />
                        </View>

                        <PrimaryButton
                            title="Place Order"
                            onPress={() => {navigation.navigate('OrderConfirmation')}}
                        />
                        
                    </View>
            </ImageBackground>
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colors.bg_color,
    },
    mainContainer: {
        flex: 1,
        padding: 20,
        paddingTop: 0,
        alignItems: 'center',
        justifyContent: 'space-between',
    },
    freeShippingText: {
        fontSize: 14,
        fontFamily: Poppins.Regular,
        color: '#FFFFFF60',
        marginBottom: 10,
        textAlign: 'center',
    },
    rowSpace: {
        flexDirection: 'row',
        alignItems: 'center',
        marginBottom: 4,
        justifyContent: 'space-between',
    },
    heading: {
        fontSize: 18,
        fontFamily: Poppins.Medium,
        color: '#FFFFFF',
    },
    subheading: {
        fontSize: 18,
        fontFamily: Poppins.Medium,
        color: '#FFFFFF90',
    },
    checkoutContainer: {
        width: '100%',
        padding: 20,
        borderRadius: 14,
        borderWidth: 1,
        borderColor: colors.dark_gray,
        backgroundColor: colors.dark_blue,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.34,
        shadowRadius: 1.27,
        elevation: 8,
    },
})


export default CheckoutScreen