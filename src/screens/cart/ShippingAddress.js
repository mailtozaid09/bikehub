import React, {useState, useEffect, useRef} from 'react'
import { Text, View, SafeAreaView, StyleSheet, TouchableOpacity, Image, ScrollView, Dimensions, useColorScheme, BackHandler, ImageBackground, } from 'react-native'

import { media } from '../../global/media'
import { colors } from '../../global/colors'
import { Poppins } from '../../global/fontFamily'
import { screenHeight, screenWidth } from '../../global/constants'

import { useDispatch, useSelector } from 'react-redux'
import { useNavigation } from '@react-navigation/native'
import { currentShippingAddress } from '../../store/modules/home/actions'

import Icon from '../../utils/icons'
import Header from '../../components/header'
import PrimaryButton from '../../components/button/PrimaryButton'
import ActionButton from '../../components/button/ActionButton'

import AlertModal from '../../components/modal/AlertModal'
import EmptyLottie from '../../components/lottie/EmptyLottie'

import { deleteShippingAddress, getAllShippingAddress } from '../../global/api'


const ShippingAddress = (props) => {

    const dispatch = useDispatch()

    const navigation = useNavigation();

    const [deleteID, setDeleteID] = useState(null);
    const [currentAdd, setCurrentAdd] = useState(null);
    const [showAlertModal, setShowAlertModal] = useState(false);

    const [alertType, setAlertType] = useState('');
    const [alertTile, setAlertTile] = useState('');
    const [alertDescription, setAlertDescription] = useState('');

    const [shippingAddressList, setShippingAddressList] = useState([]);

    useEffect(() => {
        const unsubscribe = navigation.addListener('focus', () => {
            getAddressFunction()
        });
  
        return unsubscribe;
    }, [navigation]);

    const getAddressFunction = () => {
        getAllShippingAddress()
        .then((resp) => {
            setShippingAddressList(resp)
        })
        .catch((err) => {
            console.log('====================================');
            console.log("shipping address err > ", err);
            console.log('====================================');
        })
    }
    

    const setCurrentAddress = () => {
        dispatch(currentShippingAddress(currentAdd))
    }

    const openDeleteModal = (id) => {
        setDeleteID(id);
        setAlertType('Delete')
        setAlertTile("Are you sure!");
        setAlertDescription("You want to delete?");
        setShowAlertModal(true)
    }

    const deleteAddressFunction = () => {
        if(deleteID == currentAdd?.id){
            setCurrentAdd(null)
        }
        deleteShippingAddress(deleteID)
        .then((resp) => {
            getAddressFunction()
        })
        .catch((err) => {
            console.log('====================================');
            console.log("delete shipping address err > ", err);
            console.log('====================================');
        })

        //dispatch(deleteShippingAddress(deleteID))
    }


    const AddressDetails = ({item, index}) => {
        return(
            <TouchableOpacity 
                key={index} 
                activeOpacity={0.5}
                onPress={() => {setCurrentAdd(item)}}
                style={[{width: '100%', marginBottom: 15, borderColor: currentAdd == item ? colors.primary : colors.dark_gray }, styles.addressContainer]} 
            >
                <View style={{width: '100%'}} >
                    <View >
                        <View style={{flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between'}} >
                            <Text style={styles.heading}  >Shipping Address</Text>
                            <TouchableOpacity
                                activeOpacity={0.5}
                                onPress={() => {openDeleteModal(item.id)}}
                            >
                                <Icon type={'Entypo'} name={'trash'}  size={24} color={colors.white} />
                            </TouchableOpacity>
                        </View>
                        <Text style={styles.subheading}  >{item?.name}</Text>
                        <Text style={styles.subheading}  >{item?.email}</Text>
                        <Text style={styles.subheading}  >{item?.phone}</Text>
                        <Text style={styles.subheading}  >{item?.street}, {item?.apartment}</Text>
                        <Text style={styles.subheading}  >{item?.city}, {item?.zip}</Text>
                    </View>
                </View>
            </TouchableOpacity>
        )
    }

    
    return (
        <SafeAreaView style={styles.container} >
            <ImageBackground source={media.bg_image} style={{flex: 1}} >
                    <Header title="Shipping Address" />

                    <View style={styles.mainContainer} >
                        {shippingAddressList?.length != 0 ? (
                        <>
                        <ScrollView style={{width: screenWidth, paddingHorizontal: 20,}} >
                            {shippingAddressList?.map((item, index) => (
                                <AddressDetails item={item} index={index} />
                            ))}
                            
                        </ScrollView>

                        
                        
                        <PrimaryButton
                            title="Continue"
                            disabled={currentAdd ? false : true}
                            onPress={() => {
                                setCurrentAddress();
                                navigation.navigate('ConfirmOrder');
                            }}
                        />
                        </>
                    )
                    :
                    <EmptyLottie />
                    }
                        
                    </View>
                    <View style={{bottom: shippingAddressList?.length == 0 ? 10 : 70,}} >
                        <ActionButton
                            disabled={currentAdd ? false : true}
                            onPress={() => {
                                navigation.navigate('AddShippingAddress');
                            }}
                        />
                    </View>


                    {showAlertModal && 
                        <AlertModal
                            alertType={alertType}
                            title={alertTile}
                            description={alertDescription}
                            onDelete={() => {
                                setShowAlertModal(false); 
                                deleteAddressFunction();
                            }}
                            onCancel={() => {setShowAlertModal(false); }} 
                        />
                    }
            </ImageBackground>
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colors.bg_color,
    },
    mainContainer: {
        flex: 1,
        width: '100%',
        padding: 20,
        paddingBottom: 10,
        alignItems: 'center',
        justifyContent: 'space-between',
    },
    freeShippingText: {
        fontSize: 14,
        fontFamily: Poppins.Regular,
        color: '#FFFFFF60',
        marginBottom: 10,
        textAlign: 'center',
    },
    rowSpace: {
        flexDirection: 'row',
        alignItems: 'center',
        marginBottom: 4,
        justifyContent: 'space-between',
    },
    heading: {
        fontSize: 18,
        fontFamily: Poppins.Medium,
        color: '#FFFFFF',
    },
    subheading: {
        fontSize: 18,
        fontFamily: Poppins.Medium,
        color: '#FFFFFF90',
    },
    addressContainer: {
        width: '100%',
        padding: 20,
        borderRadius: 14,
        borderWidth: 1,
        backgroundColor: colors.dark_blue,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.34,
        shadowRadius: 1.27,
        elevation: 8,
    },
})


export default ShippingAddress