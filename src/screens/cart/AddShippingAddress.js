import React, {useState, useEffect} from 'react'
import { Text, View, SafeAreaView, StyleSheet, TouchableOpacity, Image, ScrollView, Dimensions, useColorScheme, BackHandler, ImageBackground, TextInput, } from 'react-native'

import { media } from '../../global/media'
import { colors } from '../../global/colors'
import { Poppins } from '../../global/fontFamily'

import { screenHeight, screenWidth } from '../../global/constants'

import { useDispatch, useSelector } from 'react-redux'

import Header from '../../components/header'
import Input from '../../components/input'
import PrimaryButton from '../../components/button/PrimaryButton'
import { addShippingAddress } from '../../store/modules/home/actions'
import { addNewShippingAddress } from '../../global/api'

const AddShippingAddress = ({navigation}) => {

    const dispatch = useDispatch()

    const [form, setForm] = useState({})
    const [errors, setErrors] = useState({})
    const [buttonLoader, setButtonLoader] = useState(false);


    const onChange = ({name, value}) => {
        setForm({...form, [name]: value})
    }


    useEffect(() => {

    }, []);


    const saveAddress = () => {
        console.log("form-> > ", form);
        var isValid = true
        setButtonLoader(true)
                
        if(!form.name){
            console.log("Please enter a valid name!");
            isValid = false
            
            setErrors((prev) => {
                return {...prev, name: 'Please enter a valid name!'}
            })
        }

        if(!form.email){
            console.log("Please enter a valid email address!");
            isValid = false
            
            setErrors((prev) => {
                return {...prev, email: 'Please enter a valid email address!'}
            })
        }


        if(!form.phone){
            console.log("Please enter a valid mobile number!");
            isValid = false
            
            setErrors((prev) => {
                return {...prev, phone: 'Please enter a valid mobile number!'}
            })
        }

        if(!form.street){
            console.log("Please enter a valid street!");
            isValid = false
            
            setErrors((prev) => {
                return {...prev, street: 'Please enter a valid street!'}
            })
        }
        
        if(!form.apartment){
            console.log("Please enter a valid apartment!");
            isValid = false
            
            setErrors((prev) => {
                return {...prev, apartment: 'Please enter a valid apartment!'}
            })
        }

        if(!form.city){
            console.log("Please enter a valid city!");
            isValid = false
            
            setErrors((prev) => {
                return {...prev, city: 'Please enter a valid city!'}
            })
        }

        if(!form.zip){
            console.log("Please enter a valid zipcode!");
            isValid = false
            
            setErrors((prev) => {
                return {...prev, zip: 'Please enter a valid zipcode!'}
            })
        }


        if(isValid){
            // dispatch(addShippingAddress({
            //     name: form?.name,
            //     mobileNumber: form?.mobileNumber,
            //     address: form?.address,
            //     city: form?.city,
            //     addressState: form?.state,
            //     pincode: form?.pincode,
            // }))

            var body = {
                "name": form?.name,
                "email": form?.email,
                "phone": form?.phone,
                "street": form?.street,
                "apartment": form?.apartment,
                "zip": form?.zip,
                "city": form?.city,
            }
    
            console.log("body=> ", body);
    
            addNewShippingAddress(body)
             .then(resp => {
                console.log("resp => ", resp);
                setButtonLoader(false)
                navigation.goBack()
            })
            .catch(err => {
                console.log("err => ", err);
            })
        }else{
            setButtonLoader(false)
        }

    }



    return (
        <SafeAreaView style={styles.container} >
            <Header title="Add Shipping Address" />

            <View style={styles.mainContainer} >

                <ScrollView style={{width: screenWidth, paddingHorizontal: 20, }} >
                        <View style={{}} >
                            <Input
                                label="Full Name"
                                placeholder="Enter your full name"
                                keyboardType="default"
                                error={errors.name}
                                onChangeText={(text) => {onChange({name: 'name', value: text,}); setErrors({}); }}
                            />

                            <Input
                                label="Email Address"
                                placeholder="Enter your email address"
                                keyboardType="default"
                                error={errors.email}
                                onChangeText={(text) => {onChange({name: 'email', value: text,}); setErrors({}); }}
                            />

                            <Input
                                label="Mobile Number"
                                placeholder="Enter your mobile number"
                                keyboardType="number-pad"
                                error={errors.phone}
                                onChangeText={(text) => {onChange({name: 'phone', value: text,}); setErrors({}); }}
                            />

                            <Input
                                label="Street"
                                placeholder="Enter your street"
                                keyboardType="default"
                                error={errors.street}
                                onChangeText={(text) => {onChange({name: 'street', value: text,}); setErrors({}); }}
                            />
                            
                            <Input
                                label="Apartment"
                                placeholder="Enter your apartment"
                                keyboardType="default"
                                error={errors.apartment}
                                onChangeText={(text) => {onChange({name: 'apartment', value: text,}); setErrors({}); }}
                            />

                            <Input
                                label="City"
                                placeholder="Enter your city"
                                keyboardType="default"
                                error={errors.city}
                                onChangeText={(text) => {onChange({name: 'city', value: text,}); setErrors({}); }}
                            />

                            <Input
                                label="Zipcode"
                                placeholder="Enter your zipcode"
                                keyboardType="number-pad"
                                error={errors.zip}
                                onChangeText={(text) => {onChange({name: 'zip', value: text,}); setErrors({}); }}
                            />
                        </View>

                    </ScrollView>

                <PrimaryButton
                    title="Save Address"
                    buttonLoader={buttonLoader}
                    onPress={() => {saveAddress()}}
                />
                
            </View>
        </SafeAreaView>
    )
}



const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colors.dark_blue,
    },
    mainContainer: {
        flex: 1,
        padding: 20,
        paddingBottom: 10,
        alignItems: 'center',
    },
    sheetTitle: {
        fontSize: 18,
        fontFamily: Poppins.Medium,
        color: colors.white,
        marginBottom: 12,
    },
    heading: {
        fontSize: 18,
        fontFamily: Poppins.Medium,
        color: '#FFFFFF',
    },
})


export default AddShippingAddress