import React, {useState, useEffect} from 'react'
import { Text, View, SafeAreaView, StyleSheet, TouchableOpacity, Image, ScrollView, Dimensions, useColorScheme, BackHandler, ImageBackground, } from 'react-native'

import { media } from '../../global/media'
import { colors } from '../../global/colors'
import { Poppins } from '../../global/fontFamily'
import { screenHeight, screenWidth } from '../../global/constants'

import Header from '../../components/header'
import { useDispatch, useSelector } from 'react-redux'
import PrimaryButton from '../../components/button/PrimaryButton'
import { useNavigation } from '@react-navigation/native'
import LottieView from 'lottie-react-native'
import { LocalNotification } from '../../utils/LocalPushController'
import { clearCartList } from '../../store/modules/home/actions'

const OrderConfirmation = (props) => {
    const dispatch = useDispatch();
    const navigation = useNavigation();

    useEffect(() => {
        LocalNotification({
            title: "Order Placed Successfully! 🎉!",
            message: `Your product is on its way to you. Track your order now!`,
        })
    
    }, [])
    
    const continueButton = () => {
        dispatch(clearCartList([]))
        navigation.navigate('Cart')
    }
    
    return (
        <SafeAreaView style={styles.container} >
            <ImageBackground source={media.bg_image} style={{flex: 1}} >
                    <Header title="Order Confirmation" />

                    <View style={styles.mainContainer} >
                        <Text></Text>
                        <View style={{alignItems: 'center'}} >
                            <LottieView
                                source={media.success_lottie} 
                                autoPlay 
                                loop 
                                style={{height: screenWidth-150, width: screenWidth-150, }}
                            />
                            <Text style={styles.sccessTitle} >SUCCESS!</Text>
                            <Text style={styles.sccessSubTitle} >Order has been placed!</Text>
                        </View>

                        <PrimaryButton
                            title="Continue"
                            onPress={() => {continueButton()}}
                        />
                        
                    </View>
            </ImageBackground>
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colors.bg_color,
    },
    mainContainer: {
        flex: 1,
        padding: 20,
        paddingTop: 0,
        alignItems: 'center',
        justifyContent: 'space-between',
    },
    sccessTitle: {
        fontSize: 24,
        fontFamily: Poppins.Bold,
        color: '#FFFFFF',
    },
    sccessSubTitle: {
        fontSize: 20,
        fontFamily: Poppins.Medium,
        color: '#FFFFFF',
    },
    
})


export default OrderConfirmation