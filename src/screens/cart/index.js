import React, {useState, useEffect} from 'react'
import { Text, View, SafeAreaView, StyleSheet, TouchableOpacity, Image, ScrollView, Dimensions, useColorScheme, BackHandler, ImageBackground, TextInput, } from 'react-native'

import { media } from '../../global/media'
import { colors } from '../../global/colors'
import { Poppins } from '../../global/fontFamily'
import { bikes_array } from '../../global/sampleData';
import { screenHeight, screenWidth } from '../../global/constants'
import CartCard from '../../components/custom/cart/CartCard'

import SwipeButton from 'rn-swipe-button';
import Icon from '../../utils/icons'
import { useDispatch, useSelector } from 'react-redux'
import { currentOrderDetails, decrementCartItemQuantity, deleteItemFromCart, incrementCartItemQuantity } from '../../store/modules/home/actions'
import LottieView from 'lottie-react-native'
import EmptyLottie from '../../components/lottie/EmptyLottie'

const CartScreen = ({navigation}) => {

    const dispatch = useDispatch()

    const [offerValue, setOfferValue] = useState('');

    const cart_list = useSelector(state => state.home.cart_list);
    console.log('====================================');
    console.log("cart_list=>> ",cart_list);
    console.log('====================================');

    const [totalPrice, setTotalPrice] = useState(0);
    const [subTotalPrice, setSubTotalPrice] = useState(0);
    const [delivieryPrice, setDelivieryPrice] = useState(120);
    const [discountPrice, setDiscountPrice] = useState(0);
    const [isDeliveryFree, setIsDeliveryFree] = useState(false);

    useEffect(() => {
        var price = cart_list.reduce((n, {bikePrice, quantity}) => n + bikePrice * quantity, 0);
        
        var deliveryFee = price >= 1000 ? 0 : 120

        var total_price = price + deliveryFee
        
        setTotalPrice((total_price).toFixed(2));
        setSubTotalPrice((price).toFixed(2));
        setIsDeliveryFree(deliveryFee == 0 ? true : false);
    }, [cart_list || discountPrice]);


    const CartDetails = () => {
        return(
            <View style={{width: '100%', marginTop: 25,}} >
                {subTotalPrice < 1000
                ?
                <Text style={styles.freeShippingText} >Add item with total price value of $ 1000 for free shipping</Text>
                :
                <Text style={styles.freeShippingText} >Your bag qualifies for free shipping</Text>
                }
                <View style={{width: '100%'}} >
                    <View style={styles.rowSpace} >
                        <Text style={styles.heading}  >Subtotal:</Text>
                        <Text style={styles.subheading}  >$ {subTotalPrice}</Text>
                    </View>
                    <View style={styles.rowSpace} >
                        <Text style={styles.heading}  >Delivery Fee:</Text>
                        <Text style={[styles.subheading, {textDecorationLine: subTotalPrice >= 1000 ? 'line-through' : 'none'}]}  >$ {delivieryPrice}</Text>
                    </View>
                    <View style={styles.rowSpace} >
                        <Text style={styles.heading}  >Discount:</Text>
                        <Text style={styles.subheading}  >$ {discountPrice}</Text>
                    </View>
                    <View style={[styles.rowSpace, {marginTop: 15,}]} >
                        <Text style={styles.heading}  >Total:</Text>
                        <Text style={styles.subheading}  >$ {totalPrice-discountPrice}</Text>
                    </View>
                </View>
            </View>
        )
    }



    const CheckoutButton = () => {
        return(
            <View style={styles.buttonContainer} >
                <Icon type={'Feather'} name={'chevron-right'}  size={26} color={colors.white} />
            </View>
        );
    } 


    const checkIsIncludedInCart = (bike) => {
        var cartList = cart_list.some(i => i.name.includes(bike.name));

        return cartList
    }


    
    const incrementCartItemFunction = (bike) => {
        console.log("incrementCartItemFunction-> ", bike);
        dispatch(incrementCartItemQuantity({
            id: bike.id,
            quantity: bike.quantity,
        }))
    }

    const decrementCartItemFunction = (bike) => {
        console.log("decrementCartItemFunction-> ", bike);
        dispatch(decrementCartItemQuantity({
            id: bike.id,
            quantity: bike.quantity,
        }))
    }

    const checkForOffer = () => {
        if(offerValue == 'Bike'){
            setDiscountPrice(300);
            setOfferValue('')
        }
    }

    const proceedToShipping = () => {

        dispatch(currentOrderDetails({
            totalPrice: totalPrice,
            subTotalPrice: subTotalPrice,
            delivieryPrice: delivieryPrice,
            discountPrice: discountPrice,
            isDeliveryFree: isDeliveryFree,
        }))

        navigation.navigate('ShippingAddress');
    }

    let forceResetLastButton = null;
    
    return (
        <SafeAreaView style={styles.container} >
            <ImageBackground source={media.bg_image} style={{flex: 1}} >
                
            
            {cart_list?.length != 0
                ? <View style={{flex: 1, justifyContent: 'space-between'}} >
                    <ScrollView keyboardShouldPersistTaps="always" >
                        <View style={styles.mainContainer} >
                            <View style={[{width: '100%',}, styles.cardStyle]} >
                                <Text style={[styles.cardTitle, {paddingLeft: 20, paddingTop: 20, marginBottom: -10}]}  >Order Items {cart_list?.length == 1 ? '(1 item)' : `(${cart_list?.length} items)`}</Text>
                                <CartCard 
                                    dataArr={cart_list} 
                                    incrementCartItemFunction={(item) => incrementCartItemFunction(item)}
                                    decrementCartItemFunction={(item) => decrementCartItemFunction(item)}
                                />
                            </View>
                            

                            <View style={styles.appleOffersContainer} >
                                <TextInput
                                    placeholder='Apply Offer'
                                    placeholderTextColor={colors.gray}
                                    value={offerValue}
                                    style={styles.inputStyle}
                                    onChangeText={(text) => {setOfferValue(text)}}
                                />
                                <TouchableOpacity 
                                    onPress={() => {checkForOffer()}}
                                    style={styles.applyOffer} >
                                    <Text style={styles.applyOfferText} >Apply</Text>
                                </TouchableOpacity>
                            </View>

                            <CartDetails />
                        </View>
                    </ScrollView>

                    <View style={{height: 60, justifyContent: 'center', backgroundColor: '#1b222e'}} >
                    <SwipeButton
                        containerStyles={{borderRadius: 5, }}
                        height={40}
                        onSwipeSuccess={() =>{
                            forceResetLastButton && forceResetLastButton();
                            proceedToShipping()
                        }}
                        forceReset={ reset => {
                            forceResetLastButton = reset
                        }}
                        railFillBackgroundColor={colors.primary}
                        railBackgroundColor={colors.dark_blue}
                        railStyles={{borderRadius: 5}}
                        thumbIconComponent={CheckoutButton}
                        thumbIconStyles={{borderRadius: 5, }}
                        thumbIconWidth={40} 
                        titleColor={colors.white}
                        titleFontSize={16}
                        title="Checkout"
                    />
                    </View> 
                </View>
                :
                <EmptyLottie />
                }
                
            </ImageBackground>
        </SafeAreaView>
    )
}



const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colors.dark_blue,
    },
    mainContainer: {
        flex: 1,
        padding: 20,
        // alignItems: 'center',
        marginBottom: 20,
    },
    cardStyle: {
        width: '100%',
        borderRadius: 14,
        borderWidth: 1,
        borderColor: colors.dark_gray,
        backgroundColor: colors.dark_blue,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.34,
        shadowRadius: 1.27,
        elevation: 8,
    },
    freeShippingText: {
        fontSize: 14,
        fontFamily: Poppins.Regular,
        color: '#FFFFFF60',
        marginBottom: 10,
        textAlign: 'center',
    },
    rowSpace: {
        flexDirection: 'row',
        alignItems: 'center',
        marginBottom: 4,
        justifyContent: 'space-between',
    },
    heading: {
        fontSize: 18,
        fontFamily: Poppins.Medium,
        color: '#FFFFFF',
    },
    subheading: {
        fontSize: 18,
        fontFamily: Poppins.Medium,
        color: '#FFFFFF90',
    },

    appleOffersContainer: {
        width: '100%',
        height: 50,
        borderRadius: 10,
        marginTop: 20,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        backgroundColor: '#1b222e'
    },
    applyOffer: {
        height: 50,
        width: 90,
        borderRadius: 10,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: colors.primary,
    },
    applyOfferText: {
        fontSize: 14,
        fontFamily: Poppins.Medium,
        color: colors.white,
    },
    inputStyle: {
        fontSize: 14,
        fontFamily: Poppins.Medium,
        color: colors.white,
        paddingLeft: 20,
        flex: 1,
        marginRight: 10,
    },
    buttonContainer: {
        height: 44,
        width: 44,
        borderRadius: 10,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: colors.primary
    },
    cardTitle: {
        fontSize: 18,
        fontFamily: Poppins.Medium,
        color: '#FFFFFF',
    },
})


export default CartScreen