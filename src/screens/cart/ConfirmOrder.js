import React, {useState, useEffect} from 'react'
import { Text, View, SafeAreaView, StyleSheet, TouchableOpacity, Image, ScrollView, Dimensions, useColorScheme, BackHandler, ImageBackground, } from 'react-native'

import { media } from '../../global/media'
import { colors } from '../../global/colors'
import { Poppins } from '../../global/fontFamily'
import { screenHeight, screenWidth } from '../../global/constants'

import Header from '../../components/header'
import { useDispatch, useSelector } from 'react-redux'
import PrimaryButton from '../../components/button/PrimaryButton'
import { useNavigation } from '@react-navigation/native'
import CartCard from '../../components/custom/cart/CartCard'
import { decrementCartItemQuantity, incrementCartItemQuantity } from '../../store/modules/home/actions'
import { addNewOrderDetails } from '../../global/api'

const ConfirmOrder = (props) => {

    const dispatch = useDispatch()
    const navigation = useNavigation();

    const cart_list = useSelector(state => state.home.cart_list);
    const current_shipping_address = useSelector(state => state.home.current_shipping_address);
    const current_order_details = useSelector(state => state.home.current_order_details);
    const current_user_details = useSelector(state => state.auth.current_user_details);
    
    console.log('====================================');
    console.log("current_user_details ", current_user_details);
    console.log('====================================');

    useEffect(() => {
        
    }, []);

      
    const incrementCartItemFunction = (bike) => {
        console.log("incrementCartItemFunction-> ", bike);
        dispatch(incrementCartItemQuantity({
            id: bike.id,
            quantity: bike.quantity,
        }))
    }

    const decrementCartItemFunction = (bike) => {
        console.log("decrementCartItemFunction-> ", bike);
        dispatch(decrementCartItemQuantity({
            id: bike.id,
            quantity: bike.quantity,
        }))
    }


    const placeOrderFunction = () => {
     

        let orderItemsList = cart_list?.map((item) => {
            return { 
                quantity: item.quantity, 
                product: item.id 
            };
        });

        var body = {
            orderItems: orderItemsList,
            user: current_user_details?.id,
            address: current_shipping_address?.id,
        }

        addNewOrderDetails(body)
        .then((resp) => {
            console.log('====================================');
            console.log("resp => ", resp);
            console.log('====================================');
            navigation.navigate('OrderConfirmation')
        })
        .catch((err) => {
            console.log('====================================');
            console.log("err => ", err);
            console.log('====================================');
        })

        //navigation.navigate('OrderConfirmation')
    }


    const OrderItems = () => {
        return(
            <View style={[{width: '100%', marginTop: 15, paddingTop: 20, paddingBottom: 0}, styles.checkoutContainer]} >
                <Text style={[styles.cardTitle, {paddingLeft: 20}]}  >Order Items {cart_list?.length == 1 ? '(1 item)' : `(${cart_list?.length} items)`}</Text>
                <View style={{}} >
                    <CartCard
                        dataArr={cart_list}
                        isConfirmOrder={true}
                        incrementCartItemFunction={(item) => incrementCartItemFunction(item)}
                        decrementCartItemFunction={(item) => decrementCartItemFunction(item)}
                    />
                </View>
            </View>
        )
    } 

    const OrderInfo = () => {
        return(
            <View style={[{width: '100%', marginTop: 15, padding: 20}, styles.checkoutContainer]} >
                <View style={{width: '100%'}} >
                    <Text style={styles.cardTitle}  >Order Information</Text>
                    <View style={styles.rowSpace} >
                        <Text style={styles.heading}>Subtotal:</Text>
                        <Text style={styles.subheading}>$ {current_order_details?.subTotalPrice}</Text>
                    </View>
                    <View style={styles.rowSpace}>
                        <Text style={styles.heading}>Delivery Fee:</Text>
                        <Text style={[styles.subheading,  {textDecorationLine: current_order_details?.subTotalPrice >= 1000 ? 'line-through' : 'none'}]} >$ {current_order_details?.delivieryPrice}</Text>
                    </View>
                    <View style={styles.rowSpace} >
                        <Text style={styles.heading}>Discount:</Text>
                        <Text style={styles.subheading}  >$ {current_order_details?.discountPrice}</Text>
                    </View>
                    <View style={[styles.rowSpace, {paddingTop: 10, marginTop: 10, borderTopWidth: 1, borderColor: colors.dark_gray}]} >
                        <Text style={styles.heading}>Total:</Text>
                        <Text style={styles.subheading}>$ {current_order_details?.totalPrice}</Text>
                    </View>
                </View>
            </View>
        )
    } 

    const ShippingDetails = () => {
        return(
            <View style={[{width: '100%', marginTop: 15, padding: 20}, styles.checkoutContainer]} >
                <View style={{width: '100%'}} >
                    <Text style={styles.cardTitle}  >Shipping Details</Text>
                    <View style={styles.rowSpace} >
                        <Text style={styles.heading}  >Name:</Text>
                        <Text style={styles.subheading}  >{current_shipping_address?.name}</Text>
                    </View>
                    <View style={styles.rowSpace} >
                        <Text style={styles.heading}  >Email:</Text>
                        <Text style={styles.subheading}  >{current_shipping_address?.email}</Text>
                    </View>
                    <View style={styles.rowSpace} >
                        <Text style={styles.heading}  >Mobile:</Text>
                        <Text style={[styles.subheading, ]}  >{current_shipping_address?.phone}</Text>
                    </View>
                    <View style={[styles.rowSpace, {flex: 1,}]} >
                        <Text style={styles.heading}  >Address:</Text>
                       <View style={{flex: 1, alignItems: 'flex-end', marginLeft: 35}} >
                         <Text style={styles.subheading}  >{current_shipping_address?.street}, {current_shipping_address?.apartment}, {current_shipping_address?.city}, {current_shipping_address?.zip}</Text>
                       </View>
                    </View>
                </View>
            </View>
        )
    } 

    const PaymentMode = () => {
        return(
            <View style={[{width: '100%', marginTop: 15, padding: 20}, styles.checkoutContainer]} >
                <Text style={styles.heading}  >Payment Mode</Text>
                <Text style={styles.subheading}  >Cash on Delivery</Text>
            </View>
        )
    } 
    
    return (
        <SafeAreaView style={styles.container} >
            <ImageBackground source={media.bg_image} style={{flex: 1}} >
                <Header title="Confirm Order" />

                <View style={styles.mainContainer} >

                    <ScrollView style={{width: screenWidth, paddingHorizontal: 20, }} >
                        <View style={{width: '100%', marginBottom: 20}} >
                            <OrderItems />

                            <OrderInfo />

                            <ShippingDetails />

                            <PaymentMode />
                        </View>


                    </ScrollView>

                    <PrimaryButton
                        title="Place Order"
                        onPress={() => {placeOrderFunction()}}
                    />
                </View>

            </ImageBackground>
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colors.bg_color,
    },
    mainContainer: {
        flex: 1,
        padding: 20,
        paddingTop: 0,
        paddingBottom: 10,
        alignItems: 'center',
        justifyContent: 'space-between',
    },
    freeShippingText: {
        fontSize: 14,
        fontFamily: Poppins.Regular,
        color: '#FFFFFF60',
        marginBottom: 10,
        textAlign: 'center',
    },
    rowSpace: {
        flexDirection: 'row',
        alignItems: 'center',
        marginBottom: 4,
        justifyContent: 'space-between',
    },
    cardTitle: {
        fontSize: 18,
        fontFamily: Poppins.Medium,
        color: '#FFFFFF',
    },
    heading: {
        fontSize: 16,
        fontFamily: Poppins.Medium,
        color: '#FFFFFF',
    },
    subheading: {
        fontSize: 16,
        fontFamily: Poppins.Medium,
        color: '#FFFFFF90',
    },
    checkoutContainer: {
        width: '100%',
        paddingBottom: 15,
        borderRadius: 14,
        borderWidth: 1,
        borderColor: colors.dark_gray,
        backgroundColor: colors.dark_blue,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.34,
        shadowRadius: 1.27,
        elevation: 8,
    },
})


export default ConfirmOrder