import React, { useRef } from 'react'
import { Text, View, SafeAreaView, StyleSheet, TouchableOpacity, Image, ScrollView, Dimensions, useColorScheme, Vibration, } from 'react-native'

import { media } from '../../global/media'
import { colors } from '../../global/colors'
import { Poppins } from '../../global/fontFamily'
import { screenWidth } from '../../global/constants'

import PrimaryButton from '../../components/button/PrimaryButton'
import { LocalNotification, ScheduleLocalNotification } from '../../utils/LocalPushController'
import RemotePushController from '../../utils/RemotePushController'
import GetStartedSheet from '../../components/bottomsheet/GetStartedSheet'
import LoginSheet from '../../components/bottomsheet/LoginSheet'

const OnboardingScreen = ({navigation}) => {

    const refRBSheet = useRef();
    const loginRefRBSheet = useRef();

    const enableVibration = () => {
        Vibration.vibrate(50);
    }

    const loginFunction = (val, isLogin) => {
        refRBSheet.current.close();

        switch(val){
            case 'email': 
            console.log('====================================');
            console.log("through >> email ");
            console.log('====================================');
            
            navigation.navigate(isLogin ? 'Login' : 'SignUp', {params: {loginTypeEmail: true}})
            
            break;

            case 'phone': 
            console.log('====================================');
            console.log("through >> phone ");
            console.log('====================================');
            
            navigation.navigate(isLogin ? 'Login' : 'SignUp', {params: {loginTypeEmail: false}})

            break;

            case 'google': 
            console.log('====================================');
            console.log("through >> google ");
            console.log('====================================');
            break;

            case 'apple': 
            console.log('====================================');
            console.log("through >> apple ");
            console.log('====================================');
            default: 
            console.log('====================================');
            console.log("through >> default ");
            console.log('====================================');
        }
        
    }

    return (
        <SafeAreaView style={styles.container} >
            <View style={styles.imgContainer} >
                <Image source={media.getstarted} style={{height: 220, width: 220, resizeMode: 'contain', marginTop: 20}} />
                <Text style={styles.title} >Welcome to BikeHub, Ready to explore the world of cycling with BikeHub?</Text>
                <Text style={styles.subtitle} >Let's get started! Sign up now and embark on your biking adventure today.</Text>
            </View>
            <PrimaryButton
                title="Get Started" 
                onPress={() => {
                    refRBSheet.current.open();
                    enableVibration();
                    //navigation.navigate('Login');
                }}
            />

            <GetStartedSheet
                refRBSheet={refRBSheet}
                enableVibration={() => {enableVibration()}}
                loginFunction={(val, isLogin) => loginFunction(val, isLogin)}
                closeSheet={() => {refRBSheet.current.close();}}
            />

            <LoginSheet
                refRBSheet={loginRefRBSheet}
                enableVibration={() => {enableVibration()}}
                closeSheet={() => {loginRefRBSheet.current.close();}}
            />


            <RemotePushController />
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: 20,
        paddingBottom: 0,
        backgroundColor: colors.bg_color,
    },
    imgContainer: {
        flex: 1, 
        alignItems: 'center', 
        justifyContent: 'center',
    },
    title: {
        fontSize: 18,
        paddingHorizontal: 15,
        fontFamily: Poppins.Medium,
        color: colors.black,
        textAlign: 'center',
        marginTop: 20,
    },
    subtitle: {
        fontSize: 14,
        paddingHorizontal: 15,
        marginTop: 10,
        fontFamily: Poppins.Medium,
        color: colors.dark_gray,
        textAlign: 'center'
    }
})

export default OnboardingScreen