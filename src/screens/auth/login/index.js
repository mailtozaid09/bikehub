import React, {useState, useEffect} from 'react'
import { Text, View, SafeAreaView, StyleSheet, TouchableOpacity, Image, ScrollView, Dimensions, useColorScheme, } from 'react-native'

import { colors } from '../../../global/colors'
import { Poppins } from '../../../global/fontFamily'

import Input from '../../../components/input'
import PrimaryButton from '../../../components/button/PrimaryButton'
import { media } from '../../../global/media'
import LottieView from 'lottie-react-native'
import { screenWidth } from '../../../global/constants'
import Icon from '../../../utils/icons'
import { checkUserExist, userLogin } from '../../../global/api'
import { useDispatch, useSelector } from 'react-redux'
import { currentUserDetails } from '../../../store/modules/auth/actions'

const LoginScreen = (props) => {

    const navigation = props.navigation
    const dispatch = useDispatch()

    const [mobileNumber, setMobileNumber] = useState('');

    const [form, setForm] = useState({});
    const [errors, setErrors] = useState({});
    const [apiError, setApiError] = useState(null);
    const [loginTypeEmail, setLoginTypeEmail] = useState(false);
    const [buttonLoader, setButtonLoader] = useState(false);

    const current_user_details = useSelector(state => state.auth.current_user_details);

    useEffect(() => {
        var val = props?.route?.params?.params?.loginTypeEmail
        setLoginTypeEmail(val)
        console.log('====================================');
        console.log("props > ",val);
        console.log('====================================');
    
    }, [])
    

    const onChange = ({name, value}) => {
        setForm({...form, [name]: value})
    }


    const loginFunction = () => {
        setButtonLoader(true)
        var isValid = true
        if(loginTypeEmail){
            console.log("clesd");
            if(!form?.email){
                console.log("Please enter a valid email");
                isValid = false
                setErrors((prev) => {
                    return {...prev, email: 'Please enter a valid email!'}
                })
            }
        }else{
            if(mobileNumber.length != 10){
                console.log("Please enter a valid number");
                isValid = false
                setErrors((prev) => {
                    return {...prev, number: 'Please enter a valid number!'}
                })
            }
        }

        if(isValid){
            setButtonLoader(true)

            navigation.navigate('LoginVerification', {
                params: {
                    loginTypeEmail: loginTypeEmail, 
                    value: loginTypeEmail ? form.email : mobileNumber
                } 
            });
        }else{
            setButtonLoader(false)
        }
    }

    
    return (
        <SafeAreaView style={styles.container} >
            <ScrollView keyboardShouldPersistTaps="always" contentContainerStyle={{flex: 1}} >
                <View style={styles.mainContainer} >
                    <View style={{}} >
                        <View style={styles.iconContainer}>
                            <Icon type="MaterialCommunityIcons" name={loginTypeEmail ? 'email' : 'cellphone-text'} size={34} color={colors.dark_gray} />
                        </View>

                        <View style={{marginTop: 20, marginBottom: 10}} >
                            <Text style={styles.title} >Login with {loginTypeEmail ? 'Email' : 'Phone'}</Text>  
                            <Text style={styles.subTitle} >Sign in with your {loginTypeEmail ? 'email' : 'phone number'}.</Text>            
                        </View>

                        <View>

                        {loginTypeEmail
                        ?
                        <View>
                            <Input
                                label="Email"
                                placeholder="Enter your email"
                                error={errors.email}
                                onChangeText={(text) => {onChange({name: 'email', value: text,}); setErrors({}); setApiError(null); }}
                            />
                            
                        </View>
                        :
                        <Input
                            label="Phone Number"
                            placeholder="Enter your phone number"
                            isNumber
                            error={errors.number}
                            value={mobileNumber}
                            onChangeText={(text) => {setMobileNumber(text); setErrors({}); }}
                        />
                        }
                        </View>
                    </View>
                
                   

                    <View>
                        <View style={{alignItems: 'center', width: '100%'}} >
                            {apiError && <Text style={{fontSize: 14, fontFamily: Poppins.Medium, color: colors.reddish }} >{apiError}</Text>}
                        </View>
                        
                        <PrimaryButton
                            title="Continue" 
                            buttonLoader={buttonLoader}
                            onPress={() => {
                                loginFunction()
                            }}
                        />
                    </View>

                </View>
            </ScrollView>
           
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colors.bg_color,
    },
    mainContainer: {
        padding: 20,
        flex: 1,
        //alignItems: 'center',
        justifyContent: 'space-between',
    },
    alreadyContainer: {
        paddingHorizontal: 20,
        alignItems: 'center',
        justifyContent: 'center',
    },
    alreadyText: {
        fontSize: 14,
        fontFamily: Poppins.Medium,
        color: colors.gray,
        marginTop: 8,
        textAlign: 'center',
    },
    highlighted: {
        textDecorationLine: 'underline',
        color: colors.primary,
    },
    donthaveText: {
        fontSize: 14,
        fontFamily: Poppins.Medium,
        color: colors.gray,
        marginTop: 10,
    },
    buttonText: {
        fontSize: 16,
        fontFamily: Poppins.Medium,
        color: colors.white,
    },
    buttonContainer: {
        height: 56,
        borderRadius: 10,
        paddingHorizontal: 20,
        borderWidth: 1,
        borderColor: colors.gray,
        alignItems: 'center',
        justifyContent: 'space-between',
        flexDirection: 'row'
    },
    orDivider: {
       flex: 1,
       height: 0.5,
       backgroundColor: colors.gray, 
    },
    orContainer: {
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection: 'row',
        marginBottom: 20,
        marginTop: 5,
    },
    orText: {
        fontSize: 14,
        fontFamily: Poppins.Medium,
        color: colors.gray,
        marginHorizontal: 15,
    },
    iconContainer: {
        height: 60,
        width: 60,
        borderRadius: 30,
        backgroundColor: colors.gray,
        alignItems: 'center',
        justifyContent: 'center'
    },
    title: {
        fontSize: 18,
        fontFamily: Poppins.SemiBold,
        color: colors.black,
    },
    subTitle: {
        fontSize: 14,
        fontFamily: Poppins.SemiBold,
        color: colors.dark_gray,
        marginBottom: 10,
    },
})

export default LoginScreen