import React, {useState, useEffect, useRef} from 'react'
import { Text, View, SafeAreaView, StyleSheet, TouchableOpacity, Image, ScrollView, Dimensions, useColorScheme, KeyboardAvoidingView, ActivityIndicator, } from 'react-native'

import { colors } from '../../../global/colors'
import { Poppins } from '../../../global/fontFamily'
import { media } from '../../../global/media'

import Icon from '../../../utils/icons'

import Input from '../../../components/input'
import PrimaryButton from '../../../components/button/PrimaryButton'

import { launchImageLibrary } from 'react-native-image-picker'
import { checkUserExist, createNewUser } from '../../../global/api'
import { currentUserDetails, userLoggedIn } from '../../../store/modules/auth/actions'
import { useDispatch } from 'react-redux'


const RegistrationScreen = (props) => {

    const { navigation } = props;
    const dispatch = useDispatch()

    const [filePath, setFilePath] = useState('');
    const [fileType, setFileType] = useState('');
    const [mainImage, setMainImage] = useState('');

    const [isBuyer, setIsBuyer] = useState(true);
    const [form, setForm] = useState({})
    const [errors, setErrors] = useState({})
    const [buttonLoader, setButtonLoader] = useState(false);


    const [currentIdx, setCurrentIdx] = useState(0);
    const [loginDetails, setLoginDetails] = useState({});
   
    useEffect(() => {
        const unsubscribe = navigation.addListener('focus', () => {
            setForm({})
            setCurrentIdx(0)

            addEmailPassToForm()
        });
  
        return unsubscribe;
    }, [navigation]);


    const addEmailPassToForm = () => {
        var details = props?.route.params?.params
        setLoginDetails(details)
        console.log("register - - userDetails >>  ", props?.route.params);
        console.log("register - - userDetails >>  ", props?.route.params?.params);
        setForm({...form, 
            ['email']: details?.loginTypeEmail ? details?.value : '', 
            ['phone']: !details?.loginTypeEmail ? details?.value : '', 
        })
    }
    

    const onChange = ({name, value}) => {
        setForm({...form, [name]: value})
    }

    const checkNumberExist = async () => {
        var emailBody = {
            "email": form.email,
        }

        var phoneBody = {
            "phone": form.phone,
        }

        var type = !loginDetails?.loginTypeEmail ? 'email' : 'phone'
        var body = !loginDetails?.loginTypeEmail ? emailBody : phoneBody

        var numberExist = false
        await checkUserExist(body, type)
        .then(resp => {
            console.log("checkUserExist resp => ", resp);
            if(resp?.success){
                numberExist = true
                
            }
        })
        .catch(err => {
            console.log("checkUserExist err => ", err);
        })
        console.log("numberExist > >  ",numberExist);
        return numberExist;
    }

    const loginFunction = async (skip) => {
        console.log('====================================');
        console.log("form = > > > > ", form);
        console.log('====================================');
        var isValid = true
        setButtonLoader(true)
            
        if(currentIdx == 0){
            if(!form.name){
                console.log("Please enter a valid name");
                isValid = false
                setErrors((prev) => {
                    return {...prev, name: 'Please enter a valid name'}
                })
            }
        }

        if(currentIdx == 1){

            if(!form.phone || form.phone.length != 10){
                console.log("Please enter a valid phone");
                isValid = false
                setErrors((prev) => {
                    return {...prev, phone: 'Please enter a valid phone'}
                })
            }

            var exist = await checkNumberExist()
            console.log("exist= > > > ",exist);

            if(form.phone && exist){
                console.log("Mobile number already registered!");
                isValid = false
                setErrors((prev) => {
                    return {...prev, phone: 'Mobile number already registered!'}
                })
            }
        }
        
        if(currentIdx == 2){
           
        }
        
        if(currentIdx == 3){
            if(!skip){
                if(!mainImage){
                    console.log("Please choose a profile picture");
                    isValid = false
                    setErrors((prev) => {
                        return {...prev, picture: 'Please choose a profile picture'}
                    })
                }
            }
        }


        if(isValid){
           
            setCurrentIdx(currentIdx + 1)
            
            setButtonLoader(false)
            setErrors({})
            if(currentIdx >= 3){
                console.log('====================================');
                console.log("DETAILS + >> > >  ", form);
                console.log('====================================');

                createUser(skip)
            }
            //setButtonLoader(false)
        }else{
            setButtonLoader(false)
        }

    }

    const createUser = (skip) => {
        let formData = new FormData();

        if(skip){
            formData.append("profile_url", '');
        }else{
            formData.append("profile_url", {
                uri: filePath,
                type: fileType,
                name: fileType
            });
        }
       
        formData.append("name", form?.name);
        formData.append("email", form?.email);
        formData.append("phone", form?.phone);
        formData.append("password", form?.password);
        formData.append("isAdmin", isBuyer ? 'false' : 'true');

        createNewUser(formData)
         .then(resp => {
            console.log("resp => ", resp);

            // dispatch(currentUserDetails({
            //     id: resp?.id,
            //     name: resp?.name,
            //     email: resp?.email,
            //     profile_url: resp?.profile_url,
            //     phone: resp?.phone,
            //     isAdmin: resp?.isAdmin,
            // }))

            // dispatch(userLoggedIn(true))

            // navigation.navigate('Tabbar')
            navigation.navigate('SignUpVerification', {
                params: {
                    loginTypeEmail: loginDetails?.loginTypeEmail, 
                    value: loginDetails?.value
                } 
            });

        })
        .catch(err => {
            console.log("err => ", err);
            setCurrentIdx(currentIdx-1)
        })
    }

    const chooseFile = () => {
        let options = {
            includeBase64: true,
            mediaType: 'photo',
            maxWidth: 300,
            maxHeight: 550,
            quality: 1,
        };
        launchImageLibrary(options, (response) => {

        if (response.didCancel) {
            alert('User cancelled camera picker');
            return;
        } else if (response.errorCode == 'camera_unavailable') {
            alert('Camera not available on device');
            return;
        } else if (response.errorCode == 'permission') {
            alert('Permission not satisfied');
            return;
        } else if (response.errorCode == 'others') {
            alert(response.errorMessage);
            return;
        }
            setFilePath(response?.assets[0]?.uri);
            setMainImage(response?.assets[0]?.uri)
            setFileType(response?.assets[0]?.type)
        });
    };

    
    return (
        <SafeAreaView style={styles.container} >
                <View style={styles.mainContainer} >
                   
                    
                    {currentIdx == 0 ?
                        <View style={{width: '100%'}} >
                            <View>
                                <Text style={styles.heading}>What's your name?</Text>
                                <Text style={styles.subheading}>Let's us know how to properly adress you</Text>
                            </View>
                            <Input
                                label="Full Name"
                                placeholder="Enter your full name"
                                error={errors.name}
                                onChangeText={(text) => {onChange({name: 'name', value: text,}); setErrors({}); }}
                            />
                        </View>
                    : currentIdx == 1 ?
                        <View style={{width: '100%'}} >
                            <View>
                                <Text style={styles.heading}>What's your {!loginDetails?.loginTypeEmail ? 'email' : 'number'}?</Text>
                                <Text style={styles.subheading}>We'll need your {!loginDetails?.loginTypeEmail ? 'email' : 'phone number'} for security and communication purposes</Text>
                            </View>
                            {loginDetails?.loginTypeEmail 
                            ?
                            <Input
                                label="Mobile Number"
                                placeholder="Enter your mobile number"
                                error={errors.phone}
                                isNumber
                                onChangeText={(text) => {onChange({name: 'phone', value: text,}); setErrors({}); }}
                            />
                            :
                            <Input
                                label="Email"
                                placeholder="Enter your email address"
                                error={errors.email}
                                onChangeText={(text) => {onChange({name: 'email', value: text,}); setErrors({}); }}
                            /> 
                            }
                            
                        </View>
                    : currentIdx == 2 ? 
                        <View style={{width: '100%'}} >
                            <View>
                                <Text style={styles.heading}>Who are you?</Text>
                                <Text style={styles.subheading}>Please tell a bit more about yourself</Text>
                            </View>
                            <View >
                                <TouchableOpacity 
                                    activeOpacity={0.5}
                                    onPress={() => {setIsBuyer(true)}}
                                    style={{flexDirection: 'row', alignItems: 'center', justifyContent: 'center', borderWidth: 2, borderColor: isBuyer ? colors.primary : colors.gray, marginBottom: 15, borderRadius: 10, paddingHorizontal: 25, paddingVertical: 15,}} >
                                    <Image source={media.buyer} style={{height: 70, width: 70,}} />
                                    <View style={{alignItems: 'center', flex: 1, marginLeft: 10}} >
                                        <Text style={styles.heading} >Buyer</Text>
                                        <Text style={[styles.subheading, {textAlign: 'center', marginBottom: 0}]} >You are planning to use this app as buyer</Text>
                                    </View>
                                </TouchableOpacity>
                                <TouchableOpacity 
                                    activeOpacity={0.5}
                                    onPress={() => {setIsBuyer(false)}}
                                    style={{flexDirection: 'row', alignItems: 'center', justifyContent: 'center', borderWidth: 2, borderColor: !isBuyer ? colors.primary : colors.gray, marginBottom: 15, borderRadius: 10, paddingHorizontal: 25, paddingVertical: 15,}} >
                                    <Image source={media.seller} style={{height: 70, width: 70,}} />
                                    <View style={{alignItems: 'center', flex: 1, marginLeft: 10}} >
                                        <Text style={styles.heading} >Seller</Text>
                                        <Text style={[styles.subheading, {textAlign: 'center', marginBottom: 0}]} >You are planning to use this app as seller</Text>
                                    </View>
                                </TouchableOpacity>
                            </View>
                        </View>
                    : currentIdx == 3 ?
                        <View style={{width: '100%', flex: 1, alignItems: 'center', justifyContent:'space-between'}} >
                            <View>
                                <Text style={styles.heading}>Add a profile picture!</Text>
                                <Text style={styles.subheading}>Personalize your account by adding a profile picture, making your presence stand out</Text>
                            </View>

                            <View style={styles.imageContainer} >
                                <Image 
                                    source={
                                        !mainImage 
                                        ? media.person_active
                                        : {uri: mainImage}
                                    }
                                    style={mainImage ? styles.imageStyle : {height: 60, width: 60, resizeMode: 'contain'}} 
                                />

                                <TouchableOpacity  
                                    onPress={() => {
                                        chooseFile();
                                        setErrors({});
                                    }}
                                    style={styles.cameraContainer} > 
                                    <Icon type={"MaterialCommunityIcons"} name={"camera-wireless-outline"}  size={20} color={colors.primary} />
                                </TouchableOpacity>
                            </View>   

                            <Text></Text>
                        </View>
                    :
                        <View style={{width: '100%', flex: 1, alignItems: 'center', justifyContent: 'center'}} >
                            
                            <Text style={styles.heading}>Welcome to BikeHub</Text>
                            <Text style={styles.subheading}>Customising your experience...</Text>

                            <View style={{height: 60, width: 60, alignItems: 'center', justifyContent: 'center', borderRadius: 10, backgroundColor: colors.primary}} >
                                <ActivityIndicator size="large" color={colors.dark_blue} />
                            </View>
                        
                        </View>
                    }


                    <View style={{alignItems: 'center', width: '100%'}} >
                        {errors.picture && <Text style={{fontSize: 14, fontFamily: Poppins.Medium, color: colors.reddish }} >{errors?.picture}</Text>}
                        {currentIdx == 3 && <TouchableOpacity
                            activeOpacity={0.5}
                            onPress={() => {loginFunction('skip')}}
                            style={styles.skipContainer}
                        >
                            <Text style={styles.skipText}>Skip</Text>
                        </TouchableOpacity>}
                        {currentIdx <= 3 &&<PrimaryButton
                            title="Continue" 
                            buttonLoader={buttonLoader}
                            onPress={() => {
                                loginFunction()
                                //createUser()
                            }}
                        />}
                    </View>
                </View>
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colors.bg_color,
    },
    mainContainer: {
        padding: 20,
        flex: 1,
        width: '100%',
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    heading: {
        fontSize: 24,
        fontFamily: Poppins.Regular,
        color: colors.black,
    },
    subheading: {
        fontSize: 18,
        fontFamily: Poppins.Light,
        color: colors.black,
        marginBottom: 20
    },
    textInputStyle: {
        height: 44,
        width: 44,
        borderRadius: 6,
        alignItems: 'center',
        justifyContent: 'center',
        borderWidth: 1,
        fontSize: 14,
        fontFamily: Poppins.Medium,
        color: colors.black,
    },
    skipContainer: {
        height: 56,
        width: '100%',
        marginTop: 10,
        borderWidth: 0.5,
        borderColor: colors.black,
        borderRadius: 12,
        alignItems: 'center',
        justifyContent: 'center',
    },
    skipText: {
        fontSize: 18,
        fontFamily: Poppins.Light,
        color: colors.black,
    },
    cameraContainer: {
        position: 'absolute', 
        bottom: -10, 
        right: 10,
        height: 40, 
        width: 40, 
        alignItems: 'center', 
        justifyContent: 'center', 
        borderRadius: 20, 
        borderWidth: 1, 
        borderColor: colors.gray,
        backgroundColor: colors.dark_blue
    },
    imageStyle: {
        height: 140,
        width: 140,
        borderRadius: 70,
    }, 
    imageContainer: {
        height: 160, 
        width: 160, 
        borderRadius: 80, 
        borderWidth: 3, 
        marginVertical: 14, 
        alignItems: 'center', 
        justifyContent: 'center', 
        borderColor: colors.primary
    },
})

export default RegistrationScreen