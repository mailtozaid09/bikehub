import PushNotification, {Importance} from "react-native-push-notification";


PushNotification.configure({
 onRegister: function (token) {
    console.log("TOKEN:", token);
  },

  onNotification: function (notification) {
    console.log("LOCAL NOTIFICATION:", notification);
  },
 onAction: function (notification) {
    console.log("ACTION:", notification.action);
    console.log("NOTIFICATION:", notification);
  },

  onRegistrationError: function(err) {
    console.error(err.message, err);
  },



  popInitialNotification: true,
  requestPermissions: true,
});

PushNotification.createChannel(
    {
        channelId: "channel-id", 
        channelName: "My channel", 
        channelDescription: "A channel to categorise your notifications",
        playSound: true, 
        soundName: "default", 
        importance: Importance.HIGH,
        vibrate: true,
    },
    (created) => console.log(`createChannel returned '${created}'`) 
);

export const LocalNotification = (props) => {
    const { title, message, description, picture} = props;

    PushNotification.localNotification({
        channelId: "channel-id", 
        channelName: "My channel", 
        autoCancel: true,
        playSound: true, 
        soundName: "default", 
        importance: Importance.HIGH,
        vibrate: true,
        bigText: description,
        title: title,
        message: message,
        picture: picture,
        color: 'red'
    })
}


export const ScheduleLocalNotification = (props) => {

  const { title, message, description, date, picture} = props;

  PushNotification.localNotificationSchedule({
      channelId: "channel-id", 
      channelName: "My channel", 
      autoCancel: true,
      playSound: true, 
      soundName: "default", 
      importance: Importance.HIGH,
      vibrate: true,
      date: date,
      bigText: description,
      title: title,
      message: message,
      picture: picture,
  })
}