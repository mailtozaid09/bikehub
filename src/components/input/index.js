import React, { useState } from 'react';
import { TouchableOpacity, StyleSheet, Dimensions, Text, View, Touchable, TextInput, Image, useColorScheme, } from 'react-native';

import { colors } from '../../global/colors';
import { screenWidth } from '../../global/constants';
import { Poppins } from '../../global/fontFamily';


const Input = ({ placeholder, bgColor, keyboardType, label, isNumber, error, value, onClearSearchText, onChangeText, onChangeEyeIcon, isPassword, showEyeIcon }) => {

    return (
        <View style={{marginBottom: 12,}} >
            {label && <Text style={styles.label} >{label}</Text>}

            <View style={[isNumber ? styles.numberStyle : [styles.inputContainer, {backgroundColor: bgColor ? bgColor : null}]]}  >

                

                {isNumber && 
                <View style={{borderRightWidth: 1, borderColor: colors.dark_gray, marginRight: 10}} >
                    <Text style={styles.countryCode} >+91</Text>
                </View>}

                <TextInput
                    placeholder={placeholder}
                    style={styles.inputStyle}
                    value={value}
                    maxLength={isNumber ? 10 : null}
                    keyboardType={isNumber ? 'number-pad' : 'default' || keyboardType ? keyboardType : 'default'}
                    onChangeText={onChangeText}
                    placeholderTextColor={colors.dark_gray}
                    secureTextEntry={isPassword  && !showEyeIcon ? true : false}
                />

                
            </View>
            {error && <Text style={styles.error} >{error}</Text>}
        </View>
    )
}


const styles = StyleSheet.create({
    numberStyle: {
        height: 55,
        paddingHorizontal: 15,
        paddingVertical: 4,
        borderWidth: 0.5,
        borderColor: colors.dark_gray,
        borderRadius: 12,
        width: screenWidth-40,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    inputContainer: {
        height: 55,
        paddingHorizontal: 15,
        borderWidth: 0.5,
        borderColor: colors.dark_gray,
        borderRadius: 12,
        width: screenWidth-40,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    inputStyle: {
        fontSize: 16,
        alignItems: 'center',
        justifyContent: 'center',
        fontFamily: Poppins.Medium,
        color: colors.black,
        flex: 1,
    },
    iconImage: {
        height: 24,
        width: 24
    },
    countryCode: {
        fontSize: 16,
        fontFamily: Poppins.Medium,
        color: colors.black,
        marginTop: 2,
        marginRight: 10,
    },
    error: {
        fontSize: 14,
        fontFamily: Poppins.Medium,
        color: colors.reddish,
        marginTop: 4,
    },
    label: {
        fontSize: 14,
        fontFamily: Poppins.SemiBold,
        color: colors.dark_gray,
        marginBottom: 4,
    },

})

export default Input
