import React from 'react'
import { View } from 'react-native'
import { media } from '../../global/media'
import { screenWidth } from '../../global/constants'

import LottieView from 'lottie-react-native'


const EmptyLottie = () => {
    return (
        <View style={{flex: 1, alignItems: 'center', justifyContent: 'center' }} >
            <LottieView
                source={media.empty_lottie} 
                autoPlay 
                loop 
                style={{height: screenWidth-70, width: screenWidth-70, }}
            />
        </View>
    )
}

export default EmptyLottie