import React, { useState, useEffect, useRef } from 'react';
import { SafeAreaView, Text, View, TouchableOpacity, Image, StyleSheet, Dimensions, Platform, ScrollView, KeyboardAvoidingView, useColorScheme, Vibration, } from 'react-native';

import { media } from '../../global/media';
import { colors } from '../../global/colors';
import { fontSize, Poppins } from '../../global/fontFamily';

import Icon from '../../utils/icons';

import RBSheet from "react-native-raw-bottom-sheet";
import Input from '../input';
import { screenHeight, screenWidth } from '../../global/constants';
import PrimaryButton from '../button/PrimaryButton';

import { useDispatch } from 'react-redux';

const LoginSheet = ({refRBSheet, enableVibration, closeSheet, loginFunction}) => {


    return (
        <View>
            <RBSheet
                ref={refRBSheet}
                closeOnDragDown={true}
                closeOnPressMask={true}
                dragFromTopOnly={true}
                height={screenHeight-30}
                onClose={() => {}}
                animationType="slide"
                customStyles={{
                    wrapper: {
                        backgroundColor: "#00000090",
                    },
                    draggableIcon: {
                        backgroundColor: colors.dark_blue,
                        width: 80,
                        height: 0,
                    }, 
                    container: {
                        backgroundColor: colors.white,
                        borderRadius: 20,
                        width: screenWidth-30,
                        
                        margin: 15,
                        
                    }
                }}
            >
                <View style={styles.sheetContainer} >
                   
                   <Text>dsad</Text>
                </View>
            </RBSheet>
        </View>
    )
}

const styles = StyleSheet.create({
    sheetContainer: {
        flex: 1, 
        padding: 15,
        paddingTop: 0,
        backgroundColor: colors.white
    },
    sheetTitle: {
        fontSize: 18,
        fontFamily: Poppins.SemiBold,
        color: colors.black,
    },
    sheetSubTitle: {
        fontSize: 14,
        fontFamily: Poppins.SemiBold,
        color: colors.dark_gray,
        marginBottom: 10,
    },
    closeContainer: {
        height: 28,
        width: 28,
        borderRadius: 14,
        backgroundColor: colors.gray,
        alignItems: 'center',
        justifyContent: 'center'
    },
    buttonContainer: {
        flex: 1,
        height: 45,
        borderRadius: 10,
        marginBottom: 12,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: colors.primary,
    },
    buttonTitle: {
        fontSize: 14,
        fontFamily: Poppins.SemiBold,
        color: colors.white,
    },
})

export default LoginSheet
