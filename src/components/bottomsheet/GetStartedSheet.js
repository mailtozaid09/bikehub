import React, { useState, useEffect, useRef } from 'react';
import { SafeAreaView, Text, View, TouchableOpacity, Image, StyleSheet, Dimensions, Platform, ScrollView, KeyboardAvoidingView, useColorScheme, Vibration, } from 'react-native';

import { media } from '../../global/media';
import { colors } from '../../global/colors';
import { fontSize, Poppins } from '../../global/fontFamily';

import Icon from '../../utils/icons';

import RBSheet from "react-native-raw-bottom-sheet";
import Input from '../input';
import { screenWidth } from '../../global/constants';
import PrimaryButton from '../button/PrimaryButton';

import { useDispatch } from 'react-redux';

const GetStartedSheet = ({refRBSheet, enableVibration, closeSheet, loginFunction, }) => {

    const [isLogin, setIsLogin] = useState(false);

    return (
        <View>
            <RBSheet
                ref={refRBSheet}
                closeOnDragDown={true}
                closeOnPressMask={true}
                dragFromTopOnly={true}
                animationType="slide"
                //height={500}
                onClose={() => {}}
                customStyles={{
                    wrapper: {
                        backgroundColor: "#00000090",
                    },
                    draggableIcon: {
                        backgroundColor: colors.dark_blue,
                        width: 80,
                        height: 0,
                    }, 
                    container: {
                        backgroundColor: colors.white,
                        borderRadius: 20,
                        width: screenWidth-30,
                        margin: 15,
                        flex: 1,
                    }
                }}
            >
                <View style={styles.sheetContainer} >
                    <View style={{flexDirection: 'row', justifyContent: 'space-between', marginBottom: 12,}} >
                        <Image source={media.getstarted} style={{height: 50, width: 50, resizeMode: 'contain'}} />

                        <View style={styles.closeContainer}>
                            <TouchableOpacity
                                activeOpacity={0.5}
                                onPress={() => {
                                    closeSheet();
                                    enableVibration();
                                }}
                                >
                                <Icon type="AntDesign" name="close" size={16} color={colors.dark_gray} />
                            </TouchableOpacity>
                        </View>
                    </View>

                    <Text style={styles.sheetTitle} >{isLogin ? 'Welcome Back!' : 'Get Started!'}</Text>  
                    <Text style={styles.sheetSubTitle} >Ride the Future: Your Ultimate Bike Shopping Experience Starts Here!</Text>            

                    <View style={{flex: 1}} >
                        <TouchableOpacity
                            activeOpacity={0.5}
                            onPress={() => {
                                
                                enableVibration();
                                loginFunction('email', isLogin);
                            }}
                            style={styles.buttonContainer} 
                        >
                            <Text style={styles.buttonTitle} >{!isLogin ? 'Continue' : 'Login'} with Email</Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                            activeOpacity={0.5}
                            onPress={() => {
                                
                                enableVibration();
                                loginFunction('phone', isLogin);
                            }}
                            style={[styles.buttonContainer,{backgroundColor: colors.gray}]} 
                        >
                            <Text style={[styles.buttonTitle, {color: colors.black}]} >{!isLogin ? 'Continue' : 'Login'} with Phone</Text>
                        </TouchableOpacity>


                        <View style={{flex: 1, flexDirection: 'row', justifyContent: 'space-between',}} >
                            <TouchableOpacity
                                activeOpacity={0.5}
                                onPress={() => {
                                    
                                    enableVibration();
                                    loginFunction('apple', isLogin);
                                }}
                                style={[styles.buttonContainer, {marginRight: 5, backgroundColor: colors.gray, }]} 
                            >
                                <Icon type="AntDesign" name="apple1" size={22} color={colors.black} />
                            </TouchableOpacity>
                            <TouchableOpacity
                                activeOpacity={0.5}
                                onPress={() => {
                                    
                                    enableVibration();
                                    loginFunction('google', isLogin);
                                }}
                                style={[styles.buttonContainer, {marginLeft: 5, backgroundColor: colors.gray, }]} 
                            >
                                <Icon type="AntDesign" name="google" size={22} color={colors.black} />
                            </TouchableOpacity>
                        </View>


                        {isLogin
                        ?
                        <View style={styles.alreadyContainer} >
                            <Text style={styles.alreadyText} >Don't have an account? </Text>
                            <TouchableOpacity activeOpacity={0.5} onPress={() => {setIsLogin(!isLogin)}} >
                                <Text style={styles.highlighted} >Sign Up</Text>
                            </TouchableOpacity>
                        </View>
                        :
                        <View style={styles.alreadyContainer} >
                            <Text style={styles.alreadyText} >Already have an account? </Text>
                            <TouchableOpacity activeOpacity={0.5} onPress={() => {setIsLogin(!isLogin)}} >
                                <Text style={styles.highlighted} >Log In</Text>
                            </TouchableOpacity>
                        </View>
                        }

                    </View>
                </View>
            </RBSheet>
        </View>
    )
}

const styles = StyleSheet.create({
    sheetContainer: {
        flex: 1, 
        padding: 15,
        paddingTop: 0,
        backgroundColor: colors.white
    },
    sheetTitle: {
        fontSize: 18,
        fontFamily: Poppins.SemiBold,
        color: colors.black,
    },
    sheetSubTitle: {
        fontSize: 14,
        fontFamily: Poppins.SemiBold,
        color: colors.dark_gray,
        marginBottom: 10,
    },
    closeContainer: {
        height: 28,
        width: 28,
        borderRadius: 14,
        backgroundColor: colors.gray,
        alignItems: 'center',
        justifyContent: 'center'
    },
    buttonContainer: {
        flex: 1,
        height: 45,
        borderRadius: 10,
        marginBottom: 12,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: colors.primary,
    },
    buttonTitle: {
        fontSize: 14,
        fontFamily: Poppins.SemiBold,
        color: colors.white,
    },
    alreadyContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: 10,
    },
    alreadyText: {
        fontSize: 14,
        fontFamily: Poppins.Medium,
        color: colors.dark_gray,
        marginTop: 8,
        textAlign: 'center',
    },
    highlighted: {
        textDecorationLine: 'underline',
        color: colors.primary,
    },

})

export default GetStartedSheet
