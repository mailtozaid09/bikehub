import React, { useState, useEffect, useRef } from 'react';
import { SafeAreaView, Text, View, TouchableOpacity, Image, StyleSheet, Dimensions, Platform, ScrollView, KeyboardAvoidingView, useColorScheme, } from 'react-native';

import { media } from '../../global/media';
import { colors } from '../../global/colors';
import { fontSize, Poppins } from '../../global/fontFamily';

import Icon from '../../utils/icons';

import RBSheet from "react-native-raw-bottom-sheet";
import Input from '../input';
import { screenWidth } from '../../global/constants';
import PrimaryButton from '../button/PrimaryButton';

import { useDispatch } from 'react-redux';

const AddAddressSheet = ({refRBSheet, saveAddress, buttonLoader}) => {

    const dispatch = useDispatch()
    const [form, setForm] = useState({})
    const [errors, setErrors] = useState({})


    const onChange = ({name, value}) => {
        setForm({...form, [name]: value})
    }

    return (
        <View>
            <RBSheet
                ref={refRBSheet}
                closeOnDragDown={true}
                closeOnPressMask={true}
                dragFromTopOnly={true}
                height={500}
                onClose={() => {}}
                customStyles={{
                    wrapper: {
                        backgroundColor: "#00000090",
                    },
                    draggableIcon: {
                        backgroundColor: colors.gray,
                        width: 100,
                    }, 
                    container: {
                        backgroundColor: colors.dark_blue,
                        borderTopRightRadius: 20,
                        borderTopLeftRadius: 20,
                        paddingTop: 10,
                    }
                }}
            >
                <View style={styles.addressSheetContainer} >
                    <Text style={styles.sheetTitle} >Add Shipping Address</Text>            
                
                    <ScrollView style={{width: screenWidth, paddingHorizontal: 20, }} >
                        <View style={{paddingBottom: 80}} >
                            <Input
                                label="Full Name"
                                placeholder="Enter your full name"
                                keyboardType="default"
                                error={errors.name}
                                onChangeText={(text) => {onChange({name: 'name', value: text,}); setErrors({}); }}
                            />

                            <Input
                                label="Mobile Number"
                                placeholder="Enter your mobile number"
                                keyboardType="number-pad"
                                error={errors.mobileNumber}
                                onChangeText={(text) => {onChange({name: 'mobileNumber', value: text,}); setErrors({}); }}
                            />

                            <Input
                                label="Address"
                                placeholder="Enter your address"
                                keyboardType="default"
                                error={errors.address}
                                onChangeText={(text) => {onChange({name: 'address', value: text,}); setErrors({}); }}
                            />

                            <Input
                                label="City"
                                placeholder="Enter your city"
                                keyboardType="default"
                                error={errors.city}
                                onChangeText={(text) => {onChange({name: 'city', value: text,}); setErrors({}); }}
                            />

                            <Input
                                label="State"
                                placeholder="Enter your state"
                                keyboardType="default"
                                error={errors.state}
                                onChangeText={(text) => {onChange({name: 'state', value: text,}); setErrors({}); }}
                            />

                            <Input
                                label="Pincode"
                                placeholder="Enter your pincode"
                                keyboardType="number-pad"
                                error={errors.pincode}
                                onChangeText={(text) => {onChange({name: 'pincode', value: text,}); setErrors({}); }}
                            />

                            <PrimaryButton
                                title="Save Address"
                                buttonLoader={buttonLoader}
                                onPress={() => {saveAddress(form)}}
                            />

                        </View>

                    </ScrollView>
                
                </View>
            </RBSheet>
        </View>
    )
}

const styles = StyleSheet.create({
    addressSheetContainer: {
        flex: 1, 
        padding: 20,
        paddingTop: 10,
        alignItems: 'center',
        backgroundColor: colors.dark_blue
    },
    sheetTitle: {
        fontSize: 18,
        fontFamily: Poppins.Medium,
        color: colors.white,
        marginBottom: 12,
    }
})

export default AddAddressSheet
