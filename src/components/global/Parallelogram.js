import React, {useState} from 'react'
import { Text, View, SafeAreaView, StyleSheet, TouchableOpacity, Image, ScrollView, Dimensions, useColorScheme, FlatList, } from 'react-native'
import { screenWidth } from '../../global/constants';
import { colors } from '../../global/colors';


const Parallelogram = ({children}) => {

    const TriangleCorner = () => {
        return <View style={[styles.triangleCorner, ]} />;
    };
      
    const TriangleCornerTopRight = () => {
        return (
            <View style={{transform: [{ rotate: '0deg' }]}} >
                <TriangleCorner style={styles.triangleCornerTopRight} />   
            </View>
        )
    }

    return (
            <TriangleCornerTopRight />
    )
}

const styles = StyleSheet.create({
    parallelogram: {
        // width: 150,
        // height: 100,
        flexDirection: 'row'
    },
    rectangle: {
        height: 100,
        width: 100,
        backgroundColor:'blue'
    },
    triangleCorner: {
        width: 0,
        height: 0,
        borderRightWidth: screenWidth-40,
        borderTopWidth: 50,
        borderRightColor: "transparent",
        borderTopColor: colors.card_bg,
    },
    triangleCornerTopRight: {
        // transform: [{ rotate: "90deg" }],
    },
    triangleCornerBottomRight: {
        // transform: [{ rotate: "180deg" }],
        transform: [{ rotate: '180deg' }]
    },
})

export default Parallelogram