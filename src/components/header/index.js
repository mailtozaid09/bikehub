import React from 'react'
import { Text, View, SafeAreaView, StyleSheet, TouchableOpacity, Image, ScrollView, Dimensions, useColorScheme, } from 'react-native'

import Icon from '../../utils/icons';
import { media } from '../../global/media';
import { useNavigation } from '@react-navigation/native';

import { Poppins } from '../../global/fontFamily';
import { colors } from '../../global/colors';
import { screenWidth } from '../../global/constants';

const Header = ({title, onPress}) => {

    const navigation = useNavigation()
    
    return (
        <View style={styles.container} >
            <View style={{flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between'}} >
                <TouchableOpacity 
                    activeOpacity={0.5} 
                    onPress={() => {navigation.goBack()}}
                    style={styles.buttonContainer} >
                    <Icon type={'Feather'} name={'chevron-left'}  size={26} color={colors.white} />
                </TouchableOpacity>
                <Text style={styles.title} >{title}</Text>
                <Text></Text>
            </View>
        </View>
    )
}

const styles =  StyleSheet.create({
    container: {
        height: 70,
        paddingHorizontal: 20,
        width: screenWidth,
        justifyContent: 'center',
        
        backgroundColor: 'transparent',
    },
    buttonContainer: {
        height: 44,
        width: 44,
        borderRadius: 10,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: colors.primary
    },
    title: {
        fontSize: 20,
        fontFamily: Poppins.Medium,
        color: colors.white,
        marginRight: 10
    },
    subtitle: {
        fontSize: 18,
        lineHeight: 20,
        fontFamily: Poppins.Medium,
        color: colors.black,
    },
})

export default Header