import React from 'react'
import { Text, View, SafeAreaView, StyleSheet, TouchableOpacity, Image, ScrollView, Dimensions, useColorScheme, } from 'react-native'

import Icon from '../../utils/icons';
import { media } from '../../global/media';
import { useNavigation } from '@react-navigation/native';

import { Poppins } from '../../global/fontFamily';
import { colors } from '../../global/colors';
import { screenWidth } from '../../global/constants';

const HomeHeader = ({userDetails}) => {

    const navigation = useNavigation()
    
    return (
        <View style={styles.container} >
            <View style={{flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between'}} >
                <TouchableOpacity 
                    activeOpacity={0.5} 
                    onPress={() => {navigation.navigate('Notification')}}
                    style={{flexDirection: 'row', alignItems: 'center'}} >
                    <Icon type={'Feather'} name={'menu'}  size={26} color={colors.white} />
                </TouchableOpacity>
                <View style={{flexDirection: 'row', alignItems: 'center',}} >
                    <TouchableOpacity 
                        activeOpacity={0.5} 
                        onPress={() => {navigation.navigate('Notification')}}
                        style={{flexDirection: 'row', alignItems: 'center', marginRight: 15,}} >
                        <Icon type={'MaterialCommunityIcons'} name={'bell-badge-outline'}  size={26} color={colors.white}  />
                    </TouchableOpacity>
                    <TouchableOpacity 
                        activeOpacity={0.5} 
                        onPress={() => {navigation.navigate('Notification')}}
                        style={{flexDirection: 'row', alignItems: 'center'}} >
                        <Icon type={'AntDesign'} name={'questioncircleo'}  size={26} color={colors.white} />
                    </TouchableOpacity>
                </View>
            </View>
        </View>
    )
}

const styles =  StyleSheet.create({
    container: {
        paddingHorizontal: 20,
        width: screenWidth,
        justifyContent: 'center',
        backgroundColor: colors.dark_blue,
    },
    title: {
        fontSize: 14,
        fontFamily: Poppins.SemiBold,
        color: colors.black,
    },
    subtitle: {
        fontSize: 18,
        lineHeight: 20,
        fontFamily: Poppins.Medium,
        color: colors.black,
    },
})

export default HomeHeader