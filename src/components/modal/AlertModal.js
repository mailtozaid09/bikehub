import React, {useState} from 'react'
import { Text, View, SafeAreaView, StyleSheet, TouchableOpacity, Image, ScrollView, Dimensions, useColorScheme, Alert, Pressable, Modal, } from 'react-native'


import { Poppins } from '../../global/fontFamily';

import LottieView from 'lottie-react-native';
import { media } from '../../global/media';

import { colors } from '../../global/colors';

const AlertModal = ({closeModal, title, description, alertType, onCancel, onAdd, onDelete, onEdit, onLogout}) => {
  
    const [modalVisible, setModalVisible] = useState(true);

    return (
        <SafeAreaView style={styles.container} >
            <View style={styles.centeredView}>
                <Modal
                    animationType="slide"
                    transparent={true}
                    visible={modalVisible}
                    onRequestClose={() => {
                        Alert.alert('Are you sure! \nYou wan to close the modal?');
                        closeModal()
                    }}>
                    <View style={styles.centeredView}>
                        <View style={styles.modalView}>
                            
                            <View>
                                <LottieView 
                                    source={
                                        alertType == 'Add' || alertType == 'Success' ? media.success_lottie
                                        : alertType == 'Edit' ? media.edit_lottie
                                        : alertType == 'Error' ? media.error_lottie
                                        : alertType == 'Delete' ? media.error_lottie
                                        : alertType == 'Logout' ? media.logout_lottie
                                        : media.success_lottie
                                        } 
                                    autoPlay 
                                    loop 
                                    style={{height: 100, width: 100, marginBottom: 12}}
                                />
                            </View>

                            <Text style={[styles.modalTitle, {color: alertType == 'Delete' || alertType == 'Logout' || alertType == 'Error' ? colors.reddish : colors.primary}]}>{title}</Text>
                            
                            <Text style={styles.modalDescription}>{description}</Text>

                                {alertType == 'Edit' && (
                                    <View style={{flexDirection: 'row', width: '100%', alignItems: 'center', justifyContent: 'space-between'}} >
                                        <TouchableOpacity
                                            activeOpacity={0.5}
                                            style={[styles.button, styles.buttonClose, {backgroundColor: colors.white}]}
                                            onPress={onCancel}>
                                            <Text style={[styles.textStyle, {color: colors.primary}]}>Cancel</Text>
                                        </TouchableOpacity> 
                                        <TouchableOpacity
                                            activeOpacity={0.5}
                                            style={[styles.button, styles.buttonEdit]}
                                            onPress={onEdit}>
                                            <Text style={styles.textStyle}>Edit</Text>
                                        </TouchableOpacity> 
                                    </View>
                                )}

                                {alertType == 'Delete' && (
                                    <View style={{flexDirection: 'row', width: '100%', alignItems: 'center', justifyContent: 'space-between'}} >
                                        <TouchableOpacity
                                            activeOpacity={0.5}
                                            style={[styles.button, styles.buttonClose, {backgroundColor: colors.primary}]}
                                            onPress={onCancel}>
                                            <Text style={[styles.textStyle, {color: colors.white}]}>Cancel</Text>
                                        </TouchableOpacity> 
                                        <TouchableOpacity
                                            activeOpacity={0.5}
                                            style={[styles.button, styles.buttonDelete, {borderWidth: 1, backgroundColor: colors.white, borderColor: colors.reddish}]}
                                            onPress={onDelete}>
                                            <Text style={[styles.textStyle, {color: colors.reddish}]}>Delete</Text>
                                        </TouchableOpacity> 
                                    </View>
                                )}

                                {alertType == 'Logout' && (
                                    <View style={{flexDirection: 'row', width: '100%', alignItems: 'center', justifyContent: 'space-between'}} >
                                        <TouchableOpacity
                                            activeOpacity={0.5}
                                            style={[styles.button, styles.buttonClose, {backgroundColor: colors.primary}]}
                                            onPress={closeModal}>
                                            <Text style={[styles.textStyle, {color: colors.white}]}>Cancel</Text>
                                        </TouchableOpacity> 
                                        <TouchableOpacity
                                            activeOpacity={0.5}
                                            style={[styles.button, styles.buttonDelete, {borderWidth: 1, backgroundColor: colors.white, borderColor: colors.reddish}]}
                                            onPress={onLogout}>
                                            <Text style={[styles.textStyle, {color: colors.reddish}]}>Logout</Text>
                                        </TouchableOpacity> 
                                    </View>
                                )}

                                {alertType == 'Add' || alertType == 'Success' && (
                                    <View style={{}} >
                                         <TouchableOpacity
                                            activeOpacity={0.5}
                                            style={[styles.button, styles.buttonAdd]}
                                            onPress={onAdd}>
                                            <Text style={[styles.textStyle, {color: colors.white}]}>Okay</Text>
                                        </TouchableOpacity>
                                    </View>
                                )}

                                {alertType == 'Error' && (
                                    <View style={{}} >
                                         <TouchableOpacity
                                            activeOpacity={0.5}
                                            style={[styles.button, styles.buttonAdd]}
                                            onPress={closeModal}>
                                            <Text style={[styles.textStyle, {color: colors.white}]}>Okay</Text>
                                        </TouchableOpacity>
                                    </View>
                                )}
                        </View>
                    </View>
                </Modal>
            </View>
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        height: '100%',
        width: '100%',
        position: 'absolute',
        backgroundColor: '#00000050',
    },
    centeredView: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 22,
      },
    modalView: {
        margin: 20,
        width: '80%',
        backgroundColor: colors.white,
        borderRadius: 20,
        padding: 25,
        alignItems: 'center',
        shadowColor: '#000',
        shadowOffset: {
          width: 0,
          height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 4,
        elevation: 5,
      },
    button: {
        borderRadius: 30,
        height: 50,
        width: 100,
        elevation: 2,
        alignItems: 'center',
        justifyContent: 'center',
        paddingHorizontal: 20,
    },
    buttonAdd: {
        backgroundColor: colors.primary,
    },
    buttonEdit: {
        backgroundColor: colors.primary,
    },
    buttonDelete: {

    },
    buttonClose: {
        borderWidth: 1,
        backgroundColor: colors.gray,
        borderColor: colors.primary,
    },
    textStyle: {
        fontSize: 16,
        fontFamily: Poppins.SemiBold,
        color: colors.primary_light,
    },
    modalTitle: {
        textAlign: 'center',
        fontSize: 20,
        fontFamily: Poppins.Medium,
        color: colors.black,
    },
    modalDescription: {
        marginBottom: 15,
        textAlign: 'center',
        fontSize: 16,
        fontFamily: Poppins.Medium,
        color: colors.black,
    },
})

export default AlertModal