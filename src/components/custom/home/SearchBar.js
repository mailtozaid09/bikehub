import React, { useState } from 'react';
import { TouchableOpacity, StyleSheet, Dimensions, Text, View, Touchable, TextInput, Image, useColorScheme, } from 'react-native';

import { colors } from '../../../global/colors';
import { screenWidth } from '../../../global/constants';
import { Poppins } from '../../../global/fontFamily';
import Icon from '../../../utils/icons';


const SearchBar = ({ placeholder, showSearchInput, value, onShowSearchBar, onClearSearchText, onChangeText,}) => {

    return (
        <View style={{paddingHorizontal: 20, width: screenWidth, flexDirection: 'row', alignItems: 'center'}} >

            {!showSearchInput ?
            <View style={styles.chooseContainer} >
                <Text style={styles.chooseText} >Choose your Bike</Text>
            </View> 
            :
                <View style={[styles.inputContainer]}  >
                    <TextInput
                        placeholder={placeholder}
                        style={styles.inputStyle}
                        value={value}
                        onChangeText={onChangeText}
                        placeholderTextColor={colors.gray}
                    />
                </View>
            }

            <View style={styles.iconContainer} >
                {!value 
                ? 
                    <TouchableOpacity
                        activeOpacity={0.5}
                        onPress={onShowSearchBar}
                    >
                        <Icon type={'AntDesign'} name={'search1'}  size={24} color={colors.white} />
                    </TouchableOpacity>
                :   
                    <TouchableOpacity
                        activeOpacity={0.5}
                        onPress={onClearSearchText}
                    >
                        <Icon type={'AntDesign'} name={'close'}  size={24} color={colors.white} />
                    </TouchableOpacity>
                }
            </View>
        </View>
    )
}


const styles = StyleSheet.create({
    inputContainer: {
        height: 50,
        paddingHorizontal: 15,
        borderWidth: 0.5,
        borderColor: colors.gray,
        borderRadius: 10,
        flex: 1,
        marginRight: 14,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    inputStyle: {
        fontSize: 16,
        alignItems: 'center',
        justifyContent: 'center',
        fontFamily: Poppins.Medium,
        color: colors.white,
        flex: 1,
    },
    iconContainer: {
        height: 50,
        width: 50,
        borderRadius: 10,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: colors.primary
    },
    chooseContainer: {
        height: 50,
        flex: 1,
        marginRight: 14,
        flexDirection: 'row',
        alignItems: 'center',
    },
    chooseText: {
        fontSize: 20,
        fontFamily: Poppins.SemiBold,
        color: colors.white
    }

})

export default SearchBar
