import React, {useState} from 'react'
import { Text, View, SafeAreaView, StyleSheet, TouchableOpacity, Image, ScrollView, Dimensions, useColorScheme, FlatList, } from 'react-native'

import Icon from '../../../utils/icons';

import { media } from '../../../global/media';
import { colors } from '../../../global/colors';
import { Poppins } from '../../../global/fontFamily';
import { screenWidth } from '../../../global/constants';

import { useNavigation } from '@react-navigation/native';

var dummyImg = 'http://bikehub-s8of.onrender.com/public/uploads/82dbc8ade72535e280308d7a2c7ee897-removebg-preview-(5).png-1698831250478.png'

const BikeCards = ({title, dataArr, isVertical, addItemToFav, checkISFav}) => {
    console.log('====================================');
    console.log("dataArr-> > ",dataArr[0]);
    console.log('====================================');
    const navigation = useNavigation();
    
    const checkIsFav = () => {
        let magenicVendors = dataArr.filter( vendor => vendor['Name'] === 'Magenic' )
    }
    
    return (
        <View style={styles.container} >
            <FlatList
                showsHorizontalScrollIndicator={false}
                data={dataArr}
                contentContainerStyle={!isVertical ? styles.rowItemStyle : styles.columnItemStyle}
                keyExtractor={item => item.name}
                renderItem={({item,index}) => (
                    <TouchableOpacity 
                        key={item.name}  
                        activeOpacity={0.5}
                        onPress={() => {navigation.navigate('BikeDetails', {params: item})}}
                    >
                        <Image source={isVertical ? media.cart_card_ : media.card_image} style={isVertical ? [styles.cardImageFull,  {marginBottom: -40} ] : [styles.cardImage,  {marginBottom: index % 2 == 0 ? -50 : 0} ] } />
               
                        <View style={{position: 'absolute'}} >
                            <View style={isVertical ? styles.verticalContainer : styles.rowContainer} >
                                <TouchableOpacity
                                    activeOpacity={0.5}
                                    onPress={() => {
                                        addItemToFav(item)
                                    }}
                                    style={isVertical ? {position: 'absolute', right: 20, top: 40} : {position: 'absolute', right: 0, top: 20}}
                                >
                                    {checkISFav(item)
                                    ?
                                    <Icon type={'AntDesign'} name={'hearto'}  size={22} color={colors.primary} />
                                    :
                                    <Icon type={'AntDesign'} name={'hearto'}  size={22} color={colors.gray} />
                                    }
                                </TouchableOpacity>
                                <Image
                                    source={{uri: 'http://bikehub-s8of.onrender.com/public/uploads/82dbc8ade72535e280308d7a2c7ee897-removebg-preview-(5).png-1698831250478.png'}}            
                                />
                                <Image
                                source={{uri: item.image || dummyImg}} 
                                style={{height: 90, width: 90, marginTop: 10, resizeMode: 'contain', marginHorizontal: isVertical ? 15 : 15}} />

                                <View style={{width: '100%', }} >
                                    <Text style={styles.bikeType} >{item.type} Bike</Text>
                                    <Text numberOfLines={2} style={styles.bikeName} >{item.name}</Text>
                                    <Text style={styles.bikePrice} >$ {item.price}</Text>
                                </View>
                            </View>
                        </View>
                    </TouchableOpacity>
                )}
            />
        </View>
    )
}

const styles =  StyleSheet.create({
    container: {
        marginTop: -20,
    },
    title: {
        fontSize: 16,
        fontFamily: Poppins.SemiBold,
        color: colors.black,
    },
    rowItemStyle: {
        paddingBottom: 44, 
        flexDirection: 'row', 
        flexWrap: 'wrap', 
        alignItems: 'center', 
        justifyContent: 'space-between',
    },
    columnItemStyle: {
        paddingBottom: 20, 
        paddingTop: 40,
    },

    verticalContainer: {
        padding: 10, 
        width: '100%', 
        alignItems: 'center', 
        height: 200,
        flexDirection: 'row'
    },
    rowContainer: { 
        padding: 10, 
        width: '100%', 
        alignItems: 'center', 
        justifyContent: 'center', 
    },
    itemContainer: {
        borderRadius: 10,
        padding: 20,
        marginBottom: 24,
        width: screenWidth/2-40,
        height: screenWidth/2+15,
        borderWidth: 1,
        borderColor: colors.dark_gray,
        backgroundColor: colors.card_bg,
        //transform: [{rotate: '-5deg'}]
    },
    cardImageFull: {
        width: screenWidth-40,
        height: 200,
      
        resizeMode: 'contain'
    },
    cardImage: {
        width: screenWidth/2-25,
        height: screenWidth/2+30,
        resizeMode: 'contain'
    },
    bikeName: {
        fontSize: 13,
        fontFamily: Poppins.SemiBold,
        color: colors.white,
    },
    bikeType: {
        fontSize: 11,
        fontFamily: Poppins.SemiBold,
        color: '#ffffff60',
    },
    bikePrice: {
        fontSize: 12,
        fontFamily: Poppins.SemiBold,
        color: '#ffffff90',
    },
})

export default BikeCards