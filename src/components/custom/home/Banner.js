import React, {useState} from 'react'
import { Text, View, SafeAreaView, StyleSheet, TouchableOpacity, Image, ScrollView, Dimensions, useColorScheme, FlatList, } from 'react-native'

import Icon from '../../../utils/icons';

import { media } from '../../../global/media';
import { colors } from '../../../global/colors';
import { Poppins } from '../../../global/fontFamily';
import { screenWidth } from '../../../global/constants';

import { useNavigation } from '@react-navigation/native';

import LinearGradient from 'react-native-linear-gradient';
import Parallelogram from '../../global/Parallelogram';


const Banner = ({showToastMsg}) => {

    const navigation = useNavigation();
    return (
        <View style={{width: '100%'}} >
            {/* <View style={styles.container} >
                <Image source={media.bicycle_active} style={styles.bicycle} />
            </View>
            <Parallelogram />

            <View style={{position: 'absolute', bottom: 25, left: 20}} >
                    <Text style={styles.offerStyle} >30% Off</Text>
                </View> */}
            <View style={{position: 'absolute', left: -70}}>
            <Image source={media.banner_card} style={styles.banner_card} />
            </View>
        </View>
    )
}

const styles =  StyleSheet.create({
    container: {
        marginTop: 15,
        width: '100%',
        justifyContent: 'center', 
        alignItems: 'center',
        padding: 20,
        height: 160,
        borderTopLeftRadius: 14,
        borderTopRightRadius: 14,
        backgroundColor: colors.card_bg,
    },
    banner_card: {
        width: 460,
        height: 330,
        resizeMode: 'contain'
    },
    offerStyle: {
        fontSize: 26,
        fontFamily: Poppins.SemiBold,
        color: colors.white,
    },
    bicycle: {
        height: 100,
        width: 100
    }
})

export default Banner