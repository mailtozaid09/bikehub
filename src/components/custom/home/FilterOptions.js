import React, {useState} from 'react'
import { Text, View, SafeAreaView, StyleSheet, TouchableOpacity, Image, ScrollView, Dimensions, useColorScheme, FlatList, } from 'react-native'

import Icon from '../../../utils/icons';

import { media } from '../../../global/media';
import { colors } from '../../../global/colors';
import { Poppins } from '../../../global/fontFamily';
import { screenWidth } from '../../../global/constants';

import { useNavigation } from '@react-navigation/native';

const FilterOptions = ({currentOption, filterOptions, onFilterOption}) => {

    const navigation = useNavigation();

    return (
        <View style={styles.container} >
            <FlatList
                horizontal
                showsHorizontalScrollIndicator={false}
                contentContainerStyle={{ width: '100%', flexDirection: 'row', alignItems: 'flex-end', justifyContent: 'space-between'}}
                data={filterOptions}
                keyExtractor={item => item.title}
                renderItem={({item, index}) => (
                    <TouchableOpacity 
                        activeOpacity={0.5} 
                        onPress={() => {onFilterOption(item.title)}}
                        style={[styles.filterContainer, styles.shadowStyle, {backgroundColor: item.title == currentOption ? colors.primary : colors.dark_blue, marginBottom: (index+1)*12}]}  key={index}  >
                        {item.title == 'All' 
                        ?
                            <Text style={[styles.title,]} >{item.title}</Text>
                        :
                            <Image source={item.title == currentOption ? item.iconActive : item.icon} style={{height: 24, width: 24}} />
                        }
                    </TouchableOpacity>
                )}
            />
        </View>
    )
}

const styles =  StyleSheet.create({
    container: {
        marginTop: 250,
        width: '100%',
        flexDirection: 'row',
        alignItems: 'flex-end',
        justifyContent: 'space-between',
    },
    filterContainer: {
        marginRight: 10,
        height: 50,
        width: 50,
        borderRadius: 8,
        alignItems: 'center', 
        justifyContent: 'center',
        borderWidth: 1,
        borderColor: colors.dark_gray,
    },
    shadowStyle: {
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.34,
        shadowRadius: 1.27,
        elevation: 8,
    },
    title: {
        fontSize: 14,
        fontFamily: Poppins.SemiBold,
        color: colors.white,
    },
})

export default FilterOptions