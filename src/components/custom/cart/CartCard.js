import React, {useState} from 'react'
import { Text, View, SafeAreaView, StyleSheet, TouchableOpacity, Image, ScrollView, Dimensions, useColorScheme, FlatList, } from 'react-native'

import Icon from '../../../utils/icons';

import { media } from '../../../global/media';
import { colors } from '../../../global/colors';
import { Poppins } from '../../../global/fontFamily';
import { screenWidth } from '../../../global/constants';

import { useNavigation } from '@react-navigation/native';


const CartCard = ({title, dataArr, isConfirmOrder, incrementCartItemFunction, decrementCartItemFunction}) => {

    const navigation = useNavigation();

    const [count, setCount] = useState(1);
    
    const CartButtons = ({item}) => {
        return(
            <View style={{flexDirection: 'row', alignItems: 'center', }} >
                <TouchableOpacity
                    activeOpacity={0.5}
                    onPress={() => {
                        decrementCartItemFunction(item);
                        
                    }}
                    style={styles.minusIcon}
                >
                    <Icon type={'AntDesign'} name={'minus'}  size={16} color={colors.white} />
                </TouchableOpacity>

                <View style={{width: 40, alignItems: 'center',  marginHorizontal: 0}} >
                    <Text style={{fontSize: 14, fontFamily: Poppins.SemiBold, color: colors.white,}} >{item.quantity}</Text>
                </View>
                
                <TouchableOpacity
                    activeOpacity={0.5}
                    onPress={() => {
                        incrementCartItemFunction(item);
                    }}
                    style={styles.plusIcon}
                >
                    <Icon type={'AntDesign'} name={'plus'}  size={16} color={colors.white} />
                </TouchableOpacity>
            </View>
        )
    }
    
    return (
        <View style={styles.container} >
            <FlatList
                showsHorizontalScrollIndicator={false}
                data={dataArr}
                style={{flex: 1, width: '100%'}}
                contentContainerStyle={{paddingHorizontal: 20,width: '100%', }}
                keyExtractor={item => item.name}
                renderItem={({item, index}) => (
                    <View style={[styles.cartContainer, {borderBottomWidth: dataArr?.length -1 == index ? 0 : 1, borderColor: '#373f53', }]} key={item.name}  >
                        <View style={styles.imageContainer} >
                            <Image source={{uri: item.bikeIcon}} style={{height: 80, width: 80, resizeMode: 'contain'}} />
                        </View>
                        
                        <View style={{flex: 1, }} >
                            <View style={{}} >
                                <Text numberOfLines={1} style={styles.bikeName} >{item.bikeName}</Text>
                            </View>
                            <View style={{flexDirection: 'row', alignItems: 'flex-end', justifyContent: 'space-between'}} >
                                <View style={{}} >
                                    <Text style={styles.bikeType} >{item.bikeType} Bike</Text>
                                    <Text style={styles.bikePrice} >$ {item.bikePrice}</Text>
                                </View>
                                {!isConfirmOrder && <CartButtons item={item} />}
                            </View>
                        </View>
                    </View>
                )}
            />
        </View>
    )
}

const styles =  StyleSheet.create({
    container: {
        // width: screenWidth,
        alignItems: 'center',
        flex: 1
    },
    title: {
        fontSize: 16,
        fontFamily: Poppins.SemiBold,
        color: colors.black,
    },
    cartContainer: {
        borderRadius: 10,
        paddingVertical: 22,
        // width: screenWidth-40,
        flexDirection: 'row',
        alignItems: 'center',
    },
    cardImage: {
        width: screenWidth-40,
        height: 200,
        resizeMode: 'contain'
    },
    bikeName: {
        fontSize: 16,
        fontFamily: Poppins.SemiBold,
        color: colors.white,
    },
    bikeType: {
        fontSize: 14,
        fontFamily: Poppins.SemiBold,
        color: '#ffffff60',
    },
    bikePrice: {
        fontSize: 16,
        fontFamily: Poppins.SemiBold,
        color: '#ffffff90',
    },

    imageContainer: {
        height: 90,
        width: 90,
        borderRadius: 10,
        marginRight: 12,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#373f53'
    },
    plusIcon: {
        height: 22,
        width: 22,
        borderRadius: 4,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: colors.primary
    },
    minusIcon: {
        height: 22,
        width: 22,
        borderRadius: 4,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#373f53'
    }
})

export default CartCard