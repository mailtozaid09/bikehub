import React, {useState} from 'react'
import { Text, View, SafeAreaView, StyleSheet, TouchableOpacity, Image, ScrollView, Dimensions, useColorScheme, FlatList, } from 'react-native'

import Icon from '../../../utils/icons';

import { media } from '../../../global/media';
import { colors } from '../../../global/colors';
import { Poppins } from '../../../global/fontFamily';
import { screenWidth } from '../../../global/constants';

import { useNavigation } from '@react-navigation/native';


const ProductCards = ({title, dataArr, isVertical, addItemToFav, checkISFav}) => {

    const navigation = useNavigation();
    

    return (
        <View style={styles.container} >
            <FlatList
                showsHorizontalScrollIndicator={false}
                data={dataArr}
                contentContainerStyle={styles.rowItemStyle}
                keyExtractor={item => item.name}
                renderItem={({item,index}) => (
                    <View style={styles.cardContainer} >
                         <Image
                            source={
                                item.image_icon 
                                ? item.image_icon 
                                : {uri: item.image}
                            } 
                            style={{height: 50, width: 50, marginRight: 10, resizeMode: 'contain', }} />

                            <View style={{flex: 1, }} >
                                <View style={{flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between'}}>
                                    <Text style={styles.bikeType} >{item.type} Bike</Text>
                                    <Text style={styles.bikePrice} >$ {item.price}</Text>
                                </View>
                                <Text numberOfLines={1} style={styles.bikeName} >{item.name}</Text>
                            </View>
                    </View>
                )}
            />
        </View>
    )
}

const styles =  StyleSheet.create({
    container: {
        marginTop: 20,
        flex: 1,
        width: screenWidth,
    },
    title: {
        fontSize: 16,
        fontFamily: Poppins.SemiBold,
        color: colors.black,
    },
    rowItemStyle: {
        padding: 20,
    },
    cardContainer: {
        width: '100%',
        flexDirection: 'row',
        padding: 20,
        paddingBottom: 15,
        marginBottom: 15,
        borderRadius: 14,
        borderWidth: 1,
        borderColor: colors.dark_gray,
        backgroundColor: colors.dark_blue,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.34,
        shadowRadius: 1.27,
        elevation: 8,
    },
    cardImage: {
        width: screenWidth/2-25,
        height: screenWidth/2+30,
        resizeMode: 'contain'
    },
    bikeName: {
        fontSize: 13,
        fontFamily: Poppins.SemiBold,
        color: colors.white,
    },
    bikeType: {
        fontSize: 11,
        fontFamily: Poppins.SemiBold,
        color: '#ffffff60',
    },
    bikePrice: {
        fontSize: 12,
        fontFamily: Poppins.SemiBold,
        color: '#ffffff90',
    },
})

export default ProductCards