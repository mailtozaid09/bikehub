import React, { useState } from 'react';
import { TouchableOpacity, StyleSheet, Dimensions, Text, View, Touchable, useColorScheme, } from 'react-native';

import { screenWidth } from '../../global/constants';
import { fontSize, Poppins } from '../../global/fontFamily';
import { colors } from '../../global/colors';



const Toast = ({title, }) => {

    return (
        <View style={{position: 'absolute', bottom: 0, width: screenWidth, alignItems: 'center', justifyContent: 'center'}} >
            <View style={styles.toastContainer}>
                <Text style={styles.toastText} >{title}</Text>
            </View>
        </View>
    )
}



const styles = StyleSheet.create({
    toastContainer: {
        height: 50,
        backgroundColor: colors.light_gray,
        borderWidth: 0.5,
        borderRadius: 10,
        alignItems: 'center',
        justifyContent: 'center',
        width: screenWidth-40,
        marginTop: 20,
        marginBottom: 15,
    },
    toastText: {
        fontSize: 18,
        fontFamily: Poppins.Medium,
        color: colors.black,
    }
})

export default Toast
