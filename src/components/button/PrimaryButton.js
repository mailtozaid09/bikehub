import React, { useState } from 'react';
import { TouchableOpacity, StyleSheet, Dimensions, Text, View, Touchable, useColorScheme, ActivityIndicator, } from 'react-native';

import { screenWidth } from '../../global/constants';
import { fontSize, Poppins } from '../../global/fontFamily';
import { colors } from '../../global/colors';



const PrimaryButton = ({title, onPress, disabled, buttonLoader }) => {

    return (
        <TouchableOpacity
            onPress={onPress}
            activeOpacity={0.5}
            style={[styles.buttonContainer, {backgroundColor: disabled ? colors.card_bg : colors.primary}]}
            disabled={buttonLoader ? true : disabled}
        >
            {buttonLoader 
            ?
            <ActivityIndicator size="large"  color={colors.white} />
            :
            <Text style={[styles.buttonText]} >{title}</Text>
            }
        </TouchableOpacity>
    )
}



const styles = StyleSheet.create({
    buttonContainer: {
        height: 50,
        borderRadius: 25,
        alignItems: 'center',
        justifyContent: 'center',
        width: screenWidth-40,
        marginTop: 20,
        marginBottom: 15,
    },
    buttonText: {
        fontSize: 16,
        fontFamily: Poppins.SemiBold,
        color: colors.white,
    }
})

export default PrimaryButton
