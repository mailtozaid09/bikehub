import React, { useState } from 'react';
import { TouchableOpacity, StyleSheet, Dimensions, Text, View, Touchable, useColorScheme, } from 'react-native';

import { screenWidth } from '../../global/constants';
import { fontSize, Poppins } from '../../global/fontFamily';
import { colors } from '../../global/colors';
import Icon from '../../utils/icons';



const ScanButton = ({onPress, }) => {

    return (
        <View style={{alignItems: 'center', justifyContent: 'center', width: screenWidth, position: 'absolute', bottom: 0,}} >
            <TouchableOpacity
                onPress={onPress}
                activeOpacity={0.5}
                style={[styles.buttonContainer]}
            >
                <Icon type={'MaterialCommunityIcons'} name={'qrcode-scan'}  size={26} color={colors.white} />
                <Text style={[styles.buttonText]} >Scan & Pay</Text>
            </TouchableOpacity>
        </View>
    )
}



const styles = StyleSheet.create({
    buttonContainer: {
        height: 50,
        backgroundColor: colors.black,
        borderRadius: 10,
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection: 'row',
        paddingHorizontal: 20,
        marginTop: 20,
        marginBottom: 15,
    },
    buttonText: {
        fontSize: 16,
        marginLeft: 12,
        fontFamily: Poppins.SemiBold,
        color: colors.white,
    }
})

export default ScanButton
