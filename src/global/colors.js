export const colors = {
  primary: '#151517',
  bg_color: '#f5f5f5',

  black: '#151517',
  white: '#ffffff',
  
  gray: '#ececec',
  light_gray: '#f1f1f1',
  dark_gray: '#979797',
  

  dark_blue: '#20293B',

  light_gray: '#A1B4C9',

  palette1: '#3f90e9',
  palette2: '#27292d',
  palette3: '#a1b4c9',
  palette4: '#3bc8ed',
  palette5: '#2737e5',

  palette6: '#252e42',
  palette7: '#2f3c52',
  palette8: '#20293b',
  palette9: '#6f8093',
  
  primary_light: '#3FAC6450',
  
  primary_dark: '#4B4CED',
  
  dark_blue: '#20293B',
  dark_blue_1: '#2F3C52',



  card_bg: '#363E5199',

  cream: '#E8B88D',
  light_cream: '#F0E3D5',

  light_red: '#FEA7A9',


  

  
 
  blue: '#81b3f3',
  red: '#f29999',
  orange: '#f7c191',
  reddish: '#ED4545',
}