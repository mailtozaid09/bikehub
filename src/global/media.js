export const media = {
    

    getstarted: require('../../assets/images/getstarted.png'),
    bg_image: require('../../assets/images/bg_image.png'),
    banner_card: require('../../assets/images/banner_card.png'),

    card_image: require('../../assets/images/card_image.png'),
    cart_card: require('../../assets/images/cart_card.png'),

    cart_card1: require('../../assets/images/cart_card1.png'),
    cart_card2: require('../../assets/images/cart_card2.png'),

    cart_card_: require('../../assets/images/cart_card_.png'),


    buyer: require('../../assets/images/buyer.png'),
    seller: require('../../assets/images/seller.png'),


    helmet_image: require('../../assets/images/bikes/helmet_image.png'),
    electric_bike: require('../../assets/images/bikes/electric_bike.png'),
    mountain_bike: require('../../assets/images/bikes/mountain_bike.png'),
    road_bike: require('../../assets/images/bikes/road_bike.png'),




    hi_lottie: require('../../assets/lottie/hi_lottie.json'),
    hitext_lottie: require('../../assets/lottie/hitext_lottie.json'),
    
    login: require('../../assets/lottie/login.json'),
    otp: require('../../assets/lottie/otp.json'),

    home: require('../../assets/icons/home.png'),
    home_active: require('../../assets/icons/home_active.png'),

    admin: require('../../assets/icons/admin.png'),
    admin_active: require('../../assets/icons/admin_active.png'),
    
    reminder: require('../../assets/icons/reminder.png'),
    reminder_active: require('../../assets/icons/reminder_active.png'),
    
    heart: require('../../assets/icons/heart_active.png'),
    heart_active: require('../../assets/icons/heart_active.png'),

    user: require('../../assets/icons/user.png'),
    user_active: require('../../assets/icons/user_active.png'),

    logout: require('../../assets/icons/logout.png'),




    road: require('../../assets/icons/road.png'),
    battery: require('../../assets/icons/battery.png'),
    helmet: require('../../assets/icons/helmet.png'),
    mountain: require('../../assets/icons/mountain.png'),
    road_active: require('../../assets/icons/road_active.png'),
    battery_active: require('../../assets/icons/battery_active.png'),
    helmet_active: require('../../assets/icons/helmet_active.png'),
    mountain_active: require('../../assets/icons/mountain_active.png'),


    bicycle: require('../../assets/icons/bicycle.png'),
    cart: require('../../assets/icons/cart.png'),
    map: require('../../assets/icons/map.png'),
    doc: require('../../assets/icons/doc.png'),
    person: require('../../assets/icons/person.png'),
    bicycle_active: require('../../assets/icons/bicycle_active.png'),
    cart_active: require('../../assets/icons/cart_active.png'),
    map_active: require('../../assets/icons/map_active.png'),
    doc_active: require('../../assets/icons/doc_active.png'),
    person_active: require('../../assets/icons/person_active.png'),


    search: require('../../assets/icons/search.png'),
    arrow_left: require('../../assets/icons/arrow_left.png'),
    arrow_right: require('../../assets/icons/arrow_right.png'),


    login_lottie: require('../../assets/lottie/login_lottie.json'),
    dark_mode_lottie: require('../../assets/lottie/dark_mode_lottie.json'),
    empty_lottie: require('../../assets/lottie/empty_lottie.json'),
    edit_lottie: require('../../assets/lottie/edit_lottie.json'),
    error_lottie: require('../../assets/lottie/error_lottie.json'),
    error_lottie: require('../../assets/lottie/error_lottie.json'),
    logout_lottie: require('../../assets/lottie/logout_lottie.json'),
    notification_lottie: require('../../assets/lottie/notification_lottie.json'),
    success_lottie: require('../../assets/lottie/success_lottie.json'),


}
