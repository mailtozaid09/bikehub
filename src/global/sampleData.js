import { colors } from "./colors";
import { media } from "./media";

export const bikes_array = [
  {
    "name": "Rugged Mountain Bike",
    "type": "Mountain",
    "image_icon": media.mountain_bike,
    "description": "A sturdy mountain bike built for off-road adventures.",
    "price": 699.99,
    "specs": {
      "frame": "Aluminum",
      "wheelsize": "27.5 inches",
      "gearset": "Shimano Deore XT",
      "suspension": "Front and rear",
      "brakes": "Hydraulic disc"
    }
  },
  {
    "name": "High-Speed Road Bike",
    "type": "Road",
    "image_icon": media.road_bike,
    "description": "A lightweight road bike designed for speed and agility.",
    "price": 999.99,
    "specs": {
      "frame": "Carbon Fiber",
      "wheelsize": "700c",
      "gearset": "Shimano Ultegra",
      "brakes": "Caliper"
    }
  },
  {
    "name": "Smith - Tade1",
    "type": "Helmet",
    "image_icon": media.helmet_image,
    "description": "A lightweight road bike designed for speed and agility.",
    "price": 999.99,
    "specs": {
      "frame": "Carbon Fiber",
      "wheelsize": "700c",
      "gearset": "Shimano Ultegra",
      "brakes": "Caliper"
    }
  },
  {
    "name": "Electric Commuter Bike",
    "type": "Electric",
    "image_icon": media.electric_bike,
    "description": "An electric bike perfect for daily commuting.",
    "price": 1299.99,
    "specs": {
      "frame": "Aluminum",
      "wheelsize": "26 inches",
      "motor": "250W electric motor",
      "battery": "36V Lithium-ion",
      "brakes": "Hydraulic disc"
    }
  },
  {
    "name": "Trailblazer Mountain Bike",
    "type": "Mountain",
    "image_icon": media.mountain_bike,
    "description": "A trail-ready mountain bike for adrenaline seekers.",
    "price": 799.99,
    "specs": {
      "frame": "Steel",
      "wheelsize": "29 inches",
      "gearset": "SRAM GX",
      "suspension": "Front",
      "brakes": "Mechanical disc"
    }
  },
  {
    "name": "Touring Road Bike",
    "type": "Road",
    "image_icon": media.road_bike,
    "description": "A road bike designed for long-distance touring.",
    "price": 1199.99,
    "specs": {
      "frame": "Chromoly Steel",
      "wheelsize": "700c",
      "gearset": "Shimano Sora",
      "brakes": "Caliper"
    }
  },
  {
    "name": "Smith - Tade2",
    "type": "Helmet",
    "image_icon": media.helmet_image,
    "description": "A lightweight road bike designed for speed and agility.",
    "price": 999.99,
    "specs": {
      "frame": "Carbon Fiber",
      "wheelsize": "700c",
      "gearset": "Shimano Ultegra",
      "brakes": "Caliper"
    }
  },
  {
    "name": "Electric Mountain Bike",
    "type": "Electric",
    "image_icon": media.electric_bike,
    "description": "An electric mountain bike for conquering tough trails.",
    "price": 1499.99,
    "specs": {
      "frame": "Aluminum",
      "wheelsize": "27.5 inches",
      "motor": "500W electric motor",
      "battery": "48V Lithium-ion",
      "suspension": "Front and rear",
      "brakes": "Hydraulic disc"
    }
  },
  {
    "name": "City Cruiser Bike",
    "type": "Electric",
    "image_icon": media.electric_bike,
    "description": "A stylish electric cruiser bike for city rides.",
    "price": 899.99,
    "specs": {
      "frame": "Aluminum",
      "wheelsize": "26 inches",
      "motor": "350W electric motor",
      "battery": "36V Lithium-ion",
      "brakes": "Hydraulic disc"
    }
  },
  {
    "name": "Smith - Tade3",
    "type": "Helmet",
    "image_icon": media.helmet_image,
    "description": "A lightweight road bike designed for speed and agility.",
    "price": 999.99,
    "specs": {
      "frame": "Carbon Fiber",
      "wheelsize": "700c",
      "gearset": "Shimano Ultegra",
      "brakes": "Caliper"
    }
  },
  {
    "name": "Smith - Tade6",
    "type": "Helmet",
    "image_icon": media.helmet_image,
    "description": "A lightweight road bike designed for speed and agility.",
    "price": 999.99,
    "specs": {
      "frame": "Carbon Fiber",
      "wheelsize": "700c",
      "gearset": "Shimano Ultegra",
      "brakes": "Caliper"
    }
  },
  {
    "name": "All-Terrain Road Bike",
    "type": "Road",
    "image_icon": media.road_bike,
    "description": "A versatile road bike built to handle various terrains.",
    "price": 1099.99,
    "specs": {
      "frame": "Aluminum",
      "wheelsize": "700c",
      "gearset": "Shimano Tiagra",
      "brakes": "Disc"
    }
  },
  {
    "name": "Hardtail Mountain Bike",
    "type": "Mountain",
    "image_icon": media.mountain_bike,
    "description": "A hardtail mountain bike for smooth trails and rough terrain.",
    "price": 749.99,
    "specs": {
      "frame": "Aluminum",
      "wheelsize": "29 inches",
      "gearset": "SRAM NX",
      "suspension": "Front",
      "brakes": "Hydraulic disc"
    }
  },
  {
    "name": "Hardtail Mountain Bike 2",
    "type": "Mountain",
    "image_icon": media.mountain_bike,
    "description": "A hardtail mountain bike for smooth trails and rough terrain.",
    "price": 749.99,
    "specs": {
      "frame": "Aluminum",
      "wheelsize": "29 inches",
      "gearset": "SRAM NX",
      "suspension": "Front",
      "brakes": "Hydraulic disc"
    }
  },
  {
    "name": "Speed Demon Road Bike",
    "type": "Road",
    "image_icon": media.road_bike,
    "description": "A high-performance road bike for competitive cyclists.",
    "price": 1399.99,
    "specs": {
      "frame": "Carbon Fiber",
      "wheelsize": "700c",
      "gearset": "Shimano Dura-Ace",
      "brakes": "Caliper"
    }
  },
  {
    "name": "Folding Electric Bike",
    "type": "Electric",
    "image_icon": media.electric_bike,
    "description": "A foldable electric bike for easy storage and transportation.",
    "price": 999.99,
    "specs": {
      "frame": "Aluminum",
      "wheelsize": "20 inches",
      "motor": "250W electric motor",
      "battery": "36V Lithium-ion",
      "brakes": "Mechanical disc"
    }
  },
  {
    "name": "Smith - Tade4",
    "type": "Helmet",
    "image_icon": media.helmet_image,
    "description": "A lightweight road bike designed for speed and agility.",
    "price": 999.99,
    "specs": {
      "frame": "Carbon Fiber",
      "wheelsize": "700c",
      "gearset": "Shimano Ultegra",
      "brakes": "Caliper"
    }
  },
  {
    "name": "Downhill Mountain Bike",
    "type": "Mountain",
    "image_icon": media.mountain_bike,
    "description": "A downhill mountain bike built for extreme descents.",
    "price": 1699.99,
    "specs": {
      "frame": "Carbon Fiber",
      "wheelsize": "27.5 inches",
      "gearset": "SRAM X01",
      "suspension": "Front and rear",
      "brakes": "Hydraulic disc"
    }
  },
  {
    "name": "Classic Cruiser Bike",
    "type": "Electric",
    "image_icon": media.electric_bike,
    "description": "A classic-style electric cruiser bike for relaxed rides.",
    "price": 799.99,
    "specs": {
      "frame": "Steel",
      "wheelsize": "26 inches",
      "motor": "350W electric motor",
      "battery": "36V Lithium-ion",
      "brakes": "Caliper"
    }
  },
  {
    "name": "Gravel Adventure Bike",
    "type": "Road",
    "image_icon": media.road_bike,
    "description": "A gravel bike designed for off-road adventures and exploration.",
    "price": 1099.99,
    "specs": {
      "frame": "Aluminum",
      "wheelsize": "700c",
      "gearset": "Shimano GRX",
      "brakes": "Disc"
    }
  },
  {
    "name": "Fat Tire Electric Bike",
    "type": "Electric",
    "image_icon": media.electric_bike,
    "description": "A fat tire electric bike for enhanced stability on various terrains.",
    "price": 1299.99,
    "specs": {
      "frame": "Aluminum",
      "wheelsize": "26 inches",
      "motor": "500W electric motor",
      "battery": "48V Lithium-ion",
      "brakes": "Hydraulic disc"
    }
  },
  {
    "name": "Smith - Tade5",
    "type": "Helmet",
    "image_icon": media.helmet_image,
    "description": "A lightweight road bike designed for speed and agility.",
    "price": 999.99,
    "specs": {
      "frame": "Carbon Fiber",
      "wheelsize": "700c",
      "gearset": "Shimano Ultegra",
      "brakes": "Caliper"
    }
  },
]
