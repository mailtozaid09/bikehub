import React,{useEffect, useState} from 'react'
import { Text, View, StyleSheet, ScrollView, TouchableOpacity, Image, LogBox, Platform, useColorScheme,  } from 'react-native'

import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { getFocusedRouteNameFromRoute } from '@react-navigation/native';

import AsyncStorage from '@react-native-async-storage/async-storage';

import { media } from '../../global/media';
import { colors } from '../../global/colors';

import HomeStack from '../home';
import ProfileStack from '../profile';


import CartStack from '../cart';
import OrdersStack from '../orders';
import AdminStack from '../admin';

import auth from '@react-native-firebase/auth';
import { useSelector } from 'react-redux';

const Tab = createBottomTabNavigator();


export default function Tabbar({navigation}) {

    const [userDetails, setUserDetails] = useState({});
    const [isAdmin, setIsAdmin] = useState(false);

    const current_user_details = useSelector(state => state.auth.current_user_details);
    


    useEffect(() => {
        console.log('====================================');
        console.log("current_user_details > ",current_user_details);
        console.log('====================================');
        setIsAdmin(current_user_details?.isAdmin)
    }, [])
    
    const getUserDetails = () => {     
        auth().onAuthStateChanged((user) => {
            console.log('====================================');
            console.log("user > > > > ",user);
            console.log('====================================');
            if (user) {
                setUserDetails(user)
                setIsAdmin(user?.phoneNumber == '+919027346976' ? true : false)
            }
        });
    }
    
    
        
    return (
        <Tab.Navigator
            screenOptions={({ route }) => ({
                tabBarIcon: ({ focused, color, size }) => {
                let iconName, routeName, height = 24, width = 24 ;
    
                if (route.name === 'HomeStack') {
                    iconName = focused ? media.bicycle_active : media.bicycle
                    routeName = 'Home' 
                    height = 34, width = 34 ;
                } 
                
                else if (route.name === 'CartStack') {
                    iconName = focused ? media.cart_active : media.cart
                    routeName = 'Cart'
                }
                else if (route.name === 'OrdersStack') {
                    iconName = focused ? media.doc_active : media.doc
                    routeName = 'Orders' 
                  
                }
                else if (route.name === 'AdminStack') {
                    iconName = focused ? media.admin_active : media.admin
                    routeName = 'Admin' 
                    height = 28, width = 28 ;
                }
                else if (route.name === 'ProfileStack') {
                    iconName = focused ? media.person_active : media.person
                    routeName = 'Profile'
                }
                return(
                    <View style={[{alignItems: 'center', justifyContent: 'center', height: 44, width: 44, borderRadius: 22, flexDirection: 'row' },  ]} >
                        <View style={focused ? styles.activeContainer : styles.container} >
                            
                        </View>
                        <View style={{position: 'absolute',}} >
                            <Image source={iconName} style={{height: height, width: width,  marginBottom: focused ? 10 : 0}}/>
                        </View>
                    </View>
                );
                },
            })}
        >
            <Tab.Screen
                name="HomeStack"
                component={HomeStack}
                options={({navigation, route}) => ({
                    headerShown: false,
                    headerRight: () => null,
                    tabBarShowLabel: false,
                    tabBarStyle: ((route) => {
                        const routeName = getFocusedRouteNameFromRoute(route) ?? ""
                        if (routeName === 'BikeDetails') {
                          return { display: "none",  }
                        }
                        return styles.tabbarStyle
                      })(route)
                })}
            />

            
            
            {!isAdmin 
            ? 
            <>
            <Tab.Screen
                name="CartStack"
                component={CartStack}
                options={({navigation, route}) => ({
                    headerShown: false,
                    tabBarShowLabel: false,
                    tabBarStyle: ((route) => {
                        const routeName = getFocusedRouteNameFromRoute(route) ?? ""
                        if (routeName === 'Checkout' || routeName === 'ShippingAddress' || routeName === 'ConfirmOrder' || routeName === 'AddShippingAddress' || routeName === 'OrderConfirmation') {
                          return { display: "none",  }
                        }
                        return styles.tabbarStyle
                      })(route)
                })}
            />
            <Tab.Screen
                name="OrdersStack"
                component={OrdersStack}
                options={({navigation, route}) => ({
                    headerShown: false,
                    tabBarShowLabel: false,
                    tabBarStyle: ((route) => {
                        const routeName = getFocusedRouteNameFromRoute(route) ?? ""
                        if (routeName === 'AddCategory') {
                          return { display: "none",  }
                        }
                        return styles.tabbarStyle
                      })(route)
                })}
            />
            </>
            :
            <Tab.Screen
                name="AdminStack"
                component={AdminStack}
                options={({navigation, route}) => ({
                    headerShown: false,
                    tabBarShowLabel: false,
                    tabBarStyle: ((route) => {
                        const routeName = getFocusedRouteNameFromRoute(route) ?? ""
                        if (routeName === 'OrderScreen' || routeName == 'OrderDetails' || routeName === 'AddProductScreen' || routeName === 'AddCategoryScreen') {
                        return { display: "none",  }
                        }
                        return styles.tabbarStyle
                    })(route)
                })}
            />
            }
            <Tab.Screen
                name="ProfileStack"
                component={ProfileStack}
                options={({navigation, route}) => ({
                    headerShown: false,
                    tabBarShowLabel: false,
                    tabBarStyle: ((route) => {
                        const routeName = getFocusedRouteNameFromRoute(route) ?? ""
                        if (routeName === 'AddCategory') {
                          return { display: "none",  }
                        }
                        return styles.tabbarStyle
                      })(route)
                })}
            />
        </Tab.Navigator>
    );
}



const styles = StyleSheet.create({
    tabbarStyle: {
        height: Platform.OS == 'ios' ? 90 : 70,
        paddingHorizontal: 12, 
        paddingTop: Platform.OS == 'ios' ? 15 : 0, 
        paddingBottom: Platform.OS == 'android' ? 2 : 30,
        borderTopWidth: 0.1,
        borderColor: colors.gray,
        backgroundColor: colors.dark_blue,
    },
    activeContainer: {
        height: 44,
        width: 60,
        borderRadius: 10,
        marginBottom: 10,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: colors.primary,
        transform: [{rotate: '-10deg'}]
    },
    container: {

    }
})