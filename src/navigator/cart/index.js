import React from 'react';
import { SafeAreaView, Text, View, Button, TouchableOpacity, Image, StyleSheet, Dimensions, Platform, ScrollView, KeyboardAvoidingView, useColorScheme, } from 'react-native';

import { createStackNavigator } from '@react-navigation/stack';

import { colors } from '../../global/colors';
import { Poppins } from '../../global/fontFamily';
import CartScreen from '../../screens/cart';
import CheckoutScreen from '../../screens/cart/Checkout';
import OrderConfirmation from '../../screens/cart/OrderConfirmation';
import ShippingAddress from '../../screens/cart/ShippingAddress';
import AddShippingAddress from '../../screens/cart/AddShippingAddress';
import ConfirmOrder from '../../screens/cart/ConfirmOrder';

const Stack = createStackNavigator();

const CartStack = ({navgation}) => {

    return (
        <Stack.Navigator 
            initialRouteName="Cart" 
        >
            <Stack.Screen
                name="Cart"
                component={CartScreen}
                options={{
                    headerShown: true,
                    headerLeft: () => null,
                    // headerRight: () => null,
                    headerTitle: 'Cart',
                    headerTitleAlign: 'center',
                    headerStyle: styles.headerStyle,
                    headerTitleStyle: styles.headerTitleStyle,
                }}
            />
            <Stack.Screen
                name="ShippingAddress"
                component={ShippingAddress}
                options={{
                    headerShown: false,
                    // headerLeft: () => (<Header title="Checkout" />),
                    // headerRight: () => null,
                    headerTitle: 'Shipping Address',
                    headerStyle: styles.headerStyle,
                    headerTitleStyle: styles.headerTitleStyle,
                }}
            />
            <Stack.Screen
                name="AddShippingAddress"
                component={AddShippingAddress}
                options={{
                    headerShown: false,
                    // headerLeft: () => (<Header title="Checkout" />),
                    // headerRight: () => null,
                    headerTitle: 'Add Shipping Address',
                    headerStyle: styles.headerStyle,
                    headerTitleStyle: styles.headerTitleStyle,
                }}
            />
            <Stack.Screen
                name="ConfirmOrder"
                component={ConfirmOrder}
                options={{
                    headerShown: false,
                    // headerLeft: () => (<Header title="Checkout" />),
                    // headerRight: () => null,
                    headerTitle: 'Confirm Order',
                    headerStyle: styles.headerStyle,
                    headerTitleStyle: styles.headerTitleStyle,
                }}
            />
            <Stack.Screen
                name="Checkout"
                component={CheckoutScreen}
                options={{
                    headerShown: false,
                    // headerLeft: () => (<Header title="Checkout" />),
                    // headerRight: () => null,
                    headerTitle: 'Checkout',
                    headerStyle: styles.headerStyle,
                    headerTitleStyle: styles.headerTitleStyle,
                }}
            />
            <Stack.Screen
                name="OrderConfirmation"
                component={OrderConfirmation}
                options={{
                    headerShown: false,
                    // headerLeft: () => (<Header title="Checkout" />),
                    // headerRight: () => null,
                    headerTitle: 'Order Confirmation',
                    headerStyle: styles.headerStyle,
                    headerTitleStyle: styles.headerTitleStyle,
                }}
            />
        </Stack.Navigator>
    );
}

const styles = StyleSheet.create({
    headerTitleStyle: {
        fontSize: 18, 
        fontFamily: Poppins.Medium,
        color: colors.white,
    },
    headerStyle: {
        backgroundColor: colors.dark_blue,
    }
})

export default CartStack