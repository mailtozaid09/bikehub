import React from 'react';
import { SafeAreaView, Text, View, Button, TouchableOpacity, Image, StyleSheet, Dimensions, Platform, ScrollView, KeyboardAvoidingView, useColorScheme, } from 'react-native';

import { createStackNavigator } from '@react-navigation/stack';

import { colors } from '../../global/colors';
import { Poppins } from '../../global/fontFamily';

import ProfileScreen from '../../screens/profile';



const Stack = createStackNavigator();

const ProfileStack = ({navgation}) => {

    return (
        <Stack.Navigator 
            initialRouteName="Profile" 
        >
            <Stack.Screen
                name="Profile"
                component={ProfileScreen}
                options={{
                    headerShown: true,
                    headerLeft: () => null,
                    headerTitle: 'Profile',
                    headerTitleAlign: 'center',
                    headerStyle: styles.headerStyle,
                    headerTitleStyle: styles.headerTitleStyle,
                }}
            />
        </Stack.Navigator>
    );
}

const styles = StyleSheet.create({
    headerTitleStyle: {
        fontSize: 18, 
        fontFamily: Poppins.SemiBold,
        color: colors.white,
    },
    headerStyle: {
        backgroundColor: colors.dark_blue,
    }
})

export default ProfileStack