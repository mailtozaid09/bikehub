import React from 'react';
import { SafeAreaView, Text, View, Button, TouchableOpacity, Image, StyleSheet, Dimensions, Platform, ScrollView, KeyboardAvoidingView, useColorScheme, } from 'react-native';

import { createStackNavigator } from '@react-navigation/stack';

import { colors } from '../../global/colors';
import { Poppins } from '../../global/fontFamily';
import AdminScreen from '../../screens/admin';
import OrderScreen from '../../screens/admin/OrderScreen';
import AddProductScreen from '../../screens/admin/AddProductScreen';
import AddCategoryScreen from '../../screens/admin/AddCategoryScreen';
import OrderDetails from '../../screens/admin/OrderDetails';

const Stack = createStackNavigator();

const AdminStack = ({navgation}) => {

    return (
        <Stack.Navigator 
            initialRouteName="Admin" 
        >
            <Stack.Screen
                name="Admin"
                component={AdminScreen}
                options={{
                    headerShown: true,
                    headerLeft: () => null,
                    // headerRight: () => null,
                    headerTitle: 'Admin',
                    headerTitleAlign: 'center',
                    headerStyle: styles.headerStyle,
                    headerTitleStyle: styles.headerTitleStyle,
                }}
            />
            <Stack.Screen
                name="OrderScreen"
                component={OrderScreen}
                options={{
                    headerShown: true,
                    // headerLeft: () => null,
                    // headerRight: () => null,
                    headerTintColor: colors.white,
                    headerTitle: 'Order',
                    headerTitleAlign: 'center',
                    headerStyle: styles.headerStyle,
                    headerTitleStyle: styles.headerTitleStyle,
                }}
            />
            <Stack.Screen
                name="OrderDetails"
                component={OrderDetails}
                options={{
                    headerShown: true,
                    // headerLeft: () => null,
                    // headerRight: () => null,
                    headerTintColor: colors.white,
                    headerTitle: 'Order Details',
                    headerTitleAlign: 'center',
                    headerStyle: styles.headerStyle,
                    headerTitleStyle: styles.headerTitleStyle,
                }}
            />
            <Stack.Screen
                name="AddProductScreen"
                component={AddProductScreen}
                options={{
                    headerShown: true,
                    // headerLeft: () => null,
                    // headerRight: () => null,
                    headerTintColor: colors.white,
                    headerTitle: 'Add Product',
                    headerTitleAlign: 'center',
                    headerStyle: styles.headerStyle,
                    headerTitleStyle: styles.headerTitleStyle,
                }}
            />
            <Stack.Screen
                name="AddCategoryScreen"
                component={AddCategoryScreen}
                options={{
                    headerShown: true,
                    // headerLeft: () => null,
                    // headerRight: () => null,
                    headerTintColor: colors.white,
                    headerTitle: 'Add Category',
                    headerTitleAlign: 'center',
                    headerStyle: styles.headerStyle,
                    headerTitleStyle: styles.headerTitleStyle,
                }}
            />
        </Stack.Navigator>
    );
}

const styles = StyleSheet.create({
    headerTitleStyle: {
        fontSize: 18, 
        fontFamily: Poppins.Medium,
        color: colors.white,
    },
    headerStyle: {
        backgroundColor: colors.dark_blue,
    }
})

export default AdminStack