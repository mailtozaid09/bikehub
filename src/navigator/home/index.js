import React from 'react';
import { SafeAreaView, Text, View, Button, TouchableOpacity, Image, StyleSheet, Dimensions, Platform, ScrollView, KeyboardAvoidingView, useColorScheme, } from 'react-native';

import { createStackNavigator } from '@react-navigation/stack';

import { colors } from '../../global/colors';
import { Poppins } from '../../global/fontFamily';
import HomeScreen from '../../screens/home';
import HomeHeader from '../../components/header/HomeHeader';
import BikeDetailsScreen from '../../screens/home/BikeDetails';
import Header from '../../components/header';


const Stack = createStackNavigator();

const HomeStack = ({navgation}) => {

    return (
        <Stack.Navigator 
            initialRouteName="Home" 
        >
            <Stack.Screen
                name="Home"
                component={HomeScreen}
                options={{
                    headerShown: true,
                    headerLeft: () => (<HomeHeader />),
                    headerRight: () => null,
                    headerTitle: '',
                    headerStyle: styles.headerStyle,
                    headerTitleStyle: styles.headerTitleStyle,
                }}
            />
            <Stack.Screen
                name="BikeDetails"
                component={BikeDetailsScreen}
                options={{
                    headerShown: false,
                    headerLeft: () => (<Header title="Bike Details" />),
                    headerRight: () => null,
                    headerTitle: 'Bike Details',
                    headerStyle: styles.headerStyle,
                    headerTitleStyle: styles.headerTitleStyle,
                }}
            />


        </Stack.Navigator>
    );
}

const styles = StyleSheet.create({
    headerTitleStyle: {
        fontSize: 18, 
        fontFamily: Poppins.Medium,
        color: colors.white,
    },
    headerStyle: {
        backgroundColor: colors.dark_blue,
    }
})

export default HomeStack