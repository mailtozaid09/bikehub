import React from 'react';
import { SafeAreaView, Text, View, Button, TouchableOpacity, Image, StyleSheet, Dimensions, Platform, ScrollView, KeyboardAvoidingView, useColorScheme, } from 'react-native';

import { createStackNavigator } from '@react-navigation/stack';

import { colors } from '../../global/colors';
import { Poppins } from '../../global/fontFamily';
import OrdersScreen from '../../screens/orders';
import ViewOrderDetails from '../../screens/orders/ViewOrderDetails';



const Stack = createStackNavigator();

const OrdersStack = ({navgation}) => {

    return (
        <Stack.Navigator 
            initialRouteName="Orders" 
        >
            <Stack.Screen
                name="Orders"
                component={OrdersScreen}
                options={{
                    headerShown: true,
                    headerLeft: () => null,
                    // headerRight: () => null,
                    headerTitle: 'Orders',
                    headerTitleAlign: 'center',
                    headerTintColor: colors.white,
                    headerStyle: styles.headerStyle,
                    headerTitleStyle: styles.headerTitleStyle,
                }}
            />
            <Stack.Screen
                name="ViewOrderDetails"
                component={ViewOrderDetails}
                options={{
                    headerShown: true,
                    //headerLeft: () => null,
                    // headerRight: () => null,
                    headerTitle: 'Orders Details',
                    headerTitleAlign: 'center',
                    headerTintColor: colors.white,
                    headerStyle: styles.headerStyle,
                    headerTitleStyle: styles.headerTitleStyle,
                }}
            />
        </Stack.Navigator>
    );
}

const styles = StyleSheet.create({
    headerTitleStyle: {
        fontSize: 18, 
        fontFamily: Poppins.Medium,
        color: colors.white,
    },
    headerStyle: {
        backgroundColor: colors.dark_blue,
    }
})

export default OrdersStack