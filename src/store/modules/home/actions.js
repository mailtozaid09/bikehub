import { 
	ADD_ITEM_TO_FAV, ADD_ITEM_TO_CART,
	REMOVE_ITEM_FROM_FAV, REMOVE_ITEM_FROM_CART, 
	CLEAR_CART_LIST,
	ADD_SHIPPING_ADDRESS, REMOVE_SHIPPING_ADDRESS,
	SET_CURRENT_ORDER_DETAILS, SET_CURRENT_SHIPPING_ADDRESS,
    INCREMENT_CART_ITEM_QUANTITY, DECREMENT_CART_ITEM_QUANTITY,


} from './actionTypes';

let nextBikeItemId = 0
let nextCartItemId = 0
let nextShipppingAddressId = 0


export function addShippingAddress(params) {
    return {
        type: ADD_SHIPPING_ADDRESS,
        payload: {
            id: ++nextShipppingAddressId,
			name: params.name,
            mobileNumber: params.mobileNumber,
            address: params.address,
            city: params.city,
            addressState: params.addressState,
            pincode: params.pincode,
        },
    };
};

export const deleteShippingAddress = id => {
	return {
		  type: REMOVE_SHIPPING_ADDRESS,
		  payload: {
			id
		  },
	  };
};


export function currentShippingAddress(params) {
    return {
        type: SET_CURRENT_SHIPPING_ADDRESS,
        payload: {
            id: params.id,
			name: params.name,
            email: params.email,
            phone: params.phone,
            street: params.street,
            apartment: params.apartment,
            zip: params.zip,
            city: params.city,
        },
    };
};


export function currentOrderDetails(params) {
    return {
        type: SET_CURRENT_ORDER_DETAILS,
        payload: {
            totalPrice: params.totalPrice,
            subTotalPrice: params.subTotalPrice,
            delivieryPrice: params.delivieryPrice,
            discountPrice: params.discountPrice,
            isDeliveryFree: params.isDeliveryFree,
        },
    };
};




export function addFavItemDetails(params) {
    return {
        type: ADD_ITEM_TO_FAV,
        payload: {
            id: ++nextBikeItemId,
			bikeName: params.bikeName,
			bikeType: params.bikeType,
			bikePrice: params.bikePrice,
			bikeDescription: params.bikeDescription,
			bikeSpecificaton: params.bikeSpecificaton,
        },
    };
};

export const deleteFavItemDetails = name => {
	return {
		  type: REMOVE_ITEM_FROM_FAV,
		  payload: {
			name
		  },
	  };
};


export const incrementCartItemQuantity = params => {
	return {
		  type: INCREMENT_CART_ITEM_QUANTITY,
		  payload: {
            id: params.id,
            quantity: params.quantity,
		  },
	  };
};


export const decrementCartItemQuantity = params => {
	return {
		  type: DECREMENT_CART_ITEM_QUANTITY,
		  payload: {
            id: params.id,
            quantity: params.quantity,
		  },
	  };
};



export function addITemToCart(params) {
    return {
        type: ADD_ITEM_TO_CART,
        payload: {
            quantity: 1,
			id: params.id,
            bikeName: params.bikeName,
			bikeType: params.bikeType,
			bikePrice: params.bikePrice,
			bikeIcon: params.bikeIcon,
			bikeDescription: params.bikeDescription,
			bikeSpecificaton: params.bikeSpecificaton,
        },
    };
};

export const deleteItemFromCart = id => {
	return {
		  type: REMOVE_ITEM_FROM_CART,
		  payload: {
			id
		  },
	  };
};

export const clearCartList = () => {
	return {
		  type: CLEAR_CART_LIST,
		  payload: {},
	  };
};

