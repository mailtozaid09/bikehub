
import { 
    ADD_ITEM_TO_FAV, REMOVE_ITEM_FROM_FAV,
    ADD_ITEM_TO_CART, REMOVE_ITEM_FROM_CART, CLEAR_CART_LIST, 
    ADD_SHIPPING_ADDRESS, REMOVE_SHIPPING_ADDRESS, SET_CURRENT_SHIPPING_ADDRESS, SET_CURRENT_ORDER_DETAILS, INCREMENT_CART_ITEM_QUANTITY, DECREMENT_CART_ITEM_QUANTITY,
    
} from "./actionTypes";

const INITIAL_STATE = {
    current_shipping_address: {},

    current_order_details: {},

    shipping_address_list: [],

    favorite_list: [],
    favorite_list_1: [],
    cart_list: [],
};

export default function home(state = INITIAL_STATE, action) {
    switch (action.type) {

        case ADD_SHIPPING_ADDRESS: {
            const {
                id, name, mobileNumber, address, city, addressState, pincode,
            } = action.payload
            return {
                ...state,
                shipping_address_list: [
                    ...state.shipping_address_list, {
                        id, name, mobileNumber, address, city, addressState, pincode,
                    }
                ]
            }
        };

        case REMOVE_SHIPPING_ADDRESS: {
            const { id } = action.payload
                return {
                    ...state,
                    shipping_address_list: state.shipping_address_list.filter((address) => address.id != id)
            };
        }

        case SET_CURRENT_SHIPPING_ADDRESS: {
            const {
                id, name, email, phone, street, apartment, zip, city,
            } = action.payload

                return {
                    ...state,
                    current_shipping_address: {
                        id, name, email, phone, street, apartment, zip, city,
                    }
            };
        }

        case SET_CURRENT_ORDER_DETAILS: {
            const {
                totalPrice, subTotalPrice, delivieryPrice, discountPrice, isDeliveryFree,
            } = action.payload
            
                return {
                    ...state,
                    current_order_details: {
                        totalPrice, subTotalPrice, delivieryPrice, discountPrice, isDeliveryFree,
                    }
            };
        }



        case ADD_ITEM_TO_FAV: {
            const {
                id, bikeName, bikeType, bikePrice, bikeDescription, bikeSpecificaton, 
            } = action.payload
            return {
                ...state,
                favorite_list: [
                    ...state.favorite_list, {
                        id, bikeName, bikeType, bikePrice, bikeDescription, bikeSpecificaton, 
                    }
                ]
            }
        };

        case REMOVE_ITEM_FROM_FAV: {
            const { name } = action.payload
                return {
                    ...state,
                    favorite_list: state.favorite_list.filter((bike) => bike.bikeName != name)
            };
        }


        case ADD_ITEM_TO_CART: {
            const {
                id, quantity, bikeName, bikeType, bikePrice, bikeIcon, bikeDescription, bikeSpecificaton, 
            } = action.payload
            return {
                ...state,
                cart_list: [
                    ...state.cart_list, {
                        id, quantity, bikeName, bikeType, bikePrice, bikeIcon, bikeDescription, bikeSpecificaton, 
                    }
                ]
            }
        };


          
        case INCREMENT_CART_ITEM_QUANTITY:{
            const { 
                quantity, 
            } = action.payload
            const updatedCart = state.cart_list.map(cartItem => {
                if (cartItem.id != action.payload.id) {
                  return cartItem;
                } else {
                  return {
                        ...cartItem,
                        quantity: quantity + 1
                    };
                }
            });
           
            return {
                ...state,
                cart_list: updatedCart
            };
        }

        case DECREMENT_CART_ITEM_QUANTITY:{
            const { 
                id, quantity, 
            } = action.payload
            const updatedCart = state.cart_list.map(cartItem => {
                if (cartItem.id != id) {
                  return cartItem;
                } else {
                    return {
                        ...cartItem,
                        quantity: quantity - 1
                    };
                }
            });

            if(quantity <= 1){
                return {
                    ...state,
                    cart_list: state.cart_list.filter((bike) => bike.id != id)
                };
            } else {
                return {
                    ...state,
                    cart_list: updatedCart
                };
            }
        }

            

        case REMOVE_ITEM_FROM_CART: {
            const { id } = action.payload
                return {
                    ...state,
                    cart_list: state.cart_list.filter((bike) => bike.id != id)
            };
        }


        case CLEAR_CART_LIST: {
            const {  } = action.payload
                return {
                    ...state,
                    cart_list: []
            };
        }



        default:
        return state;
    }
}
