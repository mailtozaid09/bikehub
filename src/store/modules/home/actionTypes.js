

export const ADD_ITEM_TO_FAV_1 = 'ADD_ITEM_TO_FAV_1';

export const ADD_ITEM_TO_FAV = 'ADD_ITEM_TO_FAV';
export const REMOVE_ITEM_FROM_FAV = 'REMOVE_ITEM_FROM_FAV';

export const CLEAR_CART_LIST = 'CLEAR_CART_LIST';

export const ADD_ITEM_TO_CART = 'ADD_ITEM_TO_CART';
export const REMOVE_ITEM_FROM_CART = 'REMOVE_ITEM_FROM_CART';



export const SET_CURRENT_SHIPPING_ADDRESS = 'SET_CURRENT_SHIPPING_ADDRESS';
export const SET_CURRENT_ORDER_DETAILS = 'SET_CURRENT_ORDER_DETAILS';

export const INCREMENT_CART_ITEM_QUANTITY = 'INCREMENT_CART_ITEM_QUANTITY';
export const DECREMENT_CART_ITEM_QUANTITY = 'DECREMENT_CART_ITEM_QUANTITY';

export const ADD_SHIPPING_ADDRESS = 'ADD_SHIPPING_ADDRESS';
export const REMOVE_SHIPPING_ADDRESS = 'REMOVE_SHIPPING_ADDRESS';