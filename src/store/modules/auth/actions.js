import { Alert } from 'react-native';
import { users } from '../../../global/sampleData';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { CURRENT_USER_DETAILS } from './actionTypes';

let nextUserId = 0;


export function currentUserDetails(params) {
  return {
      type: CURRENT_USER_DETAILS,
      payload: {
          id: params.id,
          name: params.name,
          email: params.email,
          profile_url: params.profile_url,
          phone: params.phone,
          isAdmin: params.isAdmin,
      },
  };
};


export function userLoggedIn(user_logged_in) {
	return {
		type: '@auth/USER_LOGGED_IN',
		payload: {
			user_logged_in,
		},
	};
}

export function userId(user_id) {
	return {
		type: '@auth/USER_ID',
		payload: {
			user_id,
		},
	};
}


export function signIn(email, password) {
  const user = users.filter(
    u => u.email === email && u.password === password
  )[0];

  if (user) {
      console.log("user succes=> " , user);
       AsyncStorage.setItem("user_details", JSON.stringify(user));
  }else{
    Alert.alert('Warning', 'Incorrect email or password');
  }

  return {
    type: '@auth/SIGN_IN',
    payload: {
      user,
    },
  };
}

export function signOut() {
  return {
    type: '@auth/SIGN_OUT',
  };
}

